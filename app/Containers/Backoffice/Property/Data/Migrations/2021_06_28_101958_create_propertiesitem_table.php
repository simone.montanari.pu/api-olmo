<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertiesItemTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('olmo_propertyitem')) {
            Schema::table('olmo_slider', function (Blueprint $table) {

            });
        } else {
            Schema::create('olmo_propertyitem', function (Blueprint $table) {
                // Create new table...
                $table->charset = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';
                // General
                $table->id();
                $table->text('name_txt_general')->nullable();
                $table->text('code_txt_general')->nullable();
                $table->text('order_num_general')->nullable();
                $table->text('image_img_general')->nullable();
                $table->text('optional_is_general')->nullable();
                $table->text('enabled_is_general')->nullable();
                $table->text('description_editor_general')->nullable();                
                $table->text('color_color_general')->nullable();
                $table->timestamps();
                //$table->softDeletes();
            });
        } 


    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('properties');
    }
}
