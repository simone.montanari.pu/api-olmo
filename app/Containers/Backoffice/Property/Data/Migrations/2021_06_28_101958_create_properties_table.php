<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Ship\Parents\Controllers\ApiMigration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('olmo_property')) {
            Schema::table('olmo_slider', function (Blueprint $table) {

            });
        } else {
            Schema::create('olmo_property', function (Blueprint $table) {
                // Create new table...
                $table->charset = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';
                // General
                $table->id();
                ApiMigration::Locale($table);
                $table->text('name_txt_general')->nullable();
                $table->text('primary_img_general')->nullable();
                $table->text('optional_is_general')->nullable();
                $table->text('enabled_is_general')->nullable();
                $table->text('title_editor_general')->nullable();
                $table->text('order_num_general')->nullable();
                $table->text('property_list_general')->nullable();
                $table->timestamps();
                //$table->softDeletes();
            });
        } 


    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('properties');
    }
}
