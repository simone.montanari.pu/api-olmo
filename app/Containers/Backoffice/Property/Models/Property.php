<?php

namespace App\Containers\Backoffice\Property\Models;

use App\Ship\Parents\Models\Model;

class Property extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Property';

    public $table = 'olmo_property';
    
    public static function getAttr(){
        return [
            'table' => 'olmo_property',
            'lang'  => true,
            'required' =>[
   
            ]
        ];
    }
}
