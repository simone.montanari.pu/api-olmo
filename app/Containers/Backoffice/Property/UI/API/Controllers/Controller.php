<?php

namespace App\Containers\Backoffice\Property\UI\API\Controllers;

use App\Containers\Backoffice\Property\UI\API\Requests\CreatePropertyRequest;
use App\Containers\Backoffice\Property\UI\API\Requests\DeletePropertyRequest;
use App\Containers\Backoffice\Property\UI\API\Requests\GetAllPropertiesRequest;
use App\Containers\Backoffice\Property\UI\API\Requests\FindPropertyByIdRequest;
use App\Containers\Backoffice\Property\UI\API\Requests\UpdatePropertyRequest;
use App\Containers\Backoffice\Property\UI\API\Transformers\PropertyTransformer;
use App\Containers\Backoffice\Property\Actions\CreatePropertyAction;
use App\Containers\Backoffice\Property\Actions\FindPropertyByIdAction;
use App\Containers\Backoffice\Property\Actions\GetAllPropertiesAction;
use App\Containers\Backoffice\Property\Actions\UpdatePropertyAction;
use App\Containers\Backoffice\Property\Actions\DeletePropertyAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use App\Containers\Backoffice\Property\Models\Property;
use Illuminate\Support\Facades\DB;

class Controller extends ApiController
{
 

    public function getListV1(CreatePropertyRequest $request)
    {
        $lang = $request->lang;
    	$datas   = Property::select('name_txt_general', 'enabled_is_general','id')
        ->where('locale_hidden_general',$lang)
        ->get()->toArray();

        $attribute  = Property::getAttr();
        return  $this->getList($datas,$attribute);

    }


    public function createUpdateV1(CreatePropertyRequest $request)
    {
    	$attribute  = Property::getAttr();;
    	return $this->createUpdate($request,$attribute);
    }

    public function createUpdateValue(CreatePropertyRequest $request)
    {
        $attribute  = Property::getAttr();
        $attribute['table'] = 'olmo_propertyitem';
        $attribute['lang']  = false;
        $model              =  $request->model;
        $name_property      =  $model.'_list_general';
        $table              = 'olmo_'.$model;
        $id_model           =  $request->id_model;
        $id_property        =  $request->id_property;
        $request->id        =  $id_property;
    	$res                =  $this->createUpdate($request,$attribute);
        $id_property        =  $res[0]['value'];
        
        //Salvo id slide sul table ( model entity)
        $current_val =   @Db::table($table)->where('id',$id_model)->first()->{$name_property};

  
        $check = false;
        if($current_val != null){
            $values = explode(',',$current_val);
            if(in_array($id_property,$values)){
                $check = true;
            }
        }

        //return [$table,$current_val,$check,$id_model,$id_slider];
        if($check == false){
            if($current_val == null ){
                Db::table($table)->where('id',$id_model)->update([$name_property => $id_property ]);
            }
            else{
               Db::table($table)->where('id',$id_model)->update([$name_property => $current_val.','.$id_property]);
            }
        }
        
    }


    public function createUpdateItemPropertyV1(CreatePropertyRequest $request)
    {
        $attribute  = Property::getAttr();
        $attribute['table'] = 'olmo_propertyitem';
        $attribute['lang']  = false;
        $name_property      =  $request->name_property;
        $model              =  $request->model;
        $table              = 'olmo_'.$model;
        $id_model           =  $request->id_model;
        $id_property        =  $request->id_property;
        $request->id        =  $id_property;
    	$res                =  $this->createUpdate($request,$attribute);
        $id_property        =  $res[0]['value'];
        
        //Salvo id slide sul table ( model entity)
        $current_val =   @Db::table($table)->where('id',$id_model)->first()->{$name_property};

  
        $check = false;
        if($current_val != null){
            $values = explode(',',$current_val);
            if(in_array($id_property,$values)){
                $check = true;
            }
        }

        //return [$table,$current_val,$check,$id_model,$id_slider];
        if($check == false){
            if($current_val == null ){
                Db::table($table)->where('id',$id_model)->update([$name_property => $id_property ]);
            }
            else{
               Db::table($table)->where('id',$id_model)->update([$name_property => $current_val.','.$id_property]);
            }
        }
        
   
  
    }


    public function propertyList(CreatePropertyRequest $request)
    {
        $model       = $request->model;
        $id_model    =  $request->id;
        $table       = 'olmo_'.$model;
        $field       = $model.'_list_general';
        $e = Db::table($table)->where('id',$id_model)->select($model.'_list_general','id')->get()->toArray();

  

        if($e[0]->{$field} == ''){
            return ['items' => []];
        }else{
        $properties = explode(',',$e[0]->{$field});

        $datas   = Db::table('olmo_propertyitem')->whereIn('id',$properties)
        ->select('name_txt_general','code_txt_general','enabled_is_general','id')
        ->get()->toArray();


        $attribute  = Property::getAttr();
        $attribute['table'] = 'olmo_propertyitem';
        return  $this->getList($datas,$attribute);
        }
       
    }


    public function findById(CreatePropertyRequest $request)
    { 
        $lang       = $request->lang;
        $attribute  = Property::getAttr();
        $id         = $request->id;
        if($id == 0){
            return $this->emptyItem($attribute,$lang);
        }else{
           $datas      = Property::where('id',$id)->get()->toArray();
           $attribute  = Property::getAttr();
           return  $this->getSingleList($datas,$attribute,$id,$lang);
        }
    }



    public function findByValue(CreatePropertyRequest $request)
    { 

        $lang       = $request->lang;
        $attribute  = Property::getAttr();
        $attribute['table'] = 'olmo_propertyitem';
        $id         = $request->value_id;
        $datas  = Db::table('olmo_propertyitem')->where('id',$id)->get()->toArray();
        if($id == 0){
            return $this->emptyItem($attribute,$lang);
        }else{
        return  $this->getSingleList($datas,$attribute,$id,$lang);
        }
    }

    
}
