<?php

/**
 * @apiGroup           Property
 * @apiName            getAllProperties
 *
 * @api                {GET} /v1/property Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Property\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;
Route::post('{lang}/property', [Controller::class, 'getListV1'])->middleware(['auth:api']);

