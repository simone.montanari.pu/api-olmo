<?php

/**
 * @apiGroup           Property
 * @apiName            createProperty
 *
 * @api                {POST} /v1/properties Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Property\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::put('{lang}/property/{id}', [Controller::class, 'createUpdateV1'])->middleware(['auth:api']);

Route::put('{lang}/{model}/{id_model}/value/{id_property}', [Controller::class, 'createUpdateValue']);
