<?php

namespace App\Containers\Backoffice\Property\Actions;

use App\Containers\Backoffice\Property\Models\Property;
use App\Containers\Backoffice\Property\Tasks\FindPropertyByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindPropertyByIdAction extends Action
{
    public function run(Request $request): Property
    {
        return app(FindPropertyByIdTask::class)->run($request->id);
    }
}
