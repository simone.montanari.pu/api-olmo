<?php

namespace App\Containers\Backoffice\Product\Tasks;

use App\Containers\Backoffice\Service\Data\Repositories\ServiceRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllServicesTask extends Task
{
    protected ServiceRepository $repository;

    public function __construct(ServiceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
