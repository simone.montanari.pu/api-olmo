<?php

namespace App\Containers\Backoffice\Service\Models;

use App\Ship\Parents\Models\Model;
use Illuminate\Support\Facades\DB;

class Service extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $table = 'olmo_service';

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Service';

    public static function getAttr(){
        return [
            'table' => 'olmo_service',
            'lang'  => true,
            'default' =>[
                'qty_spin_general' => '0'
            ],
            'required' =>[
                'name_txt_general',
                'slug_txt_general',
                'template_id_general'
            ],
            'rules' => [
                [
                    'type' => 'unique',
                    'field' => 'slug_txt_general',
                    'table' => 'olmo_service',
                    'errortxt' => 'slug duplicate'
                ]
            ]
        ];
    }

}
