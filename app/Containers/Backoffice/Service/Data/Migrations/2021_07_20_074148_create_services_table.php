<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Ship\Parents\Controllers\ApiMigration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('olmo_service')) {
            Schema::table('olmo_service', function (Blueprint $table) {
                // Update existing table...
            });
        } else {
            Schema::create('olmo_service', function (Blueprint $table) {
                // Create new table...
                $table->charset = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';
                // General
                ApiMigration::Page($table);
                $table->text('category_multid_general')->nullable();
                $table->text('product_multid_general')->nullable();
                $table->unsignedDecimal('price_num_general', $precision = 12, $scale = 2)->nullable(); 
                $table->integer('qty_spin_general')->nullable();          
                // SEO
                ApiMigration::Seo($table);
                // Sitemap
                ApiMigration::Sitemap($table);                
                // Content
                $table->text('title_txt_content')->nullable();
                $table->text('subtitle_txt_content')->nullable();
                $table->text('abstract_txt_content')->nullable();
                $table->text('download_media_content')->nullable();
                $table->text('cover_img_content')->nullable();
                $table->text('cta_txt_content')->nullable();
                $table->text('content1_editor_content')->nullable();
                $table->text('image1_img_content')->nullable();
                $table->text('content2_editor_content')->nullable();
                $table->text('image2_img_content')->nullable();
                $table->text('content3_editor_content')->nullable();
                $table->text('image3_img_content')->nullable();
                $table->text('content4_editor_content')->nullable();
                $table->text('image4_img_content')->nullable();
                $table->text('images_multimg_content')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_service');
    }
}
