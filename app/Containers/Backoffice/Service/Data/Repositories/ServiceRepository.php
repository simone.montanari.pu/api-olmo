<?php

namespace App\Containers\Backoffice\Service\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class ServiceRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
