<?php

namespace App\Containers\Backoffice\Service\Actions;

use App\Containers\Backoffice\Product\Tasks\DeleteServiceTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteServiceAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteServiceTask::class)->run($request->id);
    }
}
