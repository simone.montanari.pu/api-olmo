<?php

namespace App\Containers\Backoffice\Service\Actions;

use App\Containers\Backoffice\Product\Models\Service;
use App\Containers\Backoffice\Product\Tasks\FindServiceByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindServiceByIdAction extends Action
{
    public function run(Request $request): Service
    {
        return app(FindServieByIdTask::class)->run($request->id);
    }
}
