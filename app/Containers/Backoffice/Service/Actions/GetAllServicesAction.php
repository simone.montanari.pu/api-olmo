<?php

namespace App\Containers\Backoffice\Service\Actions;

use App\Containers\Backoffice\Product\Tasks\GetAllServicesTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllServicesAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllServicesTask::class)->addRequestCriteria()->run();
    }
}
