<?php

namespace App\Containers\Backoffice\Service\Actions;

use App\Containers\Backoffice\Product\Models\Service;
use App\Containers\Backoffice\Product\Tasks\UpdateServiceTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateServiceAction extends Action
{
    public function run(Request $request): Product
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateServiceTask::class)->run($request->id, $data);
    }
}
