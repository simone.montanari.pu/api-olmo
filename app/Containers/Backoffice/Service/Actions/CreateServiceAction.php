<?php

namespace App\Containers\Backoffice\Service\Actions;

use App\Containers\Backoffice\Product\Models\Service;
use App\Containers\Backoffice\Product\Tasks\CreateServiceTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateServiceAction extends Action
{
    public function run(Request $request): Service
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateServiceTask::class)->run($data);
    }
}
