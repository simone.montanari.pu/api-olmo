<?php

namespace App\Containers\Backoffice\Service\UI\API\Controllers;

use App\Containers\Backoffice\Service\UI\API\Requests\CreateServiceRequest;
use App\Containers\Backoffice\Service\UI\API\Requests\DeleteServiceRequest;
use App\Containers\Backoffice\Service\UI\API\Requests\GetAllServicesRequest;
use App\Containers\Backoffice\Service\UI\API\Requests\FindServiceByIdRequest;
use App\Containers\Backoffice\Service\UI\API\Requests\UpdateServiceRequest;
use App\Containers\Backoffice\Service\UI\API\Transformers\ServiceTransformer;
use App\Containers\Backoffice\Service\Actions\CreateServiceAction;
use App\Containers\Backoffice\Service\Actions\FindServiceByIdAction;
use App\Containers\Backoffice\Service\Actions\GetAllServicesAction;
use App\Containers\Backoffice\Service\Actions\UpdateServiceAction;
use App\Containers\Backoffice\Service\Actions\DeleteServiceAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use App\Containers\Backoffice\Service\Models\Service;
use App\Ship\Parents\Controllers\ApiHelper;
use Illuminate\Support\Facades\DB;


class Controller extends ApiController
{
   
    public function getListV1(GetAllServicesRequest $request)
    {
        $lang = $request->lang;
        $datas      = Service::select('name_txt_general','cover_img_content','position_ord_general as position_txt_general',"lang_langs_general",'enabled_is_general','template_id_general','id')
        ->where('locale_hidden_general',$lang)
        ->get()
        ->toArray();
        $attribute  = Service::getAttr();
        return  $this->getList($datas,$attribute);

    }

    public function findById(GetAllServicesRequest $request)
    { 

        $attribute  = Service::getAttr();
    	$id 		= $request->id;
        $lang       = $request->lang;
        if($id == 0){
            return $this->emptyItem($attribute,$lang);
        }else{
    	   $datas      = Service::where('id',$id)->get()->toArray();
           $attribute  = Service::getAttr();
           return  $this->getSingleList($datas,$attribute,$id,$lang);
        }
    }


    public function createUpdateV1(GetAllServicesRequest $request)
    {
    	$attribute  = Service::getAttr();
        $rules       = $attribute['rules'];
        $required    = $attribute['required'];
        if($required){
            $res = ApiHelper::CheckRequired($request,$required);
            if($res['valid'] == false){
                return response($res['err'], 400);
                exit();
            }
        }
        if($rules){
            $res = ApiHelper::CheckRules($request,$rules);
            if($res['valid'] == false){
                return response($res['err'], 400);
                exit();
            }
        }
    	return $this->createUpdate($request,$attribute);
    }

   
}
