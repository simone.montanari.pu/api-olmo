<?php

namespace App\Containers\Backoffice\Service\UI\API\Transformers;

use App\Containers\Backoffice\Service\Models\Service;
use App\Ship\Parents\Transformers\Transformer;

class ServiceTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Service $service): array
    {
        $response = [
            'object' => $service->getResourceKey(),
            'id' => $service->getHashedKey(),
            'created_at' => $service->created_at,
            'updated_at' => $service->updated_at,
            'readable_created_at' => $service->created_at->diffForHumans(),
            'readable_updated_at' => $service->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $service->id,
            // 'deleted_at' => $service->deleted_at,
        ], $response);

        return $response;
    }
}
