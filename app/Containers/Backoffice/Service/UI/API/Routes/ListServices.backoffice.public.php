<?php

/**
 * @apiGroup           Service
 * @apiName            getAllServices
 *
 * @api                {GET} /v1/service Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
 */

use App\Containers\Backoffice\Service\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::post('{lang}/service', [Controller::class, 'getListV1'])->middleware(['auth:api']);