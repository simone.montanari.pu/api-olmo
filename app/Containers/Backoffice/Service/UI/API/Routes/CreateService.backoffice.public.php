<?php

/**
 * @apiGroup           Service
 * @apiName            createService
 *
 * @api                {POST} /backoffice/services Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Service\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::put('{lang}/service/{id}', [Controller::class, 'createUpdateV1'])->middleware(['auth:api']);

