<?php

namespace App\Containers\Backoffice\Order\Actions;

use App\Containers\Backoffice\Order\Models\Order;
use App\Containers\Backoffice\Order\Tasks\UpdateOrderTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateOrderAction extends Action
{
    public function run(Request $request): Order
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateOrderTask::class)->run($request->id, $data);
    }
}
