<?php

namespace App\Containers\Backoffice\Order\Actions;

use App\Containers\Backoffice\Order\Tasks\DeleteOrderTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteOrderAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteOrderTask::class)->run($request->id);
    }
}
