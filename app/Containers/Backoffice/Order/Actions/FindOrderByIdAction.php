<?php

namespace App\Containers\Backoffice\Order\Actions;

use App\Containers\Backoffice\Order\Models\Order;
use App\Containers\Backoffice\Order\Tasks\FindOrderByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindOrderByIdAction extends Action
{
    public function run(Request $request): Order
    {
        return app(FindOrderByIdTask::class)->run($request->id);
    }
}
