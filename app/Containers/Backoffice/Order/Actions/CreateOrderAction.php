<?php

namespace App\Containers\Backoffice\Order\Actions;

use App\Containers\Backoffice\Order\Models\Order;
use App\Containers\Backoffice\Order\Tasks\CreateOrderTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateOrderAction extends Action
{
    public function run(Request $request): Order
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateOrderTask::class)->run($data);
    }
}
