<?php

/**
 * @apiGroup           Order
 * @apiName            updateOrder
 *
 * @api                {PATCH} /v1/orders/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Order\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::put('{lang}/order/{id}', [Controller::class, 'createUpdateV1'])->middleware(['auth:api']);
Route::delete('order/{id}', [Controller::class, 'deleteProduct']);
Route::get('{lang}/order/{id}', [Controller::class, 'findById']);
Route::post('{lang}/order', [Controller::class, 'getListV1'])->middleware(['auth:api']);
    

