<?php

namespace App\Containers\Backoffice\Order\UI\API\Controllers;

use App\Containers\Backoffice\Order\UI\API\Requests\CreateOrderRequest;
use App\Containers\Backoffice\Order\UI\API\Requests\DeleteOrderRequest;
use App\Containers\Backoffice\Order\UI\API\Requests\GetAllOrdersRequest;
use App\Containers\Backoffice\Order\UI\API\Requests\FindOrderByIdRequest;
use App\Containers\Backoffice\Order\UI\API\Requests\UpdateOrderRequest;
use App\Containers\Backoffice\Order\UI\API\Transformers\OrderTransformer;
use App\Containers\Backoffice\Order\Actions\CreateOrderAction;
use App\Containers\Backoffice\Order\Actions\FindOrderByIdAction;
use App\Containers\Backoffice\Order\Actions\GetAllOrdersAction;
use App\Containers\Backoffice\Order\Actions\UpdateOrderAction;
use App\Containers\Backoffice\Order\Actions\DeleteOrderAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use App\Containers\Backoffice\Order\Models\Order;
use App\Ship\Parents\Controllers\ApiHelper;


class Controller extends ApiController
{

    public function getListV1(GetAllOrdersRequest $request)
    {
        $lang = $request->lang;
        $datas      = Order::select("code_read_general as code_txt_general",'createdonutc_read_general as createdonutc_txt_general','totalvat_read_general as totalvat_txt_general','customerid_read_general as extrafield.customer.email_email_general', 'orderstatus_select_general as orderstatus_label_general','id')
        ->get()
        ->toArray();
        $attribute  = Order::getAttr();
        return  $this->getList($datas,$attribute);

    }

    public function findById(GetAllOrdersRequest $request)
    { 

        $attribute  = Order::getAttr();
    	$id 		= $request->id;
        $lang       = $request->lang;
        if($id == 0){
            return $this->emptyItem($attribute,$lang);
        }else{
    	   $datas      = Order::where('id',$id)->get()->toArray();
           $attribute  = Order::getAttr();
         
           return  $this->getSingleList($datas,$attribute,$id,$lang);
        }
    }


    public function createUpdateV1(GetAllOrdersRequest $request)
    {
        $attribute   = Order::getAttr();
        $rules       = $attribute['rules'];
        $required    = $attribute['required'];
        if($required){
            $res = ApiHelper::CheckRequired($request,$required);
            if($res['valid'] == false){
                return response($res['err'], 400);
                exit();
            }
        }
        if($rules){
            $res = ApiHelper::CheckRules($request,$rules);
            if($res['valid'] == false){
                return response($res['err'], 400);
                exit();
            }
        }
    	return $this->createUpdate($request,$attribute);
    }





}
