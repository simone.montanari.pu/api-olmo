<?php

namespace App\Containers\Backoffice\Order\UI\API\Transformers;

use App\Containers\Backoffice\Order\Models\Order;
use App\Ship\Parents\Transformers\Transformer;

class OrderTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Order $order): array
    {
        $response = [
            'object' => $order->getResourceKey(),
            'id' => $order->getHashedKey(),
            'created_at' => $order->created_at,
            'updated_at' => $order->updated_at,
            'readable_created_at' => $order->created_at->diffForHumans(),
            'readable_updated_at' => $order->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $order->id,
            // 'deleted_at' => $order->deleted_at,
        ], $response);

        return $response;
    }
}
