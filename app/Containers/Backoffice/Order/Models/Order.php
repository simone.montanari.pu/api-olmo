<?php

namespace App\Containers\Backoffice\Order\Models;

use App\Ship\Parents\Models\Model;

class Order extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $table = 'olmo_order';

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Order';

    public static function getAttr(){
        return ['table' => 'olmo_order'];
    }

}
