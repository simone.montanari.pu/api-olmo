<?php

namespace App\Containers\Backoffice\Order\Tasks;

use App\Containers\Backoffice\Order\Data\Repositories\OrderRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllOrdersTask extends Task
{
    protected OrderRepository $repository;

    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
