<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('olmo_order')) {
            Schema::table('olmo_order', function (Blueprint $table) {
                // Update existing table...
            });
        } else {
            Schema::create('olmo_order', function (Blueprint $table) {
                // Create new table...
                $table->charset = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';
                // General
                $table->increments('id')->unsigned();
                $table->text('code_read_general')->nullable();
                $table->text('slug_txt_general')->nullable();
                $table->text('customerid_read_general')->nullable();
                $table->dateTime('createdonutc_read_general')->nullable();
                $table->dateTime('lastmod_read_general')->nullable();
                $table->text('orderstatus_select_general')->nullable();
                $table->dateTime('paidonutc_read_general')->nullable();
                $table->text('paymentmethod_txt_general')->nullable();
                $table->text('shippingmethod_txt_general')->nullable(); 
                $table->unsignedDecimal('subtotal_read_general', $precision = 12, $scale = 2)->nullable();
                $table->unsignedDecimal('total_read_general', $precision = 12, $scale = 2)->nullable();
                $table->text('discountcode_read_general')->nullable();
                $table->text('vat_read_general')->nullable();
                $table->text('country_read_general')->nullable();
                $table->text('company_read_general')->nullable();
                $table->unsignedDecimal('discountvalue_read_general', $precision = 12, $scale = 2)->nullable();
                $table->unsignedDecimal('totalvat_read_general', $precision = 12, $scale = 2)->nullable();
                $table->text('orderitems_json_items')->nullable();
                $table->text('billingname_read_general')->nullable();
                $table->text('billingsurname_read_general')->nullable();
                $table->text('billingphone_read_general')->nullable();
                $table->text('billingcompany_read_general')->nullable();
                $table->text('billingaddress1_read_general')->nullable();
                $table->text('billingaddress2_read_general')->nullable();
                $table->text('billingcity_read_general')->nullable();
                $table->text('billingstate_read_general')->nullable();
                $table->text('billingcountry_read_general')->nullable();
                $table->text('billingZIP_read_general')->nullable();
                $table->text('shippingname_read_general')->nullable();
                $table->text('shippingsurname_read_general')->nullable();
                $table->text('shippingphone_read_general')->nullable();
                $table->text('shippingcompany_read_general')->nullable();
                $table->text('shippingaddress1_read_general')->nullable();
                $table->text('shippingaddress2_read_general')->nullable();
                $table->text('shippingcity_read_general')->nullable();
                $table->text('shippingstate_read_general')->nullable();
                $table->text('shippingcountry_read_general')->nullable();
                $table->text('shippingZIP_read_general')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_order');
    }
}
