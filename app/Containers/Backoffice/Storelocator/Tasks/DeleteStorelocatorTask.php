<?php

namespace App\Containers\Backoffice\Storelocator\Tasks;

use App\Containers\Backoffice\Storelocator\Data\Repositories\StorelocatorRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteStorelocatorTask extends Task
{
    protected StorelocatorRepository $repository;

    public function __construct(StorelocatorRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id): ?int
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
