<?php

namespace App\Containers\Backoffice\Storelocator\Tasks;

use App\Containers\Backoffice\Storelocator\Data\Repositories\StorelocatorRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateStorelocatorTask extends Task
{
    protected StorelocatorRepository $repository;

    public function __construct(StorelocatorRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
