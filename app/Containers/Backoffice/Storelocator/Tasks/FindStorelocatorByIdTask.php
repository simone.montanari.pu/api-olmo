<?php

namespace App\Containers\Backoffice\Storelocator\Tasks;

use App\Containers\Backoffice\Storelocator\Data\Repositories\StorelocatorRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindStorelocatorByIdTask extends Task
{
    protected StorelocatorRepository $repository;

    public function __construct(StorelocatorRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
