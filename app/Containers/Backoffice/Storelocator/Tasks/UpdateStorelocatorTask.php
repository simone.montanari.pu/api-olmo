<?php

namespace App\Containers\Backoffice\Storelocator\Tasks;

use App\Containers\Backoffice\Storelocator\Data\Repositories\StorelocatorRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateStorelocatorTask extends Task
{
    protected StorelocatorRepository $repository;

    public function __construct(StorelocatorRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
