<?php

namespace App\Containers\Backoffice\Storelocator\Tasks;

use App\Containers\Backoffice\Storelocator\Data\Repositories\StorelocatorRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllStorelocatorsTask extends Task
{
    protected StorelocatorRepository $repository;

    public function __construct(StorelocatorRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
