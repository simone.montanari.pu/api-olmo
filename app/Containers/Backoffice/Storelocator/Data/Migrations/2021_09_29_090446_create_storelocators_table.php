<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Ship\Parents\Controllers\ApiMigration;

class CreateStorelocatorsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('olmo_storelocator')) {
            Schema::table('olmo_storelocator', function (Blueprint $table) {
                // Update existing table...
            });
        } else {
            Schema::create('olmo_storelocator', function (Blueprint $table) {
                // Create new table...
                $table->charset   = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';

                // General
                ApiMigration::Page($table);                
                $table->text('uuid_hidden_general')->nullable();
                $table->text('lat_txt_general')->nullable();
                $table->text('lng_txt_general')->nullable();
                $table->text('area_txt_general')->nullable();
                $table->text('sector_txt_general')->nullable();
                $table->text('address_editor_general')->nullable();
                $table->text('phone_txt_general')->nullable();
                $table->text('website_txt_general')->nullable();
                $table->text('email_txt_general')->nullable();
                $table->text('country_txt_general')->nullable();
                $table->text('province_txt_general')->nullable();
                $table->text('subsidiary_txt_general')->nullable();

                // Content
                $table->text('title_editor_content')->nullable();
                $table->text('subtitle_editor_content')->nullable();
                $table->text('abstract_editor_content')->nullable();
                $table->text('catalogue_media_content')->nullable();
                $table->text('cover_img_content')->nullable();
                $table->text('content1_editor_content')->nullable();
                $table->text('image1_img_content')->nullable();                
                $table->text('cta_txt_content')->nullable();          

                // SEO
                ApiMigration::Seo($table);
                // Sitemap
                ApiMigration::Sitemap($table);                 
            });
        }  
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_storelocator');
    }
}
