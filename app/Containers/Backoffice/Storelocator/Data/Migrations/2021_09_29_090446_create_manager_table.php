<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Ship\Parents\Controllers\ApiMigration;

class CreateManagerTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('olmo_manager')) {
            Schema::table('olmo_storelocator', function (Blueprint $table) {
                // Update existing table...
            });
        } else {
            Schema::create('olmo_manager', function (Blueprint $table) {
                // Create new table...
                $table->charset = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';

                $table->increments('id')->unsigned();         
                $table->text('store_id')->nullable();
                $table->text('creation_date')->nullable();
                $table->text('type')->nullable();
              
                             
            });
        }  
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_manager');
    }
}
