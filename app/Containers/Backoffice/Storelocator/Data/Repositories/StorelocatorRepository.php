<?php

namespace App\Containers\Backoffice\Storelocator\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class StorelocatorRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
