<?php

namespace App\Containers\Backoffice\Storelocator\Actions;

use App\Containers\Backoffice\Storelocator\Models\Storelocator;
use App\Containers\Backoffice\Storelocator\Tasks\CreateStorelocatorTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateStorelocatorAction extends Action
{
    public function run(Request $request): Storelocator
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateStorelocatorTask::class)->run($data);
    }
}
