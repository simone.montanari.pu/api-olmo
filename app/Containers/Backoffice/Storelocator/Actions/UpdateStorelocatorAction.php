<?php

namespace App\Containers\Backoffice\Storelocator\Actions;

use App\Containers\Backoffice\Storelocator\Models\Storelocator;
use App\Containers\Backoffice\Storelocator\Tasks\UpdateStorelocatorTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateStorelocatorAction extends Action
{
    public function run(Request $request): Storelocator
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateStorelocatorTask::class)->run($request->id, $data);
    }
}
