<?php

namespace App\Containers\Backoffice\Storelocator\Actions;

use App\Containers\Backoffice\Storelocator\Tasks\DeleteStorelocatorTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteStorelocatorAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteStorelocatorTask::class)->run($request->id);
    }
}
