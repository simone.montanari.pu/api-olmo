<?php

namespace App\Containers\Backoffice\Storelocator\Actions;

use App\Containers\Backoffice\Storelocator\Tasks\GetAllStorelocatorsTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllStorelocatorsAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllStorelocatorsTask::class)->addRequestCriteria()->run();
    }
}
