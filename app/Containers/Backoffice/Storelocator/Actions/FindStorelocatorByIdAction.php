<?php

namespace App\Containers\Backoffice\Storelocator\Actions;

use App\Containers\Backoffice\Storelocator\Models\Storelocator;
use App\Containers\Backoffice\Storelocator\Tasks\FindStorelocatorByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindStorelocatorByIdAction extends Action
{
    public function run(Request $request): Storelocator
    {
        return app(FindStorelocatorByIdTask::class)->run($request->id);
    }
}
