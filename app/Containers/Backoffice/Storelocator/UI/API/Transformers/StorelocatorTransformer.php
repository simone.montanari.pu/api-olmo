<?php

namespace App\Containers\Backoffice\Storelocator\UI\API\Transformers;

use App\Containers\Backoffice\Storelocator\Models\Storelocator;
use App\Ship\Parents\Transformers\Transformer;

class StorelocatorTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Storelocator $storelocator): array
    {
        $response = [
            'object' => $storelocator->getResourceKey(),
            'id' => $storelocator->getHashedKey(),
            'created_at' => $storelocator->created_at,
            'updated_at' => $storelocator->updated_at,
            'readable_created_at' => $storelocator->created_at->diffForHumans(),
            'readable_updated_at' => $storelocator->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $storelocator->id,
            // 'deleted_at' => $storelocator->deleted_at,
        ], $response);

        return $response;
    }
}
