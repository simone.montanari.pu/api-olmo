<?php

namespace App\Containers\Backoffice\Storelocator\UI\API\Controllers;

use App\Containers\Backoffice\Storelocator\UI\API\Requests\CreateStorelocatorRequest;
use App\Containers\Backoffice\Storelocator\UI\API\Requests\DeleteStorelocatorRequest;
use App\Containers\Backoffice\Storelocator\UI\API\Requests\GetAllStorelocatorsRequest;
use App\Containers\Backoffice\Storelocator\UI\API\Requests\FindStorelocatorByIdRequest;
use App\Containers\Backoffice\Storelocator\UI\API\Requests\UpdateStorelocatorRequest;
use App\Containers\Backoffice\Storelocator\UI\API\Transformers\StorelocatorTransformer;
use App\Containers\Backoffice\Storelocator\Actions\CreateStorelocatorAction;
use App\Containers\Backoffice\Storelocator\Actions\FindStorelocatorByIdAction;
use App\Containers\Backoffice\Storelocator\Actions\GetAllStorelocatorsAction;
use App\Containers\Backoffice\Storelocator\Actions\UpdateStorelocatorAction;
use App\Containers\Backoffice\Storelocator\Actions\DeleteStorelocatorAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use App\Containers\Backoffice\Storelocator\Models\Storelocator;
use App\Ship\Parents\Controllers\ApiHelper;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Ship\Parents\Controllers\ApiManager;


class Controller extends ApiController
{

    public function getListV1(CreateStorelocatorRequest $request)
    {
        $lang   = $request->lang;
    	$datas  = Storelocator::select('name_txt_general','lat_txt_general','lng_txt_general','id')  
        ->get()
        ->toArray();

        $attribute  = Storelocator::getAttr();
        return  $this->getList($datas,$attribute);

    }

    public function export(CreateStorelocatorRequest $request)
    {
        return ApiManager::export('storelocator','csv');
    }


    public function getImport(CreateStorelocatorRequest $request)
    {
       $i      = 0;
       $res    = [];
       $import = Db::table('olmo_manager')->where('type','import')->orderBy('creation_date','desc')->get();
       foreach($import as $imp){
           $res[$i]['date'] = $imp->creation_date;
           $store                    = Db::table('olmo_storage')->where('id',$imp->store_id)->first();
           $truename                 = $store->truename;
           $folder                   = $store->model;
           $res[$i]['public']        = $store->public;
           $res[$i]['path']          = '/import/'.$folder.'/'.$truename;

           $i++;
       }
      
       return $res;
    }

 

    public function getExport(CreateStorelocatorRequest $request)
    {
       $i      = 0;
       $res    = [];
       $import = Db::table('olmo_manager')->where('type','export')->orderBy('creation_date','desc')->get();
       foreach($import as $imp){
           $res[$i]['date'] = $imp->creation_date;
           $store                    = Db::table('olmo_storage')->where('id',$imp->store_id)->first();
           $truename                 = $store->truename;
           $folder                   = $store->model;
           $res[$i]['public']        = $store->public;
           $res[$i]['path']          = '/export/'.$folder.'/'.$truename;

           $i++;
       }
      
       return $res;
    }


    public function createUpdateV1(CreateStorelocatorRequest $request)
    {
    	$attribute   = Storelocator::getAttr();
        // $rules       = $attribute['rules'];
        // $required    = $attribute['required'];
        
        if(isset($attribute['requiredbackoffice'])){
            $required    = $attribute['requiredbackoffice'];
        }
       
        if($required){
            $res = ApiHelper::CheckRequired($request,$required);
            if($res['valid'] == false){
                return response($res['err'], 400);
                exit();
            }
        }

         
        // if($rules){
        //     $res = ApiHelper::CheckRules($request,$rules);
        //     if($res['valid'] == false){
        //         return response($res['err'], 400);
        //         exit();
        //     }
        // }
       
 
    	return $this->createUpdate($request,$attribute);
    }    

    public function findById(CreateStorelocatorRequest $request)
    { 
        $lang       = $request->lang;
        $attribute  = Storelocator::getAttr();
        $id         = $request->id;
        if($id == 0){
            return $this->emptyItem($attribute,$lang);
        }else{
           $datas      = Storelocator::where('id',$id)->get()->toArray();
           $attribute  = Storelocator::getAttr();
           return  $this->getSingleList($datas,$attribute,$id,$lang);
        }
    }   
    
    public function uploadChunk(CreateStorelocatorRequest $request)
    {

         $chunk_id     = @$_REQUEST['id'];
         $filename     = @$_REQUEST['fileName'];
         $data         = file_get_contents('php://input');
         $temp_data    = Cache::get($filename);
         $current_data = $temp_data.$data;
         Cache::put($filename,$current_data,1200);

         $arr['data']         = null;
         $arr['isSuccess']    = true;
         $arr['errorMessage'] = null;

         return $arr;


    }


    public function completeChunk(CreateStorelocatorRequest $request)
    {

         $chunk_id     = @$request->id;


         $filename     = @$_REQUEST['fileName'];
         $truename     = @$_REQUEST['trueName'];
         $fieldname    = @$_REQUEST['fieldName'];
         $current_data = Cache::get($filename);

         $pagename     =  'storelocator';
         $type         = pathinfo($truename, PATHINFO_EXTENSION);

       

         $categories_files = [
           'csv'  => 'documents'
         ];
        
    
         $path  = 'public/'.$categories_files[$type].'/'.$pagename.'/'.$filename;
         $res   = Storage::put($path, $current_data);

         $public_path = '/'.$categories_files[$type].'/'.$pagename.'/'.$truename;
         $physical_path = '/'.$categories_files[$type].'/'.$pagename.'/'.$filename;

         if($res){
            $res = true;
         }else{
            $res = false;
         }
       


        $storage_id = DB::table('olmo_storage')->insertGetId([
           'filename' 	=> $filename,
           'truename' 	=> $truename,
           'model'     => $pagename,
           'meta'      => $chunk_id.$fieldname,
           'public'    => 'true',
           'type'      => $type
        ]);
        Cache::forget($filename);


        //Importazione
        //Storelocator::truncate();
        $err         = null;
        $attribute   = Storelocator::getAttr();
        $mapping     = $attribute['csvimport']['mapping'];
        $separator   = $attribute['csvimport']['separator'];
        $idcsv       = $attribute['csvimport']['id'];
        $row = 1;

        if (($handle = fopen(storage_path('app/public'.$physical_path), "r")) !== FALSE) {


            Db::table('olmo_manager')->insert([
                'store_id'          => $storage_id,
                'creation_date'     => date("Y-m-d H:i:s"),
                'type'              => 'import'
            ]);

            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                
                //Prendo l'identificativo del record del csv
                $idvalue         = $data[$idcsv];
                $fieldname       = $mapping[$idcsv];

                $storelocator   = Storelocator::where($fieldname,$idvalue)->first();
                //Se esiste il record lo aggiorno altrimenti lo creo
                if($storelocator){
                    $storelocator   = Storelocator::where($fieldname,$idvalue)->first();
                    foreach($mapping as $k => $v){
                        $storelocator->{$v} =  @$data[$k];
                    }
                    $storelocator->save();
                }
                else{
                    $storelocator = new Storelocator;
                    foreach($mapping as $k => $v){
                        $storelocator->{$v} =  @$data[$k];
                    }
                    $storelocator->save();
                }
            }
            fclose($handle);
        }else{
            $err = "File notfound";
        }
      
        return ["Operation completed"];

    }

}
