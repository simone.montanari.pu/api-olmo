<?php

/**
 * @apiGroup           Storelocator
 * @apiName            getAllStorelocators
 *
 * @api                {GET} /v1/storelocators Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Storelocator\UI\API\Controllers\Controller;

use App\Ship\Parents\Controllers\ApiManager;

use Illuminate\Support\Facades\Route;

Route::put('{lang}/storelocator/{id}', [Controller::class, 'createUpdateV1'])->middleware(['auth:api']);

Route::get('{lang}/storelocator/{id}', [Controller::class, 'findById']);

Route::post('{lang}/storelocator', [Controller::class, 'getListV1'])->middleware(['auth:api']);

//Manager
Route::get('{lang}/storelocator/manager/import/list', [Controller::class, 'getImport']);

Route::get('{lang}/storelocator/manager/export', [Controller::class, 'export']);

Route::get('{lang}/storelocator/manager/export/list', [Controller::class, 'getExport']);

Route::post('{lang}/storelocator/uploadchunks', [Controller::class, 'uploadChunk']);

Route::post('{lang}/storelocator/uploadcomplete', [Controller::class, 'completeChunk']);

