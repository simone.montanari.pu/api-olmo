<?php

namespace App\Containers\Backoffice\Storelocator\Models;

use App\Ship\Parents\Models\Model;

class Storelocator extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    public $timestamps = false;

    public $table = 'olmo_storelocator';

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Storelocator';

    public static function getAttr(){
        return [
            'table' => 'olmo_storelocator',
            'requiredbackoffice' => [
                'name_txt_general'
            ],
            'csvimport' => [
                'id'        => '0',
                'separator' => ';',
                'mapping' =>[
                    '0' => 'uuid_hidden_general',
                    '1' => 'lat_txt_general',
                    '2' => 'lng_txt_general',
                    '5' => 'email_txt_general'
                ]
            ]            
        ];
    }
}
