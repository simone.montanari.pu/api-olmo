<?php

namespace App\Containers\Backoffice\Address\Tasks;

use App\Containers\Backoffice\Address\Data\Repositories\AddressRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllAddressesTask extends Task
{
    protected AddressRepository $repository;

    public function __construct(AddressRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
