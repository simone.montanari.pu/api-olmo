<?php

namespace App\Containers\Backoffice\Address\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class AddressRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
