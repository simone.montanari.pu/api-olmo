<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('olmo_address')) {
            Schema::table('olmo_address', function (Blueprint $table) {
                // Update existing table...
            });
        } else {
            Schema::create('olmo_address', function (Blueprint $table) {
                // Create new table...
                $table->charset = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';
                // General
                $table->id();
                $table->text('enabled_is_general')->nullable();
                $table->text('default_is_general')->nullable();
                $table->text('typeaddress_select_general')->nullable();
                $table->text('name_txt_general')->nullable();
                $table->text('surname_txt_general')->nullable();
                $table->string('phone_txt_general', 100)->nullable();
                $table->text('company_txt_general')->nullable();
                $table->text('address1_txt_general')->nullable();
                $table->text('address2_txt_shipping')->nullable();
                $table->text('city_txt_general')->nullable();
                $table->text('region_txt_general')->nullable();
                $table->text('country_txt_general')->nullable();
                $table->text('zip_txt_general')->nullable();
                $table->text('customer_hidden_general')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_address');
    }
}
