<?php

namespace App\Containers\Backoffice\Address\UI\API\Controllers;

use App\Containers\Backoffice\Address\UI\API\Requests\CreateAddressRequest;
use App\Containers\Backoffice\Address\UI\API\Requests\DeleteAddressRequest;
use App\Containers\Backoffice\Address\UI\API\Requests\GetAllAddressesRequest;
use App\Containers\Backoffice\Address\UI\API\Requests\FindAddressByIdRequest;
use App\Containers\Backoffice\Address\UI\API\Requests\UpdateAddressRequest;
use App\Containers\Backoffice\Address\UI\API\Transformers\AddressTransformer;
use App\Containers\Backoffice\Address\Actions\CreateAddressAction;
use App\Containers\Backoffice\Address\Actions\FindAddressByIdAction;
use App\Containers\Backoffice\Address\Actions\GetAllAddressesAction;
use App\Containers\Backoffice\Address\Actions\UpdateAddressAction;
use App\Containers\Backoffice\Address\Actions\DeleteAddressAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;

class Controller extends ApiController
{
    public function createAddress(CreateAddressRequest $request): JsonResponse
    {
        $address = app(CreateAddressAction::class)->run($request);
        return $this->created($this->transform($address, AddressTransformer::class));
    }

    public function findAddressById(FindAddressByIdRequest $request): array
    {
        $address = app(FindAddressByIdAction::class)->run($request);
        return $this->transform($address, AddressTransformer::class);
    }

    public function getAllAddresses(GetAllAddressesRequest $request): array
    {
        $addresses = app(GetAllAddressesAction::class)->run($request);
        return $this->transform($addresses, AddressTransformer::class);
    }

    public function updateAddress(UpdateAddressRequest $request): array
    {
        $address = app(UpdateAddressAction::class)->run($request);
        return $this->transform($address, AddressTransformer::class);
    }

    public function deleteAddress(DeleteAddressRequest $request): JsonResponse
    {
        app(DeleteAddressAction::class)->run($request);
        return $this->noContent();
    }
}
