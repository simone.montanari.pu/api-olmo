<?php

namespace App\Containers\Backoffice\Address\UI\API\Transformers;

use App\Containers\Backoffice\Address\Models\Address;
use App\Ship\Parents\Transformers\Transformer;

class AddressTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Address $address): array
    {
        $response = [
            'object' => $address->getResourceKey(),
            'id' => $address->getHashedKey(),
            'created_at' => $address->created_at,
            'updated_at' => $address->updated_at,
            'readable_created_at' => $address->created_at->diffForHumans(),
            'readable_updated_at' => $address->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $address->id,
            // 'deleted_at' => $address->deleted_at,
        ], $response);

        return $response;
    }
}
