<?php

/**
 * @apiGroup           Address
 * @apiName            createAddress
 *
 * @api                {POST} /v1/addresses Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Address\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::post('addresses', [Controller::class, 'createAddress'])
    ->name('api_address_create_address')
    ->middleware(['auth:api']);

