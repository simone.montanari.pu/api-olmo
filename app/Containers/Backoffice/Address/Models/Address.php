<?php

namespace App\Containers\Backoffice\Address\Models;

use App\Ship\Parents\Models\Model;

class Address extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $table = 'olmo_address';

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Address';
    
    public static function getAttr(){
        return [
            'table' => 'olmo_address',
            'required' => [
                'name_txt_general',
                'surname_txt_general',
                'phone_txt_general',
                'company_txt_general',
                'address1_txt_general',
                'city_txt_general',
                'region_txt_general',
                'country_txt_general',
                'zip_txt_general',
            ],
            'forms' => [
                'billing' => [
                     'groups'  => ['general','billing'],
                     'exclude' => ['typeaddress_select_general','excldefault_is_generalude', 'customer_hidden_general', 'enabled_is_general', 'default_is_general'],
                     'submit'  => false
                ],
                'shipping' => [
                    'groups'  => ['general','shipping'],
                    'exclude' => ['typeaddress_select_general','excldefault_is_generalude', 'customer_hidden_general', 'enabled_is_general', 'default_is_general'],
                    'submit'  => false
                ]
             ]
        ];
    }
    
}
