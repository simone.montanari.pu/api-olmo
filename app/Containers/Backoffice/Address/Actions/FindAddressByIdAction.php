<?php

namespace App\Containers\Backoffice\Address\Actions;

use App\Containers\Backoffice\Address\Models\Address;
use App\Containers\Backoffice\Address\Tasks\FindAddressByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindAddressByIdAction extends Action
{
    public function run(Request $request): Address
    {
        return app(FindAddressByIdTask::class)->run($request->id);
    }
}
