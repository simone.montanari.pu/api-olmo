<?php

namespace App\Containers\Backoffice\Address\Actions;

use App\Containers\Backoffice\Address\Tasks\GetAllAddressesTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllAddressesAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllAddressesTask::class)->addRequestCriteria()->run();
    }
}
