<?php

namespace App\Containers\Backoffice\Address\Actions;

use App\Containers\Backoffice\Address\Tasks\DeleteAddressTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteAddressAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteAddressTask::class)->run($request->id);
    }
}
