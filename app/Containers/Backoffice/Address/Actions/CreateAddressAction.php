<?php

namespace App\Containers\Backoffice\Address\Actions;

use App\Containers\Backoffice\Address\Models\Address;
use App\Containers\Backoffice\Address\Tasks\CreateAddressTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateAddressAction extends Action
{
    public function run(Request $request): Address
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateAddressTask::class)->run($data);
    }
}
