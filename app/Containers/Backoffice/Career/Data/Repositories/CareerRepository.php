<?php

namespace App\Containers\Backoffice\Career\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class CareerRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
