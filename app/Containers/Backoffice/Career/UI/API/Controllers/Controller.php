<?php

namespace App\Containers\Backoffice\Career\UI\API\Controllers;

use App\Containers\Backoffice\Career\UI\API\Requests\CreateCareerRequest;
use App\Containers\Backoffice\Career\UI\API\Requests\DeleteCareerRequest;
use App\Containers\Backoffice\Career\UI\API\Requests\GetAllCareersRequest;
use App\Containers\Backoffice\Career\UI\API\Requests\FindCareerByIdRequest;
use App\Containers\Backoffice\Career\UI\API\Requests\UpdateCareerRequest;
use App\Containers\Backoffice\Career\UI\API\Transformers\CareerTransformer;
use App\Containers\Backoffice\Career\Actions\CreateCareerAction;
use App\Containers\Backoffice\Career\Actions\FindCareerByIdAction;
use App\Containers\Backoffice\Career\Actions\GetAllCareersAction;
use App\Containers\Backoffice\Career\Actions\UpdateCareerAction;
use App\Containers\Backoffice\Career\Actions\DeleteCareerAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;

class Controller extends ApiController
{
    public function createCareer(CreateCareerRequest $request): JsonResponse
    {
        $career = app(CreateCareerAction::class)->run($request);
        return $this->created($this->transform($career, CareerTransformer::class));
    }

    public function findCareerById(FindCareerByIdRequest $request): array
    {
        $career = app(FindCareerByIdAction::class)->run($request);
        return $this->transform($career, CareerTransformer::class);
    }

    public function getAllCareers(GetAllCareersRequest $request): array
    {
        $careers = app(GetAllCareersAction::class)->run($request);
        return $this->transform($careers, CareerTransformer::class);
    }

    public function updateCareer(UpdateCareerRequest $request): array
    {
        $career = app(UpdateCareerAction::class)->run($request);
        return $this->transform($career, CareerTransformer::class);
    }

    public function deleteCareer(DeleteCareerRequest $request): JsonResponse
    {
        app(DeleteCareerAction::class)->run($request);
        return $this->noContent();
    }
}
