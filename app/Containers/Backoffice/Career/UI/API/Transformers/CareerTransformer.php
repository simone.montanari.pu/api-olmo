<?php

namespace App\Containers\Backoffice\Career\UI\API\Transformers;

use App\Containers\Backoffice\Career\Models\Career;
use App\Ship\Parents\Transformers\Transformer;

class CareerTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Career $career): array
    {
        $response = [
            'object' => $career->getResourceKey(),
            'id' => $career->getHashedKey(),
            'created_at' => $career->created_at,
            'updated_at' => $career->updated_at,
            'readable_created_at' => $career->created_at->diffForHumans(),
            'readable_updated_at' => $career->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $career->id,
            // 'deleted_at' => $career->deleted_at,
        ], $response);

        return $response;
    }
}
