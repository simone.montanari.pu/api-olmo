<?php

/**
 * @apiGroup           Career
 * @apiName            findCareerById
 *
 * @api                {GET} /v1/careers/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Career\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('careers/{id}', [Controller::class, 'findCareerById'])
    ->name('api_career_find_career_by_id')
    ->middleware(['auth:api']);

