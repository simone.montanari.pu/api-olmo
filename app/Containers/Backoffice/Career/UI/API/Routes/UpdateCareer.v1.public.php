<?php

/**
 * @apiGroup           Career
 * @apiName            updateCareer
 *
 * @api                {PATCH} /v1/careers/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Career\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::patch('careers/{id}', [Controller::class, 'updateCareer'])
    ->name('api_career_update_career')
    ->middleware(['auth:api']);

