<?php

/**
 * @apiGroup           Career
 * @apiName            getAllCareers
 *
 * @api                {GET} /v1/careers Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Career\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('careers', [Controller::class, 'getAllCareers'])
    ->name('api_career_get_all_careers')
    ->middleware(['auth:api']);

