<?php

/**
 * @apiGroup           Career
 * @apiName            createCareer
 *
 * @api                {POST} /v1/careers Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Career\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::post('careers', [Controller::class, 'createCareer'])
    ->name('api_career_create_career')
    ->middleware(['auth:api']);

