<?php

/**
 * @apiGroup           Career
 * @apiName            deleteCareer
 *
 * @api                {DELETE} /v1/careers/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Career\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::delete('careers/{id}', [Controller::class, 'deleteCareer'])
    ->name('api_career_delete_career')
    ->middleware(['auth:api']);

