<?php

namespace App\Containers\Backoffice\Career\Models;

use App\Ship\Parents\Models\Model;

class Career extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $table = 'olmo_career';

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Career';

    public static function getAttr(){
        return ['table' => 'olmo_career'];
    }

}
