<?php

namespace App\Containers\Backoffice\Career\Tasks;

use App\Containers\Backoffice\Career\Data\Repositories\CareerRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllCareersTask extends Task
{
    protected CareerRepository $repository;

    public function __construct(CareerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
