<?php

namespace App\Containers\Backoffice\Career\Tasks;

use App\Containers\Backoffice\Career\Data\Repositories\CareerRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindCareerByIdTask extends Task
{
    protected CareerRepository $repository;

    public function __construct(CareerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
