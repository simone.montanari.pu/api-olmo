<?php

namespace App\Containers\Backoffice\Career\Tasks;

use App\Containers\Backoffice\Career\Data\Repositories\CareerRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteCareerTask extends Task
{
    protected CareerRepository $repository;

    public function __construct(CareerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id): ?int
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
