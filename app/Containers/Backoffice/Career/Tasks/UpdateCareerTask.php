<?php

namespace App\Containers\Backoffice\Career\Tasks;

use App\Containers\Backoffice\Career\Data\Repositories\CareerRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateCareerTask extends Task
{
    protected CareerRepository $repository;

    public function __construct(CareerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
