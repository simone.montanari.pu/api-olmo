<?php

namespace App\Containers\Backoffice\Career\Tasks;

use App\Containers\Backoffice\Career\Data\Repositories\CareerRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateCareerTask extends Task
{
    protected CareerRepository $repository;

    public function __construct(CareerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
