<?php

namespace App\Containers\Backoffice\Career\Actions;

use App\Containers\Backoffice\Career\Models\Career;
use App\Containers\Backoffice\Career\Tasks\CreateCareerTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateCareerAction extends Action
{
    public function run(Request $request): Career
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateCareerTask::class)->run($data);
    }
}
