<?php

namespace App\Containers\Backoffice\Career\Actions;

use App\Containers\Backoffice\Career\Tasks\DeleteCareerTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteCareerAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteCareerTask::class)->run($request->id);
    }
}
