<?php

namespace App\Containers\Backoffice\Career\Actions;

use App\Containers\Backoffice\Career\Models\Career;
use App\Containers\Backoffice\Career\Tasks\FindCareerByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindCareerByIdAction extends Action
{
    public function run(Request $request): Career
    {
        return app(FindCareerByIdTask::class)->run($request->id);
    }
}
