<?php

namespace App\Containers\Backoffice\Career\Actions;

use App\Containers\Backoffice\Career\Models\Career;
use App\Containers\Backoffice\Career\Tasks\UpdateCareerTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateCareerAction extends Action
{
    public function run(Request $request): Career
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateCareerTask::class)->run($request->id, $data);
    }
}
