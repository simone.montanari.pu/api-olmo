<?php

namespace App\Containers\Backoffice\Career\Actions;

use App\Containers\Backoffice\Career\Tasks\GetAllCareersTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllCareersAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllCareersTask::class)->addRequestCriteria()->run();
    }
}
