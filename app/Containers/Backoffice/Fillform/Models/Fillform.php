<?php

namespace App\Containers\Backoffice\Fillform\Models;

use App\Ship\Parents\Models\Model;

use Illuminate\Support\Facades\Http;

class Fillform extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Fillform';

    public $table = 'olmo_fillform';

 

    public static function sendMailFillform($formid,$params = array()) {

        $token = env('FILLFORM_TOKEN');
        $url = 'https://'.env('FILLFORM_API_URL').$token.'/sendMailV2/'.$formid;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;

    }


    public static function getAttr(){
        return [
            'table' => 'olmo_fillform'
        ];
    }
}
