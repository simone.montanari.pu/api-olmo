<?php

namespace App\Containers\Backoffice\Fillform\Actions;

use App\Containers\Backoffice\Fillform\Models\Fillform;
use App\Containers\Backoffice\Fillform\Tasks\CreateFillformTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateFillformAction extends Action
{
    public function run(Request $request): Fillform
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateFillformTask::class)->run($data);
    }
}
