<?php

namespace App\Containers\Backoffice\Fillform\Actions;

use App\Containers\Backoffice\Fillform\Models\Fillform;
use App\Containers\Backoffice\Fillform\Tasks\UpdateFillformTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateFillformAction extends Action
{
    public function run(Request $request): Fillform
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateFillformTask::class)->run($request->id, $data);
    }
}
