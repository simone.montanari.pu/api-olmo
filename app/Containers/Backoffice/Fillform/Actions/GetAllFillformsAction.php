<?php

namespace App\Containers\Backoffice\Fillform\Actions;

use App\Containers\Backoffice\Fillform\Tasks\GetAllFillformsTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllFillformsAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllFillformsTask::class)->addRequestCriteria()->run();
    }
}
