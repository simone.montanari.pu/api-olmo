<?php

namespace App\Containers\Backoffice\Fillform\Actions;

use App\Containers\Backoffice\Fillform\Models\Fillform;
use App\Containers\Backoffice\Fillform\Tasks\FindFillformByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindFillformByIdAction extends Action
{
    public function run(Request $request): Fillform
    {
        return app(FindFillformByIdTask::class)->run($request->id);
    }
}
