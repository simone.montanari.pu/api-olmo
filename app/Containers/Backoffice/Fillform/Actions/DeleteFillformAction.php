<?php

namespace App\Containers\Backoffice\Fillform\Actions;

use App\Containers\Backoffice\Fillform\Tasks\DeleteFillformTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteFillformAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteFillformTask::class)->run($request->id);
    }
}
