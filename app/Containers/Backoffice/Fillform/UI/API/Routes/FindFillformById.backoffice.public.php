<?php

/**
 * @apiGroup           Fillform
 * @apiName            findFillformById
 *
 * @api                {GET} /v1/fillforms/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Fillform\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('{lang}/fillform/{id}', [Controller::class, 'findById']) ->name('api_fillform_find_fillform_by_id')->middleware(['auth:api']);



