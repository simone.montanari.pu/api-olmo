<?php

namespace App\Containers\Backoffice\Fillform\UI\API\Transformers;

use App\Containers\Backoffice\Fillform\Models\Fillform;
use App\Ship\Parents\Transformers\Transformer;

class FillformTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Fillform $fillform): array
    {
        $response = [
            'object' => $fillform->getResourceKey(),
            'id' => $fillform->getHashedKey(),
            'created_at' => $fillform->created_at,
            'updated_at' => $fillform->updated_at,
            'readable_created_at' => $fillform->created_at->diffForHumans(),
            'readable_updated_at' => $fillform->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $fillform->id,
            // 'deleted_at' => $fillform->deleted_at,
        ], $response);

        return $response;
    }
}
