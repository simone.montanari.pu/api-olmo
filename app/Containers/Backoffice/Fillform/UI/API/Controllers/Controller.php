<?php

namespace App\Containers\Backoffice\Fillform\UI\API\Controllers;

use App\Containers\Backoffice\Fillform\UI\API\Requests\CreateFillformRequest;
use App\Containers\Backoffice\Fillform\UI\API\Requests\DeleteFillformRequest;
use App\Containers\Backoffice\Fillform\UI\API\Requests\GetAllFillformsRequest;
use App\Containers\Backoffice\Fillform\UI\API\Requests\FindFillformByIdRequest;
use App\Containers\Backoffice\Fillform\UI\API\Requests\UpdateFillformRequest;
use App\Containers\Backoffice\Fillform\UI\API\Transformers\FillformTransformer;
use App\Containers\Backoffice\Fillform\Actions\CreateFillformAction;
use App\Containers\Backoffice\Fillform\Actions\FindFillformByIdAction;
use App\Containers\Backoffice\Fillform\Actions\GetAllFillformsAction;
use App\Containers\Backoffice\Fillform\Actions\UpdateFillformAction;
use App\Containers\Backoffice\Fillform\Actions\DeleteFillformAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use App\Containers\Backoffice\Fillform\Models\Fillform;
use App\Ship\Parents\Controllers\ApiHelper;
use Illuminate\Support\Facades\DB;

class Controller extends ApiController
{
    public function getListV1(GetAllFillformsRequest $request)
    {
        $lang = $request->lang;
    	$datas   = Fillform::select('name_txt_general','formtype_select_general as formtype_txt_general','code_txt_general','id')
        ->get()->toArray();

        $attribute  = Fillform::getAttr();
        return  $this->getList($datas,$attribute);

    }

    public function testSendMail(GetAllFillformsRequest $request)
    {
        $data             = DB::table('olmo_customer')->where('id',1)->get()->toArray();
        $formIdRegister   = DB::table('olmo_fillform')->where('formtype_select_general','customercreation')->first()->code_txt_general;
        $res = Fillform::sendMailFillform($formIdRegister,$data[0]);
        return $res;
    }



    public function createUpdateV1(GetAllFillformsRequest $request)
    {
    	$attribute  = Fillform::getAttr();
    	return $this->createUpdate($request,$attribute);
    }

    public function findById(GetAllFillformsRequest $request)
    { 
        $lang       = $request->lang;
        $attribute  = Fillform::getAttr();
        $id         = $request->id;
        if($id == 0){
            return $this->emptyItem($attribute,$lang);
        }else{
           $datas      = Fillform::where('id',$id)->get()->toArray();
           $attribute  = Fillform::getAttr();
           return  $this->getSingleList($datas,$attribute,$id,$lang);
        }
    }

}
