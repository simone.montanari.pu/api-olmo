<?php

namespace App\Containers\Backoffice\Fillform\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class FillformRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
