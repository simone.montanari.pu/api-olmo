<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFillformsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('olmo_fillform')) {
            Schema::table('olmo_fillform', function (Blueprint $table) {
                // Update existing table...
            });
        } else {
            Schema::create('olmo_fillform', function (Blueprint $table) {
                $table->charset = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';
                $table->increments('id')->unsigned();
                $table->text('code_txt_general')->nullable();
                $table->text('name_txt_general')->nullable();
                $table->text('formtype_select_general')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('fillforms');
    }
}



