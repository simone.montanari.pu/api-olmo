<?php

namespace App\Containers\Backoffice\Fillform\Tasks;

use App\Containers\Backoffice\Fillform\Data\Repositories\FillformRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllFillformsTask extends Task
{
    protected FillformRepository $repository;

    public function __construct(FillformRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
