<?php

namespace App\Containers\Backoffice\Fillform\Tasks;

use App\Containers\Backoffice\Fillform\Data\Repositories\FillformRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateFillformTask extends Task
{
    protected FillformRepository $repository;

    public function __construct(FillformRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
