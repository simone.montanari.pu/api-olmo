<?php

namespace App\Containers\Backoffice\Fillform\Tasks;

use App\Containers\Backoffice\Fillform\Data\Repositories\FillformRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindFillformByIdTask extends Task
{
    protected FillformRepository $repository;

    public function __construct(FillformRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
