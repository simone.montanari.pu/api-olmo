<?php

namespace App\Containers\Backoffice\Fillform\Tasks;

use App\Containers\Backoffice\Fillform\Data\Repositories\FillformRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteFillformTask extends Task
{
    protected FillformRepository $repository;

    public function __construct(FillformRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id): ?int
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
