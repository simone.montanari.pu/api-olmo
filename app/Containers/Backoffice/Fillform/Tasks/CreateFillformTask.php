<?php

namespace App\Containers\Backoffice\Fillform\Tasks;

use App\Containers\Backoffice\Fillform\Data\Repositories\FillformRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateFillformTask extends Task
{
    protected FillformRepository $repository;

    public function __construct(FillformRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
