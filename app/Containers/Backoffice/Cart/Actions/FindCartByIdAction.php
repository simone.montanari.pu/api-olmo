<?php

namespace App\Containers\Backoffice\Cart\Actions;

use App\Containers\Backoffice\Cart\Models\Cart;
use App\Containers\Backoffice\Cart\Tasks\FindCartByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindCartByIdAction extends Action
{
    public function run(Request $request): Cart
    {
        return app(FindCartByIdTask::class)->run($request->id);
    }
}
