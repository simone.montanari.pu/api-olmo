<?php

namespace App\Containers\Backoffice\Cart\Actions;

use App\Containers\Backoffice\Cart\Tasks\DeleteCartTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteCartAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteCartTask::class)->run($request->id);
    }
}
