<?php

namespace App\Containers\Backoffice\Cart\Actions;

use App\Containers\Backoffice\Cart\Models\Cart;
use App\Containers\Backoffice\Cart\Tasks\UpdateCartTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateCartAction extends Action
{
    public function run(Request $request): Cart
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateCartTask::class)->run($request->id, $data);
    }
}
