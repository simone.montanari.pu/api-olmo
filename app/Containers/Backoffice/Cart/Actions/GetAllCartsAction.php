<?php

namespace App\Containers\Backoffice\Cart\Actions;

use App\Containers\Backoffice\Cart\Tasks\GetAllCartsTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllCartsAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllCartsTask::class)->addRequestCriteria()->run();
    }
}
