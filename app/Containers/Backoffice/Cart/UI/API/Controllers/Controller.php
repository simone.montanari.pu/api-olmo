<?php

namespace App\Containers\Backoffice\Cart\UI\API\Controllers;

use App\Containers\Backoffice\Cart\UI\API\Requests\CreateCartRequest;
use App\Containers\Backoffice\Cart\UI\API\Requests\DeleteCartRequest;
use App\Containers\Backoffice\Cart\UI\API\Requests\GetAllCartsRequest;
use App\Containers\Backoffice\Cart\UI\API\Requests\FindCartByIdRequest;
use App\Containers\Backoffice\Cart\UI\API\Requests\UpdateCartRequest;
use App\Containers\Backoffice\Cart\UI\API\Transformers\CartTransformer;
use App\Containers\Backoffice\Cart\Actions\CreateCartAction;
use App\Containers\Backoffice\Cart\Actions\FindCartByIdAction;
use App\Containers\Backoffice\Cart\Actions\GetAllCartsAction;
use App\Containers\Backoffice\Cart\Actions\UpdateCartAction;
use App\Containers\Backoffice\Cart\Actions\DeleteCartAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;

class Controller extends ApiController
{
    public function createCart(CreateCartRequest $request): JsonResponse
    {
        $cart = app(CreateCartAction::class)->run($request);
        return $this->created($this->transform($cart, CartTransformer::class));
    }

    public function findCartById(FindCartByIdRequest $request): array
    {
        $cart = app(FindCartByIdAction::class)->run($request);
        return $this->transform($cart, CartTransformer::class);
    }

    public function getAllCarts(GetAllCartsRequest $request): array
    {
        $carts = app(GetAllCartsAction::class)->run($request);
        return $this->transform($carts, CartTransformer::class);
    }

    public function updateCart(UpdateCartRequest $request): array
    {
        $cart = app(UpdateCartAction::class)->run($request);
        return $this->transform($cart, CartTransformer::class);
    }

    public function deleteCart(DeleteCartRequest $request): JsonResponse
    {
        app(DeleteCartAction::class)->run($request);
        return $this->noContent();
    }
}
