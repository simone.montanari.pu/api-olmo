<?php

namespace App\Containers\Backoffice\Cart\UI\API\Transformers;

use App\Containers\Backoffice\Cart\Models\Cart;
use App\Ship\Parents\Transformers\Transformer;

class CartTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Cart $cart): array
    {
        $response = [
            'object' => $cart->getResourceKey(),
            'id' => $cart->getHashedKey(),
            'created_at' => $cart->created_at,
            'updated_at' => $cart->updated_at,
            'readable_created_at' => $cart->created_at->diffForHumans(),
            'readable_updated_at' => $cart->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $cart->id,
            // 'deleted_at' => $cart->deleted_at,
        ], $response);

        return $response;
    }
}
