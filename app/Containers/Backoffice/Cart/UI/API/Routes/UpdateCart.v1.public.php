<?php

/**
 * @apiGroup           Cart
 * @apiName            updateCart
 *
 * @api                {PATCH} /v1/shoppingcart/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Cart\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::patch('shoppingcart/{id}', [Controller::class, 'updateCart'])
    ->name('api_cart_update_cart')
    ->middleware(['auth:api']);

