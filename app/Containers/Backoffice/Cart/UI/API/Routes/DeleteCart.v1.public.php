<?php

/**
 * @apiGroup           Cart
 * @apiName            deleteCart
 *
 * @api                {DELETE} /v1/shoppingcart/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Cart\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::delete('shoppingcart/{id}', [Controller::class, 'deleteCart'])
    ->name('api_cart_delete_cart')
    ->middleware(['auth:api']);

