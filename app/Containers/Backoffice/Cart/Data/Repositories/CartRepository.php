<?php

namespace App\Containers\Backoffice\Cart\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class CartRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
