<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Ship\Parents\Controllers\ApiMigration;

class CreateCartsitemsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('olmo_cartitem')) {
            Schema::table('olmo_cartitem', function (Blueprint $table) {
                // Update existing table...
            });
        } else {
            Schema::create('olmo_cartitem', function (Blueprint $table) {
                // Create new table...
                $table->charset = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';
                // General
                $table->increments('id')->unsigned();
                $table->text('cart_id_general')->nullable();
                $table->text('model_id_general')->nullable();
                $table->text('model_select_general')->nullable();
                $table->text('quantity_num_general')->nullable();
                $table->text('customer_id_general')->nullable();
                $table->text('property_list_general')->nullable();

            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_cartitem');
    }
}
