<?php

namespace App\Containers\Backoffice\Cart\Tasks;

use App\Containers\Backoffice\Cart\Data\Repositories\CartRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllCartsTask extends Task
{
    protected CartRepository $repository;

    public function __construct(CartRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
