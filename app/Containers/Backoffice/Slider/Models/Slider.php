<?php

namespace App\Containers\Backoffice\Slider\Models;

use App\Ship\Parents\Models\Model;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class Slider extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Slider';

    public $table = 'olmo_slideritem';


    public static function getAttr(){
        return [
            'table' => 'olmo_slideritem',
        ];
    }

    public static function getItemslider($id){
            return Db::table('olmo_slideritem')->where('slider_id',$id)->get();
    }
}
