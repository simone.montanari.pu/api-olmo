<?php

namespace App\Containers\Backoffice\Slider\UI\API\Transformers;

use App\Containers\Backoffice\Slider\Models\Slider;
use App\Ship\Parents\Transformers\Transformer;

class SliderTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Slider $slider): array
    {
        $response = [
            'object' => $slider->getResourceKey(),
            'id' => $slider->getHashedKey(),
            'created_at' => $slider->created_at,
            'updated_at' => $slider->updated_at,
            'readable_created_at' => $slider->created_at->diffForHumans(),
            'readable_updated_at' => $slider->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $slider->id,
            // 'deleted_at' => $slider->deleted_at,
        ], $response);

        return $response;
    }
}
