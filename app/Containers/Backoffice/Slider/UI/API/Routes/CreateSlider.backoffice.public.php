<?php

/**
 * @apiGroup           Slider
 * @apiName            createSlider
 *
 * @api                {POST} /v1/slider Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Slider\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::put('en/{model}/{id_model}/slider/{name_slider}/{id_slider}', [Controller::class, 'createUpdateV1'])->middleware(['auth:api']);

