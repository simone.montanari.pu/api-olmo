<?php

/**
 * @apiGroup           Slider
 * @apiName            updateSlider
 *
 * @api                {PATCH} /v1/slider/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Slider\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::patch('slider/{id}', [Controller::class, 'updateSlider'])
    ->name('api_slider_update_slider')
    ->middleware(['auth:api']);

