<?php

namespace App\Containers\Backoffice\Slider\UI\API\Controllers;

use App\Containers\Backoffice\Slider\UI\API\Requests\CreateSliderRequest;
use App\Containers\Backoffice\Slider\UI\API\Requests\DeleteSliderRequest;
use App\Containers\Backoffice\Slider\UI\API\Requests\GetAllSlidersRequest;
use App\Containers\Backoffice\Slider\UI\API\Requests\FindSliderByIdRequest;
use App\Containers\Backoffice\Slider\UI\API\Requests\UpdateSliderRequest;
use App\Containers\Backoffice\Slider\UI\API\Transformers\SliderTransformer;
use App\Containers\Backoffice\Slider\Actions\CreateSliderAction;
use App\Containers\Backoffice\Slider\Actions\FindSliderByIdAction;
use App\Containers\Backoffice\Slider\Actions\GetAllSlidersAction;
use App\Containers\Backoffice\Slider\Actions\UpdateSliderAction;
use App\Containers\Backoffice\Slider\Actions\DeleteSliderAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use App\Containers\Backoffice\Slider\Models\Slider;
use Illuminate\Support\Facades\DB;

class Controller extends ApiController
{

    public function getListV1(CreateSliderRequest $request)
    {
        $lang = $request->lang;
    	$datas   = Slider::select('name_txt_general','enabled_is_general','id')
        ->get()->toArray();
        $attribute  = Slider::getAttr();
        return  $this->getList($datas,$attribute);

    }


    public function createUpdateV1(CreateSliderRequest $request)
    {
    	$attribute   =  Slider::getAttr();
        $name_slider =  $request->name_slider;
        $model       =  $request->model;
        $table       = 'olmo_'.$model;
        $id_model    =  $request->id_model;
        $id_slider   =  $request->id_slider;
        $request->id =  $id_slider;
    	$res         =  $this->createUpdate($request,$attribute);
        $id_slider   =  $res[0]['value'];
        
        //Salvo id slide sul table ( model entity)
        $current_val =   @Db::table($table)->where('id',$id_model)->first()->{$name_slider};

  
        $check = false;
        if($current_val != null){
            $values = explode(',',$current_val);
            if(in_array($id_slider,$values)){
                $check = true;
            }
        }

        //return [$table,$current_val,$check,$id_model,$id_slider];
        if($check == false){
            if($current_val == null ){
                Db::table($table)->where('id',$id_model)->update([$name_slider => $id_slider ]);
            }
            else{
                Db::table($table)->where('id',$id_model)->update([$name_slider => $current_val.','.$id_slider]);
            }
        }
        
        //Ritorno l'entita del modello
        $lang       = $request->lang;
        $attribute  =   ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model))::getAttr();
        $id         = $id_model;
  

    }

    public function findById(CreateSliderRequest $request)
    { 
        $lang       = $request->lang;
        $attribute  = Slider::getAttr();
        $id         = $request->id;
        if($id == 0){
            return $this->emptyItem($attribute,$lang);
        }else{
           $datas      = Slider::where('id',$id)->get()->toArray();
           $attribute  = Slider::getAttr();
           return  $this->getSingleList($datas,$attribute,$id,$lang);
        }
    }


    
}
