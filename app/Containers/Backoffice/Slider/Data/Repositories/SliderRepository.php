<?php

namespace App\Containers\Backoffice\Slider\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class SliderRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
