<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('olmo_slider')) {
            Schema::table('olmo_slider', function (Blueprint $table) {

            });
        } else {
            Schema::create('olmo_slider', function (Blueprint $table) {                
                // Create new table...
                $table->charset = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';
                // General
                $table->id();
                $table->text('name_txt_general')->nullable();
                $table->timestamps();
                //$table->softDeletes();
            });
        }        
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sliders');
    }
}
