<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class OlmoSlideritem extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('olmo_slideritem')) {
            Schema::table('olmo_slider', function (Blueprint $table) {

            });
        } else {
            Schema::create('olmo_slideritem', function (Blueprint $table) {
                // Create new table...
                $table->charset = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';
                // General
                $table->id();
                $table->text('name_txt_content')->nullable();
                $table->text('primary_filemanager_content')->nullable();
                $table->text('secondary_filemanager_content')->nullable();
                $table->text('pretitle_editor_content')->nullable();
                $table->text('title_editor_content')->nullable();
                $table->text('subtitle_editor_content')->nullable();
                $table->text('cta_txt_content')->nullable();
                $table->text('link_txt_content')->nullable();
                $table->timestamps();
                //$table->softDeletes();
            });
        }        
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_slideritem');
    }
}
