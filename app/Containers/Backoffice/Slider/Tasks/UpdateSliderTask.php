<?php

namespace App\Containers\Backoffice\Slider\Tasks;

use App\Containers\Backoffice\Slider\Data\Repositories\SliderRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateSliderTask extends Task
{
    protected SliderRepository $repository;

    public function __construct(SliderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
