<?php

namespace App\Containers\Backoffice\Slider\Tasks;

use App\Containers\Backoffice\Slider\Data\Repositories\SliderRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteSliderTask extends Task
{
    protected SliderRepository $repository;

    public function __construct(SliderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id): ?int
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
