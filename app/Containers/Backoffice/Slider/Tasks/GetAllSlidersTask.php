<?php

namespace App\Containers\Backoffice\Slider\Tasks;

use App\Containers\Backoffice\Slider\Data\Repositories\SliderRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllSlidersTask extends Task
{
    protected SliderRepository $repository;

    public function __construct(SliderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
