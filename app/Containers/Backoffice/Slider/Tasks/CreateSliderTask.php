<?php

namespace App\Containers\Backoffice\Slider\Tasks;

use App\Containers\Backoffice\Slider\Data\Repositories\SliderRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateSliderTask extends Task
{
    protected SliderRepository $repository;

    public function __construct(SliderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
