<?php

namespace App\Containers\Backoffice\Slider\Actions;

use App\Containers\Backoffice\Slider\Models\Slider;
use App\Containers\Backoffice\Slider\Tasks\FindSliderByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindSliderByIdAction extends Action
{
    public function run(Request $request): Slider
    {
        return app(FindSliderByIdTask::class)->run($request->id);
    }
}
