<?php

namespace App\Containers\Backoffice\Slider\Actions;

use App\Containers\Backoffice\Slider\Models\Slider;
use App\Containers\Backoffice\Slider\Tasks\CreateSliderTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateSliderAction extends Action
{
    public function run(Request $request): Slider
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateSliderTask::class)->run($data);
    }
}
