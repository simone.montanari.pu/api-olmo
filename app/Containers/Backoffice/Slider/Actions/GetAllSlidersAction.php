<?php

namespace App\Containers\Backoffice\Slider\Actions;

use App\Containers\Backoffice\Slider\Tasks\GetAllSlidersTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllSlidersAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllSlidersTask::class)->addRequestCriteria()->run();
    }
}
