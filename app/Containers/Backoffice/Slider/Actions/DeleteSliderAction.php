<?php

namespace App\Containers\Backoffice\Slider\Actions;

use App\Containers\Backoffice\Slider\Tasks\DeleteSliderTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteSliderAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteSliderTask::class)->run($request->id);
    }
}
