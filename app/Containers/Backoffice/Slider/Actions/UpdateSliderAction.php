<?php

namespace App\Containers\Backoffice\Slider\Actions;

use App\Containers\Backoffice\Slider\Models\Slider;
use App\Containers\Backoffice\Slider\Tasks\UpdateSliderTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateSliderAction extends Action
{
    public function run(Request $request): Slider
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateSliderTask::class)->run($request->id, $data);
    }
}
