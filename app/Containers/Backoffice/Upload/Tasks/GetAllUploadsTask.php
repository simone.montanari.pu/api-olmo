<?php

namespace App\Containers\Backoffice\Upload\Tasks;

use App\Containers\Backoffice\Upload\Data\Repositories\UploadRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllUploadsTask extends Task
{
    protected UploadRepository $repository;

    public function __construct(UploadRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
