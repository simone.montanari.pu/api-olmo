<?php

namespace App\Containers\Backoffice\Upload\Actions;

use App\Containers\Backoffice\Upload\Models\Upload;
use App\Containers\Backoffice\Upload\Tasks\UpdateUploadTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateUploadAction extends Action
{
    public function run(Request $request): Upload
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateUploadTask::class)->run($request->id, $data);
    }
}
