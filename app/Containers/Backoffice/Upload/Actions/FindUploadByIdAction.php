<?php

namespace App\Containers\Backoffice\Upload\Actions;

use App\Containers\Backoffice\Upload\Models\Upload;
use App\Containers\Backoffice\Upload\Tasks\FindUploadByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindUploadByIdAction extends Action
{
    public function run(Request $request): Upload
    {
        return app(FindUploadByIdTask::class)->run($request->id);
    }
}
