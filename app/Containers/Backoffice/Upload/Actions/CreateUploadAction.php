<?php

namespace App\Containers\Backoffice\Upload\Actions;

use App\Containers\Backoffice\Upload\Models\Upload;
use App\Containers\Backoffice\Upload\Tasks\CreateUploadTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateUploadAction extends Action
{
    public function run(Request $request): Upload
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateUploadTask::class)->run($data);
    }
}
