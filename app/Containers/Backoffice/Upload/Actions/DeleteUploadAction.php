<?php

namespace App\Containers\Backoffice\Upload\Actions;

use App\Containers\Backoffice\Upload\Tasks\DeleteUploadTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteUploadAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteUploadTask::class)->run($request->id);
    }
}
