<?php

namespace App\Containers\Backoffice\Upload\Models;

use App\Ship\Parents\Models\Model;

class Upload extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Upload';
}
