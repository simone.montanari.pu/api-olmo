<?php

namespace App\Containers\Backoffice\Upload\UI\API\Controllers;

use App\Containers\Backoffice\Upload\UI\API\Requests\CreateUploadRequest;
use App\Containers\Backoffice\Upload\UI\API\Requests\DeleteUploadRequest;
use App\Containers\Backoffice\Upload\UI\API\Requests\GetAllUploadsRequest;
use App\Containers\Backoffice\Upload\UI\API\Requests\FindUploadByIdRequest;
use App\Containers\Backoffice\Upload\UI\API\Requests\UpdateUploadRequest;
use App\Containers\Backoffice\Upload\UI\API\Transformers\UploadTransformer;
use App\Containers\Backoffice\Upload\Actions\CreateUploadAction;
use App\Containers\Backoffice\Upload\Actions\FindUploadByIdAction;
use App\Containers\Backoffice\Upload\Actions\GetAllUploadsAction;
use App\Containers\Backoffice\Upload\Actions\UpdateUploadAction;
use App\Containers\Backoffice\Upload\Actions\DeleteUploadAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class Controller extends ApiController
{
   

        public function testUpload(GetAllUploadsRequest $request)
        {
           header("Content-type:application/jpeg");

           header("Content-Disposition:attachment;filename=luna.jpg");

           return Storage::get('public/img1.jpg');

        }

        public function uploadChunk(GetAllUploadsRequest $request)
        {

             $chunk_id     = @$_REQUEST['id'];
             $filename     = @$_REQUEST['fileName'];
             $data         = file_get_contents('php://input');
             $temp_data    = Cache::get($filename);
             $current_data = $temp_data.$data;
             Cache::put($filename,$current_data,1200);

             $arr['data']         = null;
             $arr['isSuccess']    = true;
             $arr['errorMessage'] = null;

             return $arr;


        }


        public function completeChunk(GetAllUploadsRequest $request)
        {

             $chunk_id     = @$request->id;


             $filename     = @$_REQUEST['fileName'];
             $truename     = @$_REQUEST['trueName'];
             $fieldname    = @$_REQUEST['fieldName'];
             $current_data = Cache::get($filename);

             $pagename     =  $request->pagename;
             $type = pathinfo($truename, PATHINFO_EXTENSION);

           

             $categories_files = [
               'jpg'  => 'images',
               'jpeg' => 'images',
               'png'  =>  'images',
               'pdf'  => 'documents',
               'gif'  => 'images',
               'webm' => 'images',
               'txt'  => 'documents',
               'mp4'  => 'media'
             ];
            
        
             $path  = 'public/'.$categories_files[$type].'/'.$pagename.'/'.$filename;
             $res   = Storage::put($path, $current_data);

             $public_path = '/'.$categories_files[$type].'/'.$pagename.'/'.$truename;

             if($res){
                $res = true;
             }else{
                $res = false;
             }
           


            $storage_id = DB::table('olmo_storage')->insertGetId([
               'filename' 	=> $filename,
               'truename' 	=> $truename,
               'model'     => $pagename,
               'meta'     => $chunk_id.$fieldname,
               'public'    => 'true',
               'type'      => $type
            ]);


            if(strpos($fieldname,'_multimg')    !== false) { 

               //storage id incrementale
               $entity     = DB::table('olmo_'.$pagename)->where('id',$chunk_id)->first();

               if($entity){
                  $storage_id = $entity->{$fieldname}.','.$storage_id;
                  $storage_id = trim($storage_id,",");
               }

               $update = DB::table('olmo_'.$pagename)->where('id',$chunk_id)->update([
                  $fieldname 	=> $storage_id
               ]);   

            }else{
               $update = DB::table('olmo_'.$pagename)->where('id',$chunk_id)->update([
                  $fieldname 	=> $storage_id
               ]);
            }

            $arr['data']         = $public_path;
            $arr['id']           = $storage_id;
            $arr['isSuccess']    = $res;
            $arr['isUpdate']     = $update;
            $arr['errorMessage'] = null;

            Cache::forget($filename);
            return $arr;

        }

}
