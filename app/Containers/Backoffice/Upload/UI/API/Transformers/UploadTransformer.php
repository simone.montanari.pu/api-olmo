<?php

namespace App\Containers\Backoffice\Upload\UI\API\Transformers;

use App\Containers\Backoffice\Upload\Models\Upload;
use App\Ship\Parents\Transformers\Transformer;

class UploadTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Upload $upload): array
    {
        $response = [
            'object' => $upload->getResourceKey(),
            'id' => $upload->getHashedKey(),
            'created_at' => $upload->created_at,
            'updated_at' => $upload->updated_at,
            'readable_created_at' => $upload->created_at->diffForHumans(),
            'readable_updated_at' => $upload->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $upload->id,
            // 'deleted_at' => $upload->deleted_at,
        ], $response);

        return $response;
    }
}
