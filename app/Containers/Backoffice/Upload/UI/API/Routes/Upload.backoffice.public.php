<?php

/**
 * @apiGroup           Upload
 * @apiName            updateUpload
 *
 * @api                {PATCH} /v1/uploads/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Upload\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::post('{lang}/{pagename}/{id}/uploadchunks', [Controller::class, 'uploadChunk']);

Route::post('{lang}/{pagename}/{id}/uploadcomplete', [Controller::class, 'completeChunk']);

//Route::get('storage/{path}/{truename}', [Controller::class, 'testUpload']);