<?php

/**
 * @apiGroup           User
 * @apiName            createUser
 *
 * @api                {POST} /v1/olmo_users Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\User\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::post('auth/login', [Controller::class, 'createSessionlogin']);

Route::put('{lang}/user/{id}', [Controller::class, 'createUpdateV1'])->middleware(['auth:api']);

