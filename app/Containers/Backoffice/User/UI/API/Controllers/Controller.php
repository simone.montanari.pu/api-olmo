<?php

namespace App\Containers\Backoffice\User\UI\API\Controllers;

use App\Containers\Backoffice\User\UI\API\Requests\CreateUserRequest;
use App\Containers\Backoffice\User\UI\API\Requests\DeleteUserRequest;
use App\Containers\Backoffice\User\UI\API\Requests\GetAllUsersRequest;
use App\Containers\Backoffice\User\UI\API\Requests\FindUserByIdRequest;
use App\Containers\Backoffice\User\UI\API\Requests\UpdateUserRequest;
use App\Containers\Backoffice\User\UI\API\Transformers\UserTransformer;
use App\Containers\Backoffice\User\Actions\CreateUserAction;
use App\Containers\Backoffice\User\Actions\FindUserByIdAction;
use App\Containers\Backoffice\User\Actions\GetAllUsersAction;
use App\Containers\Backoffice\User\Actions\UpdateUserAction;
use App\Containers\Backoffice\User\Actions\DeleteUserAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use App\Containers\Backoffice\User\Models\User;
use App\Ship\Parents\Controllers\ApiHelper;

class Controller extends ApiController
{


    public function findById(CreateUserRequest $request)
    { 
        $attribute  = User::getAttr();
       $id    = $request->id;
        if($id == 0){
            return $this->emptyItem($attribute);
        }else{
         $datas      = User::where('id',$id)->get()->toArray();
           $attribute  = User::getAttr();
           return  $this->getSingleList($datas,$attribute,$id);
        }
    }


    public function getListV1(CreateUserRequest $request)
    {

           $header = getallheaders();
        $X_token =  @$header['x-token'];
        if($X_token == ''){
            $X_token =  @$header['X-Token'];
        }

      $role_id = User::where('token_hidden_general',$X_token)->first();

      $role_id = $role_id->role_id_general;
      $datas      = User::select('email_email_general','enabled_is_general','role_id_general','id')->where('role_id_general','>=',$role_id)->get()->toArray();
        $attribute  = User::getAttr();
        return  $this->getList($datas,$attribute);

    }


    public function createUpdateV1(CreateUserRequest $request)
    {

    	$attribute   = User::getAttr();
        $rules       = $attribute['rules'];
        
        if(isset($attribute['requiredbackoffice'])){
            
            $required    = $attribute['requiredbackoffice'];

            if($required){
                $res = ApiHelper::CheckRequired($request,$required);
                if($res['valid'] == false){
                    return response($res['err'], 400);
                    exit();
                }
            }

        }
        
        if($rules){
            $res = ApiHelper::CheckRules($request,$rules);
            if($res['valid'] == false){
                return response($res['err'], 400);
                exit();
            }
        }        

        return $this->createUpdate($request,$attribute);
    }


    public function createSessionlogin(CreateUserRequest $request)
    {
        $username    = $request->username;
        $password    = $request->password;

        $user = @User::where('email_email_general',$username)->where('password_pwd_general',$password)
        ->select(
            'id',
            'email_email_general as email',
            'email_email_general as username',
            'role_id_general as role_id',
            'avatar_img_general as avatar',
            'name_txt_general as name'
            )
        ->first();

        if(!($user))
        {
            return response(["User not found"], 404);
            exit();
        }

        $table_relation = env('TABLE_PREFIX').'_role';
        $user['role']  = $this->currentRelation($table_relation,$user->role_id,'select')['key'];
        $token = md5(time().'OLMO');
        User::where('id',$user->id)->update(['token_hidden_general' => $token ]);
        $user['token'] = $token;
        $user['avatar'] = ApiHelper::getSinglePathStorageById($user['avatar']);
        $user['name'] = $user['name'];
        unset($user['id']);
        unset($user['role_id']);
        echo json_encode($user);
    }



    public function removeSessionlogin(CreateUserRequest $request)
    {
       $token    = $request->token;

       $res = User::where('token_hidden_general',$token)->update(['token_hidden_general' => '']);

        if(!($res)){
             return response('Fail logout', 401);
        }else{
            return response('Success logout', 200);
        }

       exit();
    
    }

}
