<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('olmo_user')) {
            Schema::table('olmo_user', function (Blueprint $table) {
                // Update existing table...
            });
        } else {
            Schema::create('olmo_user', function (Blueprint $table) {
                // Create new table...
                $table->charset = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';
                // General
                $table->increments('id')->unsigned();
                $table->string('enabled_is_general', 5)->nullable();
                $table->text('name_txt_general')->nullable();
                $table->text('email_email_general')->nullable();
                $table->text('password_pwd_general')->nullable();
                $table->text('confirmpassword_pwd_general')->nullable();
                $table->text('role_id_general')->nullable();
                $table->text('avatar_img_general')->nullable();
                $table->text('token_hidden_general')->nullable();
                $table->text('updated_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_user');
    }
}
