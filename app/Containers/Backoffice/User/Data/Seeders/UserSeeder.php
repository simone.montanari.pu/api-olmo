<?php

namespace App\Containers\Backoffice\User\Data\Seeders;

use App\Ship\Parents\Seeders\Seeder;
use Illuminate\Support\Facades\DB;


class UserSeeder extends Seeder
{
    public function run()
    {

       	DB::table('olmo_user')->truncate();
       
		DB::table('olmo_user')->insert([
			'enabled_is_general' 	=> 'true',
			'email_email_general' 	=> 'admin@acanto.net',
			'password_pwd_general'  => '0cc175b9c0f1b6a831c399e269772661',
			'role_id_general'       => '1',
			'token_hidden_general'  => '1045e2086fda7ba429d6d2feb3562346'
		]);

        DB::table('olmo_user')->insert([
       		'enabled_is_general' 	=> 'true',
       		'email_email_general' 	=> 'publisher@acanto.net',
       		'password_pwd_general'  => '0cc175b9c0f1b6a831c399e269772661',
       		'role_id_general'       => '3',
       		'token_hidden_general'  => '1045e2086fda7ba429d6d2feb3562346'
       	]);


    }
}
