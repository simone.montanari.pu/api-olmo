<?php

namespace App\Containers\Backoffice\User\Models;

use App\Ship\Parents\Models\Model;

class User extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
 
    ];

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'User';

    public $table = 'olmo_user';

    public static function getAttr(){
        return [
            'table' => 'olmo_user',
            'requiredbackoffice' => [
                'email_email_general'
            ],
            'rules' => [
                [
                    'type' => 'unique',
                    'field' => 'email_email_general',
                    'table' => 'olmo_user',
                    'lang' => false,
                    'errortxt' => 'Email duplicate'
                ],
                [
                    'type' => 'equal',
                    'field_one' => 'password_pwd_general',
                    'field_two' => 'confirmpassword_pwd_general',
                    'errortxt' => 'Password not match'
                ]                
            ]
       ];
        
    } 
}
