<?php

namespace App\Containers\Backoffice\User\Actions;

use App\Containers\Backoffice\User\Models\User;
use App\Containers\Backoffice\User\Tasks\CreateUserTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateUserAction extends Action
{
    public function run(Request $request): User
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateUserTask::class)->run($data);
    }
}
