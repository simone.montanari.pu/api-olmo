<?php

namespace App\Containers\Backoffice\User\Actions;

use App\Containers\Backoffice\User\Models\User;
use App\Containers\Backoffice\User\Tasks\UpdateUserTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateUserAction extends Action
{
    public function run(Request $request): User
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateUserTask::class)->run($request->id, $data);
    }
}
