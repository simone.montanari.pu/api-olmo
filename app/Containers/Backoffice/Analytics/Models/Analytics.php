<?php

namespace App\Containers\Backoffice\Analytics\Models;

use App\Ship\Parents\Models\Model;

class Analytics extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $table = 'olmo_analytics';

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Analytics';

    public static function getAttr(){
        return ['table' => 'olmo_analytics'];
    }

}
