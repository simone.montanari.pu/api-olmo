<?php

namespace App\Containers\Backoffice\Analytics\Tasks;

use App\Containers\Backoffice\Analytics\Data\Repositories\AnalyticsRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindAnalyticsByIdTask extends Task
{
    protected AnalyticsRepository $repository;

    public function __construct(AnalyticsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
