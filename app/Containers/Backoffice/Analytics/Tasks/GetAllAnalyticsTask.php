<?php

namespace App\Containers\Backoffice\Analytics\Tasks;

use App\Containers\Backoffice\Analytics\Data\Repositories\AnalyticsRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllAnalyticsTask extends Task
{
    protected AnalyticsRepository $repository;

    public function __construct(AnalyticsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
