<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnalyticsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('olmo_analytics')) {
            Schema::table('olmo_analytics', function (Blueprint $table) {
                // Update existing table...
            });
        } else {
            Schema::create('olmo_analytics', function (Blueprint $table) {
                // Create new table...
                $table->charset = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';
                // General
                $table->increments('id')->unsigned();
                $table->text('gtmheader_txt_analytics')->nullable();
                $table->text('gtmbody_txt_analytics')->nullable();
                $table->text('gtmheaderprod_txt_analytics')->nullable();
                $table->text('gtmbodyprod_txt_analytics')->nullable();
                $table->text('robots_txt_analytics')->nullable();
                $table->text('robotsprod_txt_analytics')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_analytics');
    }
}
