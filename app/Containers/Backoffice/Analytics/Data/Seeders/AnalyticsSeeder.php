<?php

namespace App\Containers\Backoffice\Analytics\Data\Seeders;

use App\Ship\Parents\Seeders\Seeder;
use Illuminate\Support\Facades\DB;

class AnalyticsSeeder extends Seeder
{
    public function run()
    {
        DB::table('olmo_analytics')->truncate();
       
        DB::table('olmo_analytics')->insert([
            'gtmheader_txt_analytics'       => '',
            'gtmbody_txt_analytics'         => '',
            'gtmheaderprod_txt_analytics'   => '',
            'gtmbodyprod_txt_analytics'     => '',
            'robots_txt_analytics'          => '',
            'robotsprod_txt_analytics'      => ''
        ]);
    }
}
