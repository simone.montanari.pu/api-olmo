<?php

namespace App\Containers\Backoffice\Analytics\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class AnalyticsRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
