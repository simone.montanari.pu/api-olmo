<?php

namespace App\Containers\Backoffice\Analytics\UI\API\Controllers;

use App\Containers\Backoffice\Analytics\UI\API\Requests\CreateAnalyticsRequest;
use App\Containers\Backoffice\Analytics\UI\API\Requests\DeleteAnalyticsRequest;
use App\Containers\Backoffice\Analytics\UI\API\Requests\GetAllAnalyticsRequest;
use App\Containers\Backoffice\Analytics\UI\API\Requests\FindAnalyticsByIdRequest;
use App\Containers\Backoffice\Analytics\UI\API\Requests\UpdateAnalyticsRequest;
use App\Containers\Backoffice\Analytics\UI\API\Transformers\AnalyticsTransformer;
use App\Containers\Backoffice\Analytics\Actions\CreateAnalyticsAction;
use App\Containers\Backoffice\Analytics\Actions\FindAnalyticsByIdAction;
use App\Containers\Backoffice\Analytics\Actions\GetAllAnalyticsAction;
use App\Containers\Backoffice\Analytics\Actions\UpdateAnalyticsAction;
use App\Containers\Backoffice\Analytics\Actions\DeleteAnalyticsAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use App\Containers\Backoffice\Analytics\Models\Analytics;
class Controller extends ApiController
{
    public function createAnalytics(CreateAnalyticsRequest $request)
    {
        $id         =  1;
        $datas      = Analytics::where('id',$id)->get()->toArray();
        $attribute  = Analytics::getAttr();
        return  $this->getSingleList($datas,$attribute,$id);
    }


    public function createUpdateV1(CreateAnalyticsRequest $request)
    {
    	$attribute  = Analytics::getAttr();
    	return $this->createUpdate($request,$attribute);
    }

    
}
