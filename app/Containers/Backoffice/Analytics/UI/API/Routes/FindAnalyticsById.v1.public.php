<?php

/**
 * @apiGroup           Analytics
 * @apiName            findAnalyticsById
 *
 * @api                {GET} /v1/analytics/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Analytics\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('analytics/{id}', [Controller::class, 'findAnalyticsById'])
    ->name('api_analytics_find_analytics_by_id')
    ->middleware(['auth:api']);

