<?php

/**
 * @apiGroup           Analytics
 * @apiName            deleteAnalytics
 *
 * @api                {DELETE} /v1/analytics/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Analytics\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::delete('analytics/{id}', [Controller::class, 'deleteAnalytics'])
    ->name('api_analytics_delete_analytics')
    ->middleware(['auth:api']);

