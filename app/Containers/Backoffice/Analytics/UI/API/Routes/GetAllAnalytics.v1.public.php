<?php

/**
 * @apiGroup           Analytics
 * @apiName            getAllAnalytics
 *
 * @api                {GET} /v1/analytics Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Analytics\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('analytics', [Controller::class, 'getAllAnalytics'])
    ->name('api_analytics_get_all_analytics')
    ->middleware(['auth:api']);

