<?php

namespace App\Containers\Backoffice\Analytics\UI\API\Transformers;

use App\Containers\Backoffice\Analytics\Models\Analytics;
use App\Ship\Parents\Transformers\Transformer;

class AnalyticsTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Analytics $analytics): array
    {
        $response = [
            'object' => $analytics->getResourceKey(),
            'id' => $analytics->getHashedKey(),
            'created_at' => $analytics->created_at,
            'updated_at' => $analytics->updated_at,
            'readable_created_at' => $analytics->created_at->diffForHumans(),
            'readable_updated_at' => $analytics->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $analytics->id,
            // 'deleted_at' => $analytics->deleted_at,
        ], $response);

        return $response;
    }
}
