<?php

namespace App\Containers\Backoffice\Analytics\Actions;

use App\Containers\Backoffice\Analytics\Models\Analytics;
use App\Containers\Backoffice\Analytics\Tasks\CreateAnalyticsTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateAnalyticsAction extends Action
{
    public function run(Request $request): Analytics
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateAnalyticsTask::class)->run($data);
    }
}
