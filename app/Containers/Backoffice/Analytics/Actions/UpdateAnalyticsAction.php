<?php

namespace App\Containers\Backoffice\Analytics\Actions;

use App\Containers\Backoffice\Analytics\Models\Analytics;
use App\Containers\Backoffice\Analytics\Tasks\UpdateAnalyticsTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateAnalyticsAction extends Action
{
    public function run(Request $request): Analytics
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateAnalyticsTask::class)->run($request->id, $data);
    }
}
