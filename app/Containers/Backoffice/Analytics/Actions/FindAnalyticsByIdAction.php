<?php

namespace App\Containers\Backoffice\Analytics\Actions;

use App\Containers\Backoffice\Analytics\Models\Analytics;
use App\Containers\Backoffice\Analytics\Tasks\FindAnalyticsByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindAnalyticsByIdAction extends Action
{
    public function run(Request $request): Analytics
    {
        return app(FindAnalyticsByIdTask::class)->run($request->id);
    }
}
