<?php

namespace App\Containers\Backoffice\Analytics\Actions;

use App\Containers\Backoffice\Analytics\Tasks\GetAllAnalyticsTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllAnalyticsAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllAnalyticsTask::class)->addRequestCriteria()->run();
    }
}
