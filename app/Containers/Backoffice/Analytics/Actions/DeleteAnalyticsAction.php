<?php

namespace App\Containers\Backoffice\Analytics\Actions;

use App\Containers\Backoffice\Analytics\Tasks\DeleteAnalyticsTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteAnalyticsAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteAnalyticsTask::class)->run($request->id);
    }
}
