<?php

namespace App\Containers\Backoffice\Role\UI\API\Transformers;

use App\Containers\Backoffice\Role\Models\Role;
use App\Ship\Parents\Transformers\Transformer;

class RoleTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Role $role): array
    {
        $response = [
            'object' => $role->getResourceKey(),
            'id' => $role->getHashedKey(),
            'created_at' => $role->created_at,
            'updated_at' => $role->updated_at,
            'readable_created_at' => $role->created_at->diffForHumans(),
            'readable_updated_at' => $role->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $role->id,
            // 'deleted_at' => $role->deleted_at,
        ], $response);

        return $response;
    }
}
