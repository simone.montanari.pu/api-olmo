<?php

/**
 * @apiGroup           Role
 * @apiName            getAllRoles
 *
 * @api                {GET} /v1/role Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Role\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('role', [Controller::class, 'getAllRoles'])
    ->name('api_role_get_all_roles')
    ->middleware(['auth:api']);

