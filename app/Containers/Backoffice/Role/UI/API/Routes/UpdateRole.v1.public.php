<?php

/**
 * @apiGroup           Role
 * @apiName            updateRole
 *
 * @api                {PATCH} /v1/role/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Role\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::patch('role/{id}', [Controller::class, 'updateRole'])
    ->name('api_role_update_role')
    ->middleware(['auth:api']);

