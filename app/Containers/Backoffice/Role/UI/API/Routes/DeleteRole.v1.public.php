<?php

/**
 * @apiGroup           Role
 * @apiName            deleteRole
 *
 * @api                {DELETE} /v1/role/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Role\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::delete('role/{id}', [Controller::class, 'deleteRole'])
    ->name('api_role_delete_role')
    ->middleware(['auth:api']);

