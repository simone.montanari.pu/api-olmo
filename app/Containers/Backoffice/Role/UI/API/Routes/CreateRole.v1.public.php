<?php

/**
 * @apiGroup           Role
 * @apiName            createRole
 *
 * @api                {POST} /v1/role Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Role\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::post('role', [Controller::class, 'createRole'])
    ->name('api_role_create_role')
    ->middleware(['auth:api']);

