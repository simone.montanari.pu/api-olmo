<?php

namespace App\Containers\Backoffice\Role\UI\API\Controllers;

use App\Containers\Backoffice\Role\UI\API\Requests\CreateRoleRequest;
use App\Containers\Backoffice\Role\UI\API\Requests\DeleteRoleRequest;
use App\Containers\Backoffice\Role\UI\API\Requests\GetAllRolesRequest;
use App\Containers\Backoffice\Role\UI\API\Requests\FindRoleByIdRequest;
use App\Containers\Backoffice\Role\UI\API\Requests\UpdateRoleRequest;
use App\Containers\Backoffice\Role\UI\API\Transformers\RoleTransformer;
use App\Containers\Backoffice\Role\Actions\CreateRoleAction;
use App\Containers\Backoffice\Role\Actions\FindRoleByIdAction;
use App\Containers\Backoffice\Role\Actions\GetAllRolesAction;
use App\Containers\Backoffice\Role\Actions\UpdateRoleAction;
use App\Containers\Backoffice\Role\Actions\DeleteRoleAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;

class Controller extends ApiController
{
    public function createRole(CreateRoleRequest $request): JsonResponse
    {
        $role = app(CreateRoleAction::class)->run($request);
        return $this->created($this->transform($role, RoleTransformer::class));
    }

    public function findRoleById(FindRoleByIdRequest $request): array
    {
        $role = app(FindRoleByIdAction::class)->run($request);
        return $this->transform($role, RoleTransformer::class);
    }

    public function getAllRoles(GetAllRolesRequest $request): array
    {
        $roles = app(GetAllRolesAction::class)->run($request);
        return $this->transform($roles, RoleTransformer::class);
    }

    public function updateRole(UpdateRoleRequest $request): array
    {
        $role = app(UpdateRoleAction::class)->run($request);
        return $this->transform($role, RoleTransformer::class);
    }

    public function deleteRole(DeleteRoleRequest $request): JsonResponse
    {
        app(DeleteRoleAction::class)->run($request);
        return $this->noContent();
    }
}
