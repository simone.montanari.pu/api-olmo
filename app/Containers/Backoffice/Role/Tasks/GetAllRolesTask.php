<?php

namespace App\Containers\Backoffice\Role\Tasks;

use App\Containers\Backoffice\Role\Data\Repositories\RoleRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllRolesTask extends Task
{
    protected RoleRepository $repository;

    public function __construct(RoleRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
