<?php

namespace App\Containers\Backoffice\Role\Actions;

use App\Containers\Backoffice\Role\Tasks\DeleteRoleTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteRoleAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteRoleTask::class)->run($request->id);
    }
}
