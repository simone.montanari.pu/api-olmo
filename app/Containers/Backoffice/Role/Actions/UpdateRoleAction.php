<?php

namespace App\Containers\Backoffice\Role\Actions;

use App\Containers\Backoffice\Role\Models\Role;
use App\Containers\Backoffice\Role\Tasks\UpdateRoleTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateRoleAction extends Action
{
    public function run(Request $request): Role
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateRoleTask::class)->run($request->id, $data);
    }
}
