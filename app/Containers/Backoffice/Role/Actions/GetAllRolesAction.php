<?php

namespace App\Containers\Backoffice\Role\Actions;

use App\Containers\Backoffice\Role\Tasks\GetAllRolesTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllRolesAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllRolesTask::class)->addRequestCriteria()->run();
    }
}
