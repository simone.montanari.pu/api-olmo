<?php

namespace App\Containers\Backoffice\Role\Actions;

use App\Containers\Backoffice\Role\Models\Role;
use App\Containers\Backoffice\Role\Tasks\CreateRoleTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateRoleAction extends Action
{
    public function run(Request $request): Role
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateRoleTask::class)->run($data);
    }
}
