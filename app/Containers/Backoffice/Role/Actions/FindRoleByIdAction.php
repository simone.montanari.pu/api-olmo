<?php

namespace App\Containers\Backoffice\Role\Actions;

use App\Containers\Backoffice\Role\Models\Role;
use App\Containers\Backoffice\Role\Tasks\FindRoleByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindRoleByIdAction extends Action
{
    public function run(Request $request): Role
    {
        return app(FindRoleByIdTask::class)->run($request->id);
    }
}
