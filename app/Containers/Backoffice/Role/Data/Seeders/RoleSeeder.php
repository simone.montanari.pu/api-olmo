<?php

namespace App\Containers\Backoffice\Role\Data\Seeders;

use App\Ship\Parents\Seeders\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    public function run()
    {
        DB::table('olmo_role')->truncate();
       
        DB::table('olmo_role')->insert([
            'name_txt_general'     => 'superadmin',
        ]);

        DB::table('olmo_role')->insert([
            'name_txt_general'     => 'admin',
        ]);

        DB::table('olmo_role')->insert([
            'name_txt_general'     => 'editor',
        ]);

        DB::table('olmo_role')->insert([
            'name_txt_general'     => 'publisher',
        ]);

    }
}
