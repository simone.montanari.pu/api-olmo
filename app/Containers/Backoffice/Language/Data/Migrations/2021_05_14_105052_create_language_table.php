<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLanguageTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('olmo_language')) {
            Schema::table('olmo_language', function (Blueprint $table) {
                // Update existing table...
            });
        } else {
            Schema::create('olmo_language', function (Blueprint $table) {
                // Create new table...
                $table->charset = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';
                // General
                $table->increments('id')->unsigned();
                $table->string('enabled_is_general', 5)->nullable();
                $table->string('code_txt_general', 10)->nullable();
                $table->text('label_txt_general')->nullable();
                $table->string('name_txt_general', 255)->nullable();
                $table->string('displayname_txt_general', 255)->nullable();
                $table->string('default_is_general', 5)->nullable();
                $table->string('menu_is_general', 5)->nullable();
                $table->smallInteger('position_ord_general')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_language');
    }
}
