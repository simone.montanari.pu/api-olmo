<?php

namespace App\Containers\Backoffice\Language\Data\Seeders;

use App\Ship\Parents\Seeders\Seeder;
use Illuminate\Support\Facades\DB;

class LanguageSeeder extends Seeder
{
    public function run()
    {
        DB::table('olmo_language')->truncate();
       
        DB::table('olmo_language')->insert([
            'enabled_is_general'        => 'true',
            'code_txt_general'          => 'en',
            'name_txt_general'          => 'english',
            'displayname_txt_general'   => 'English',
            'default_is_general'        => 'true',
            'position_ord_general'      => '0'
        ]);
    }
}
