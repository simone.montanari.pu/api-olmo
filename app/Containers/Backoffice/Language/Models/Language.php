<?php

namespace App\Containers\Backoffice\Language\Models;

use App\Ship\Parents\Models\Model;

class Language extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $table = 'olmo_language';

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Language';

    public static function getAttr(){
        return ['table' => 'olmo_language'];
    }

}
