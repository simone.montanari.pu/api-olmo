<?php

namespace App\Containers\Backoffice\Language\UI\API\Controllers;

use App\Containers\Backoffice\Language\UI\API\Requests\CreateLanguageRequest;
use App\Containers\Backoffice\Language\UI\API\Requests\DeleteLanguageRequest;
use App\Containers\Backoffice\Language\UI\API\Requests\GetAllLanguagesRequest;
use App\Containers\Backoffice\Language\UI\API\Requests\FindLanguageByIdRequest;
use App\Containers\Backoffice\Language\UI\API\Requests\UpdateLanguageRequest;
use App\Containers\Backoffice\Language\UI\API\Transformers\LanguageTransformer;
use App\Containers\Backoffice\Language\Actions\CreateLanguageAction;
use App\Containers\Backoffice\Language\Actions\FindLanguageByIdAction;
use App\Containers\Backoffice\Language\Actions\GetAllLanguagesAction;
use App\Containers\Backoffice\Language\Actions\UpdateLanguageAction;
use App\Containers\Backoffice\Language\Actions\DeleteLanguageAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use App\Containers\Backoffice\Language\Models\Language;

class Controller extends ApiController
{
    public function getListV1(GetAllLanguagesRequest $request)
    {
    	$datas   = Language::select('code_txt_general','displayname_txt_general','default_is_general','enabled_is_general','id')->get()->toArray();

        $attribute  = Language::getAttr();
        return  $this->getList($datas,$attribute);

    }


    public function createUpdateV1(GetAllLanguagesRequest $request)
    {
    	$attribute  = Language::getAttr();
    	return $this->createUpdate($request,$attribute);
    }

    public function findById(GetAllLanguagesRequest $request)
    { 
        $attribute  = Language::getAttr();
        $id         = $request->id;
        if($id == 0){
            return $this->emptyItem($attribute);
        }else{
           $datas      = Language::where('id',$id)->get()->toArray();
           $attribute  = Language::getAttr();
           return  $this->getSingleList($datas,$attribute,$id);
        }
    }

}
