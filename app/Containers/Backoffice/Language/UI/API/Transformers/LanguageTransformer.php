<?php

namespace App\Containers\Backoffice\Language\UI\API\Transformers;

use App\Containers\Backoffice\Language\Models\Language;
use App\Ship\Parents\Transformers\Transformer;

class LanguageTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Language $language): array
    {
        $response = [
            'object' => $language->getResourceKey(),
            'id' => $language->getHashedKey(),
            'created_at' => $language->created_at,
            'updated_at' => $language->updated_at,
            'readable_created_at' => $language->created_at->diffForHumans(),
            'readable_updated_at' => $language->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $language->id,
            // 'deleted_at' => $language->deleted_at,
        ], $response);

        return $response;
    }
}
