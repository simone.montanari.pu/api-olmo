<?php

/**
 * @apiGroup           Language
 * @apiName            createLanguage
 *
 * @api                {POST} /v1/languages Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Language\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::put('{lang}/language/{id}', [Controller::class, 'createUpdateV1'])->middleware(['auth:api']);

