<?php

/**
 * @apiGroup           Language
 * @apiName            deleteLanguage
 *
 * @api                {DELETE} /v1/languages/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Language\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::delete('languages/{id}', [Controller::class, 'deleteLanguage'])
    ->name('api_language_delete_language')
    ->middleware(['auth:api']);

