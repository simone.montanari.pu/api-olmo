<?php

/**
 * @apiGroup           Language
 * @apiName            getAllLanguages
 *
 * @api                {GET} /v1/languages Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Language\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::post('{lang}/language', [Controller::class, 'getListV1'])->middleware(['auth:api']);