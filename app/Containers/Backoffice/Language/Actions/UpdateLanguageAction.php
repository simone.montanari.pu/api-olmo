<?php

namespace App\Containers\Backoffice\Language\Actions;

use App\Containers\Backoffice\Language\Models\Language;
use App\Containers\Backoffice\Language\Tasks\UpdateLanguageTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateLanguageAction extends Action
{
    public function run(Request $request): Language
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateLanguageTask::class)->run($request->id, $data);
    }
}
