<?php

namespace App\Containers\Backoffice\Language\Tasks;

use App\Containers\Backoffice\Language\Data\Repositories\LanguageRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllLanguagesTask extends Task
{
    protected LanguageRepository $repository;

    public function __construct(LanguageRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
