<?php

namespace App\Containers\Backoffice\Discount\Actions;

use App\Containers\Backoffice\Discount\Tasks\DeleteDiscountTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteDiscountAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteDiscountTask::class)->run($request->id);
    }
}
