<?php

namespace App\Containers\Backoffice\Discount\Actions;

use App\Containers\Backoffice\Discount\Models\Discount;
use App\Containers\Backoffice\Discount\Tasks\CreateDiscountTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateDiscountAction extends Action
{
    public function run(Request $request): Discount
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateDiscountTask::class)->run($data);
    }
}
