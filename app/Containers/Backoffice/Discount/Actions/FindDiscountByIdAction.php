<?php

namespace App\Containers\Backoffice\Discount\Actions;

use App\Containers\Backoffice\Discount\Models\Discount;
use App\Containers\Backoffice\Discount\Tasks\FindDiscountByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindDiscountByIdAction extends Action
{
    public function run(Request $request): Discount
    {
        return app(FindDiscountByIdTask::class)->run($request->id);
    }
}
