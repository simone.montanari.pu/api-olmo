<?php

namespace App\Containers\Backoffice\Discount\Actions;

use App\Containers\Backoffice\Discount\Tasks\GetAllDiscountsTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllDiscountsAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllDiscountsTask::class)->addRequestCriteria()->run();
    }
}
