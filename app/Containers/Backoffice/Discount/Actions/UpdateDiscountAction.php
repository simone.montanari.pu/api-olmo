<?php

namespace App\Containers\Backoffice\Discount\Actions;

use App\Containers\Backoffice\Discount\Models\Discount;
use App\Containers\Backoffice\Discount\Tasks\UpdateDiscountTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateDiscountAction extends Action
{
    public function run(Request $request): Discount
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateDiscountTask::class)->run($request->id, $data);
    }
}
