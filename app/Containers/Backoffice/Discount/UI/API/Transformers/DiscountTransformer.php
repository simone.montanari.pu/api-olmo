<?php

namespace App\Containers\Backoffice\Discount\UI\API\Transformers;

use App\Containers\Backoffice\Discount\Models\Discount;
use App\Ship\Parents\Transformers\Transformer;

class DiscountTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Discount $discount): array
    {
        $response = [
            'object' => $discount->getResourceKey(),
            'id' => $discount->getHashedKey(),
            'created_at' => $discount->created_at,
            'updated_at' => $discount->updated_at,
            'readable_created_at' => $discount->created_at->diffForHumans(),
            'readable_updated_at' => $discount->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $discount->id,
            // 'deleted_at' => $discount->deleted_at,
        ], $response);

        return $response;
    }
}
