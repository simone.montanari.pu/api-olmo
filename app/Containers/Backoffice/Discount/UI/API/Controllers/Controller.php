<?php

namespace App\Containers\Backoffice\Discount\UI\API\Controllers;

use App\Containers\Backoffice\Discount\UI\API\Requests\CreateDiscountRequest;
use App\Containers\Backoffice\Discount\UI\API\Requests\DeleteDiscountRequest;
use App\Containers\Backoffice\Discount\UI\API\Requests\GetAllDiscountsRequest;
use App\Containers\Backoffice\Discount\UI\API\Requests\FindDiscountByIdRequest;
use App\Containers\Backoffice\Discount\UI\API\Requests\UpdateDiscountRequest;
use App\Containers\Backoffice\Discount\UI\API\Transformers\DiscountTransformer;
use App\Containers\Backoffice\Discount\Actions\CreateDiscountAction;
use App\Containers\Backoffice\Discount\Actions\FindDiscountByIdAction;
use App\Containers\Backoffice\Discount\Actions\GetAllDiscountsAction;
use App\Containers\Backoffice\Discount\Actions\UpdateDiscountAction;
use App\Containers\Backoffice\Discount\Actions\DeleteDiscountAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use App\Containers\Backoffice\Discount\Models\Discount;

class Controller extends ApiController
{
    public function getListV1(GetAllDiscountsRequest $request)
    {
        $lang = $request->lang;
    	$datas   = Discount::select('code_txt_general','enabled_is_general','id')  
        ->get()
        ->toArray();

        $attribute  = Discount::getAttr();
        return  $this->getList($datas,$attribute);

    }


    public function createUpdateV1(GetAllDiscountsRequest $request)
    {
    	$attribute  = Discount::getAttr();
    	return $this->createUpdate($request,$attribute);
    }

    public function findById(GetAllDiscountsRequest $request)
    { 
        $lang       = $request->lang;
        $attribute  = Discount::getAttr();
        $id         = $request->id;
        if($id == 0){
            return $this->emptyItem($attribute,$lang);
        }else{
           $datas      = Discount::where('id',$id)->get()->toArray();
           $attribute  = Discount::getAttr();
           return  $this->getSingleList($datas,$attribute,$id);
        }
    }
}
