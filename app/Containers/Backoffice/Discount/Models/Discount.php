<?php

namespace App\Containers\Backoffice\Discount\Models;

use App\Ship\Parents\Models\Model;

class Discount extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $table = 'olmo_discount';

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Discount';

    public static function getAttr(){
        return ['table' => 'olmo_discount'];
    }

}
