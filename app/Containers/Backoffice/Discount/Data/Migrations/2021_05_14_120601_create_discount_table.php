<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDiscountTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('olmo_discount')) {
            Schema::table('olmo_discount', function (Blueprint $table) {
                // Update existing table...
            });
        } else {
            Schema::create('olmo_discount', function (Blueprint $table) {
                // Create new table...
                $table->charset = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';
                // General
                $table->increments('id')->unsigned();
                $table->string('enabled_is_general', 5)->nullable();
                $table->text('code_txt_general')->nullable();
                $table->smallInteger('percentage_num_general')->unsigned()->nullable();
                $table->smallInteger('value_num_general')->unsigned()->nullable();
                $table->dateTime('startdate_date_general')->nullable();
                $table->dateTime('enddate_date_general')->nullable();
                $table->integer('category_id_condition')->nullable();
                $table->string('customer_id_condition', 100)->nullable();
                $table->string('customerprop_select_condition', 100)->nullable();                
                $table->string('customercondition_select_condition', 100)->nullable();    
                $table->string('customervalue_txt_condition', 100)->nullable();
                // $table->smallInteger('use')->unsigned()->nullable();
                $table->smallInteger('generalnumberofuses_num_condition')->unsigned()->nullable();
                $table->smallInteger('usernumberofuses_num_condition')->unsigned()->nullable();
                $table->smallInteger('ordercondition_select_condition')->unsigned()->nullable();
                $table->smallInteger('ordernumber_num_condition')->unsigned()->nullable();                
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_discount');
    }
}
