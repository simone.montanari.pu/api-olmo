<?php

namespace App\Containers\Backoffice\Discount\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class DiscountRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
