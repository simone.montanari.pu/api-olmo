<?php

namespace App\Containers\Backoffice\Discount\Tasks;

use App\Containers\Backoffice\Discount\Data\Repositories\DiscountRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateDiscountTask extends Task
{
    protected DiscountRepository $repository;

    public function __construct(DiscountRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
