<?php

namespace App\Containers\Backoffice\Discount\Tasks;

use App\Containers\Backoffice\Discount\Data\Repositories\DiscountRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateDiscountTask extends Task
{
    protected DiscountRepository $repository;

    public function __construct(DiscountRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
