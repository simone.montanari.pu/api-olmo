<?php

namespace App\Containers\Backoffice\Discount\Tasks;

use App\Containers\Backoffice\Discount\Data\Repositories\DiscountRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllDiscountsTask extends Task
{
    protected DiscountRepository $repository;

    public function __construct(DiscountRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
