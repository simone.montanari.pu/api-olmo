<?php

namespace App\Containers\Backoffice\Discount\Tasks;

use App\Containers\Backoffice\Discount\Data\Repositories\DiscountRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindDiscountByIdTask extends Task
{
    protected DiscountRepository $repository;

    public function __construct(DiscountRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
