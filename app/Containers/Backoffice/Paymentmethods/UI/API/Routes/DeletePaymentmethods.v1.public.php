<?php

/**
 * @apiGroup           Paymentmethods
 * @apiName            deletePaymentmethods
 *
 * @api                {DELETE} /v1/paymentmethods/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Paymentmethods\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::delete('paymentmethods/{id}', [Controller::class, 'deletePaymentmethods'])
    ->name('api_paymentmethods_delete_paymentmethods')
    ->middleware(['auth:api']);

