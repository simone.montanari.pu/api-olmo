<?php

/**
 * @apiGroup           Paymentmethods
 * @apiName            findPaymentmethodsById
 *
 * @api                {GET} /v1/paymentmethods/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Paymentmethods\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('paymentmethods/{id}', [Controller::class, 'findPaymentmethodsById'])
    ->name('api_paymentmethods_find_paymentmethods_by_id')
    ->middleware(['auth:api']);

