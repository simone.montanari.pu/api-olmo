<?php

/**
 * @apiGroup           Paymentmethods
 * @apiName            createPaymentmethods
 *
 * @api                {POST} /v1/paymentmethods Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Paymentmethods\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::post('paymentmethods', [Controller::class, 'createPaymentmethods'])
    ->name('api_paymentmethods_create_paymentmethods')
    ->middleware(['auth:api']);

