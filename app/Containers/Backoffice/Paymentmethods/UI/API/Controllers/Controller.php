<?php

namespace App\Containers\Backoffice\Paymentmethods\UI\API\Controllers;

use App\Containers\Backoffice\Paymentmethods\UI\API\Requests\CreatePaymentmethodsRequest;
use App\Containers\Backoffice\Paymentmethods\UI\API\Requests\DeletePaymentmethodsRequest;
use App\Containers\Backoffice\Paymentmethods\UI\API\Requests\GetAllPaymentmethodsRequest;
use App\Containers\Backoffice\Paymentmethods\UI\API\Requests\FindPaymentmethodsByIdRequest;
use App\Containers\Backoffice\Paymentmethods\UI\API\Requests\UpdatePaymentmethodsRequest;
use App\Containers\Backoffice\Paymentmethods\UI\API\Transformers\PaymentmethodsTransformer;
use App\Containers\Backoffice\Paymentmethods\Actions\CreatePaymentmethodsAction;
use App\Containers\Backoffice\Paymentmethods\Actions\FindPaymentmethodsByIdAction;
use App\Containers\Backoffice\Paymentmethods\Actions\GetAllPaymentmethodsAction;
use App\Containers\Backoffice\Paymentmethods\Actions\UpdatePaymentmethodsAction;
use App\Containers\Backoffice\Paymentmethods\Actions\DeletePaymentmethodsAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;

class Controller extends ApiController
{
    public function createPaymentmethods(CreatePaymentmethodsRequest $request): JsonResponse
    {
        $paymentmethods = app(CreatePaymentmethodsAction::class)->run($request);
        return $this->created($this->transform($paymentmethods, PaymentmethodsTransformer::class));
    }

    public function findPaymentmethodsById(FindPaymentmethodsByIdRequest $request): array
    {
        $paymentmethods = app(FindPaymentmethodsByIdAction::class)->run($request);
        return $this->transform($paymentmethods, PaymentmethodsTransformer::class);
    }

    public function getAllPaymentmethods(GetAllPaymentmethodsRequest $request): array
    {
        $paymentmethods = app(GetAllPaymentmethodsAction::class)->run($request);
        return $this->transform($paymentmethods, PaymentmethodsTransformer::class);
    }

    public function updatePaymentmethods(UpdatePaymentmethodsRequest $request): array
    {
        $paymentmethods = app(UpdatePaymentmethodsAction::class)->run($request);
        return $this->transform($paymentmethods, PaymentmethodsTransformer::class);
    }

    public function deletePaymentmethods(DeletePaymentmethodsRequest $request): JsonResponse
    {
        app(DeletePaymentmethodsAction::class)->run($request);
        return $this->noContent();
    }
}
