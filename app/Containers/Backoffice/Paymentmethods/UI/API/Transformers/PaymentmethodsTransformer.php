<?php

namespace App\Containers\Backoffice\Paymentmethods\UI\API\Transformers;

use App\Containers\Backoffice\Paymentmethods\Models\Paymentmethods;
use App\Ship\Parents\Transformers\Transformer;

class PaymentmethodsTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Paymentmethods $paymentmethods): array
    {
        $response = [
            'object' => $paymentmethods->getResourceKey(),
            'id' => $paymentmethods->getHashedKey(),
            'created_at' => $paymentmethods->created_at,
            'updated_at' => $paymentmethods->updated_at,
            'readable_created_at' => $paymentmethods->created_at->diffForHumans(),
            'readable_updated_at' => $paymentmethods->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $paymentmethods->id,
            // 'deleted_at' => $paymentmethods->deleted_at,
        ], $response);

        return $response;
    }
}
