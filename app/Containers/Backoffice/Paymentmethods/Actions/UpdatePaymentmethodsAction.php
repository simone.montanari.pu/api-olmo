<?php

namespace App\Containers\Backoffice\Paymentmethods\Actions;

use App\Containers\Backoffice\Paymentmethods\Models\Paymentmethods;
use App\Containers\Backoffice\Paymentmethods\Tasks\UpdatePaymentmethodsTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdatePaymentmethodsAction extends Action
{
    public function run(Request $request): Paymentmethods
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdatePaymentmethodsTask::class)->run($request->id, $data);
    }
}
