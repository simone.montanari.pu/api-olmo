<?php

namespace App\Containers\Backoffice\Paymentmethods\Actions;

use App\Containers\Backoffice\Paymentmethods\Models\Paymentmethods;
use App\Containers\Backoffice\Paymentmethods\Tasks\CreatePaymentmethodsTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreatePaymentmethodsAction extends Action
{
    public function run(Request $request): Paymentmethods
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreatePaymentmethodsTask::class)->run($data);
    }
}
