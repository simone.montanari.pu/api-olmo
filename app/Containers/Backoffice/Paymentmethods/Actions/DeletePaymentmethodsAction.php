<?php

namespace App\Containers\Backoffice\Paymentmethods\Actions;

use App\Containers\Backoffice\Paymentmethods\Tasks\DeletePaymentmethodsTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeletePaymentmethodsAction extends Action
{
    public function run(Request $request)
    {
        return app(DeletePaymentmethodsTask::class)->run($request->id);
    }
}
