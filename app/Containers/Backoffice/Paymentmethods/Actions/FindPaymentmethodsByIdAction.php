<?php

namespace App\Containers\Backoffice\Paymentmethods\Actions;

use App\Containers\Backoffice\Paymentmethods\Models\Paymentmethods;
use App\Containers\Backoffice\Paymentmethods\Tasks\FindPaymentmethodsByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindPaymentmethodsByIdAction extends Action
{
    public function run(Request $request): Paymentmethods
    {
        return app(FindPaymentmethodsByIdTask::class)->run($request->id);
    }
}
