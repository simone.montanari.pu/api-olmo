<?php

namespace App\Containers\Backoffice\Paymentmethods\Actions;

use App\Containers\Backoffice\Paymentmethods\Tasks\GetAllPaymentmethodsTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllPaymentmethodsAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllPaymentmethodsTask::class)->addRequestCriteria()->run();
    }
}
