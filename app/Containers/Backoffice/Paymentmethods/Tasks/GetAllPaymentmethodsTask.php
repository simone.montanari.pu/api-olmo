<?php

namespace App\Containers\Backoffice\Paymentmethods\Tasks;

use App\Containers\Backoffice\Paymentmethods\Data\Repositories\PaymentmethodsRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllPaymentmethodsTask extends Task
{
    protected PaymentmethodsRepository $repository;

    public function __construct(PaymentmethodsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
