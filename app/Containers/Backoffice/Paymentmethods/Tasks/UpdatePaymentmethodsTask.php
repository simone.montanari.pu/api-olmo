<?php

namespace App\Containers\Backoffice\Paymentmethods\Tasks;

use App\Containers\Backoffice\Paymentmethods\Data\Repositories\PaymentmethodsRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdatePaymentmethodsTask extends Task
{
    protected PaymentmethodsRepository $repository;

    public function __construct(PaymentmethodsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
