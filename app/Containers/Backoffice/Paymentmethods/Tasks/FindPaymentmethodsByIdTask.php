<?php

namespace App\Containers\Backoffice\Paymentmethods\Tasks;

use App\Containers\Backoffice\Paymentmethods\Data\Repositories\PaymentmethodsRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindPaymentmethodsByIdTask extends Task
{
    protected PaymentmethodsRepository $repository;

    public function __construct(PaymentmethodsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
