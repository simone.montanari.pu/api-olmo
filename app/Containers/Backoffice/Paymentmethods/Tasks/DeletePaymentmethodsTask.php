<?php

namespace App\Containers\Backoffice\Paymentmethods\Tasks;

use App\Containers\Backoffice\Paymentmethods\Data\Repositories\PaymentmethodsRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeletePaymentmethodsTask extends Task
{
    protected PaymentmethodsRepository $repository;

    public function __construct(PaymentmethodsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id): ?int
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
