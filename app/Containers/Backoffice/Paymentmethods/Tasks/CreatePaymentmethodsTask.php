<?php

namespace App\Containers\Backoffice\Paymentmethods\Tasks;

use App\Containers\Backoffice\Paymentmethods\Data\Repositories\PaymentmethodsRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreatePaymentmethodsTask extends Task
{
    protected PaymentmethodsRepository $repository;

    public function __construct(PaymentmethodsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
