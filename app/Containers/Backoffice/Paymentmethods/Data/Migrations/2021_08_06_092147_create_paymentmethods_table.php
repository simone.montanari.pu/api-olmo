<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentmethodsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('olmo_paymentmethod')) {
            Schema::table('olmo_paymentmethod', function (Blueprint $table) {
                // Update existing table...
            });
        } else {
            Schema::create('olmo_paymentmethod', function (Blueprint $table) {
                // Create new table...
                $table->charset = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';
    
                $table->increments('id')->unsigned();
                $table->text('code_txt_general')->nullable();
                $table->text('name_txt_general')->nullable();
                $table->text('description_txt_general')->nullable();


                $table->text('requiredShippingAddress')->nullable();
                $table->text('requiredBillingAddress')->nullable();
            
                //locale
                $table->text('lang_langs_general')->nullable();
                $table->text('locale_hidden_general')->nullable();
                $table->text('parentid_hidden_general')->nullable();  
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_paymentmethod');
    }
}
