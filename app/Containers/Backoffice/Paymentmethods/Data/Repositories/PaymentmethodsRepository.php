<?php

namespace App\Containers\Backoffice\Paymentmethods\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class PaymentmethodsRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
