<?php

namespace App\Containers\Backoffice\Paymentmethods\Data\Seeders;

use App\Ship\Parents\Seeders\Seeder;
use Illuminate\Support\Facades\DB;


class PaymentmethodsSeeder extends Seeder
{
    public function run()
    {
       
		DB::table('olmo_paymentmethod')->insert([
			'code_txt_general' 				=> 'banktransfer',
			'name_txt_general' 				=> 'Bonifico Bancario',
			'description_txt_general'       => "L'ordine verrà processato a pagamento effettuato",
			'locale_hidden_general'         => "en",
			'parentid_hidden_general'		=> '0'
		]);

		DB::table('olmo_paymentmethod')->insert([
			'code_txt_general' 				=> 'creditcart',
			'name_txt_general' 				=> 'Carta di credito',
			'description_txt_general'       => "Pagamento sicuto con carte di credito",
			'locale_hidden_general'         => "en",
			'parentid_hidden_general'		=> '0'
		]);


    }
}