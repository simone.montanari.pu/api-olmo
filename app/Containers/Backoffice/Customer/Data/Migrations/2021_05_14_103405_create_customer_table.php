<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Ship\Parents\Controllers\ApiMigration;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('olmo_customer')) {
            Schema::table('olmo_customer', function (Blueprint $table) {
                // Update existing table...
            });
        } else {
            Schema::create('olmo_customer', function (Blueprint $table) {
                // Create new table...
                $table->charset = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';
                // General
                $table->increments('id')->unsigned();
                $table->string('enabled_is_general', 5)->nullable();
                $table->string('customerstatus_select_general', 100)->nullable();
                $table->string('token_read_general', 100)->nullable();
                $table->string('tokenactive_read_general', 100)->nullable();
                $table->string('endsession_read_general', 100)->nullable();
                $table->string('currentlocale_read_general', 100)->nullable();
                $table->string('currenttimezone_read_general', 100)->nullable();
                $table->text('email_email_general')->nullable();
                $table->text('password_pwd_general')->nullable();
                $table->text('confirmpassword_pwd_general')->nullable();
                $table->text('customerlang_txt_general')->nullable();
                $table->text('privacy_is_general')->nullable();
                $table->text('terms_is_general')->nullable();
                $table->dateTime('create_read_general')->useCurrent()->nullable();
                $table->dateTime('lastmod_read_general')->useCurrentOnUpdate()->nullable();
                ApiMigration::ECustomer($table);                
                // ************
                //  Be careful: you are modifing the register form so watch out and share with your team what you change
                //  as you can see the _register creates another tab and in the same time generates the register form
                //  which you can access throught this api {domain.com}/api/forms/:locale/address/register
                // ************
                $table->text('name_txt_register');
                $table->text('surname_txt_register');
                $table->text('profession_txt_register');
                $table->text('company_txt_register');
                $table->text('country_txt_register');
                $table->string('vat_txt_register', 100)->nullable();
                // ************
                //  Be careful: down here you are going to edit the profile fields which not appear in the register form
                // ************
                // $table->text('sector_txt_profile')->nullable();
                // $table->text('distributor_txt_profile')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_customer');
    }
}
