<?php

namespace App\Containers\Backoffice\Customer\Tasks;

use App\Containers\Backoffice\Customer\Data\Repositories\CustomerRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllCustomersTask extends Task
{
    protected CustomerRepository $repository;

    public function __construct(CustomerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
