<?php

namespace App\Containers\Backoffice\Customer\Actions;

use App\Containers\Backoffice\Customer\Tasks\GetAllCustomersTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllCustomersAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllCustomersTask::class)->addRequestCriteria()->run();
    }
}
