<?php

namespace App\Containers\Backoffice\Customer\Actions;

use App\Containers\Backoffice\Customer\Tasks\DeleteCustomerTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteCustomerAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteCustomerTask::class)->run($request->id);
    }
}
