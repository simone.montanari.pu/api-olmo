<?php

namespace App\Containers\Backoffice\Customer\Actions;

use App\Containers\Backoffice\Customer\Models\Customer;
use App\Containers\Backoffice\Customer\Tasks\CreateCustomerTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateCustomerAction extends Action
{
    public function run(Request $request): Customer
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateCustomerTask::class)->run($data);
    }
}
