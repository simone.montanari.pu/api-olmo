<?php

namespace App\Containers\Backoffice\Customer\Models;

use App\Ship\Parents\Models\Model;

class Customer extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'create_read_general',
        'lastmode_read_general',
    ];

    public $table = 'olmo_customer';

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Customer';

    public static function getAttr(){
        return [
            'table' => 'olmo_customer',
            'required' => [
                'customerstatus_select_general',
                'email_email_general',
                'password_pwd_general',
                'confirmpassword_pwd_general',
                'name_txt_register',
                'surname_txt_register',
                'profession_txt_register',
                'company_txt_register',
                'country_txt_register',
                'vat_txt_register',
                'privacy_is_register',
                'terms_is_register'
            ],
            'requiredbackoffice' => [
                'email_email_general'
            ],
            'forms' => [
                'register' => [
                     'groups' => ['register','email_email_general','password_pwd_general','confirmpassword_pwd_general'],
                     'exclude' => [],
                     'submit'  => true
                ],
                'profile' => [
                    'groups' => ['register','profile'],
                    'exclude' => ['privacy_is_register','terms_is_register'],
                    'submit'  => true
                ]
            ],
            'rules' => [
                [
                    'type' => 'unique',
                    'field' => 'email_email_general',
                    'table' => 'olmo_customer',
                    'lang' => false,
                    'errortxt' => 'Email duplicate'
                ],
                [
                    'type' => 'equal',
                    'field_one' => 'password_pwd_general',
                    'field_two' => 'confirmpassword_pwd_general',
                    'errortxt' => 'Password not match'
                ]                
            ],
            'cart' =>[
                'discountglobalfield' => 'vat_txt_register'
            ]
       ];
        
    }

}
