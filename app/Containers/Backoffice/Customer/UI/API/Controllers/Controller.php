<?php

namespace App\Containers\Backoffice\Customer\UI\API\Controllers;

use App\Containers\Backoffice\Customer\UI\API\Requests\CreateCustomerRequest;
use App\Containers\Backoffice\Customer\UI\API\Requests\DeleteCustomerRequest;
use App\Containers\Backoffice\Customer\UI\API\Requests\GetAllCustomersRequest;
use App\Containers\Backoffice\Customer\UI\API\Requests\FindCustomerByIdRequest;
use App\Containers\Backoffice\Customer\UI\API\Requests\UpdateCustomerRequest;
use App\Containers\Backoffice\Customer\UI\API\Transformers\CustomerTransformer;
use App\Containers\Backoffice\Customer\Actions\CreateCustomerAction;
use App\Containers\Backoffice\Customer\Actions\FindCustomerByIdAction;
use App\Containers\Backoffice\Customer\Actions\GetAllCustomersAction;
use App\Containers\Backoffice\Customer\Actions\UpdateCustomerAction;
use App\Containers\Backoffice\Customer\Actions\DeleteCustomerAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use App\Containers\Backoffice\Customer\Models\Customer;
use App\Ship\Parents\Controllers\ApiHelper;
use Illuminate\Support\Facades\DB;

class Controller extends ApiController
{


    public function getListV1(CreateCustomerRequest $request)
    {
        $lang   = $request->lang;
    	$datas   = Customer::select('email_email_general','name_txt_register','surname_txt_register','customerstatus_select_general as customerstatus_label_general','id')  
        ->get()
        ->toArray();

        $attribute  = Customer::getAttr();
        return  $this->getList($datas,$attribute);

    }


    public function createUpdateV1(CreateCustomerRequest $request)
    {
    	$attribute  = Customer::getAttr();
        $rules       = $attribute['rules'];
        $required    = $attribute['required'];
        
        if(isset($attribute['requiredbackoffice'])){
            $required    = $attribute['requiredbackoffice'];
        }
       
        if($required){
            $res = ApiHelper::CheckRequired($request,$required);
            if($res['valid'] == false){
                return response($res['err'], 400);
                exit();
            }
        }

         
        if($rules){
            $res = ApiHelper::CheckRules($request,$rules);
            if($res['valid'] == false){
                return response($res['err'], 400);
                exit();
            }
        }
       
 
    	return $this->createUpdate($request,$attribute);
    }

    public function findById(CreateCustomerRequest $request)
    { 
        $lang       = $request->lang;
        $attribute  = Customer::getAttr();
        $id         = $request->id;
        if($id == 0){
            return $this->emptyItem($attribute,$lang);
        }else{
           $datas      = Customer::where('id',$id)->get()->toArray();
           $attribute  = Customer::getAttr();
           return  $this->getSingleList($datas,$attribute,$id);
        }
    }

     public function addressList(CreateCustomerRequest $request)
    {
        $model       = $request->model;
        $id_model    =  $request->id;
        $table       = 'olmo_customer';
        $field       = 'address_list_general';
        $e = Db::table($table)->where('id',$id_model)->select('address_list_general','id')->get()->toArray();

  

        if($e[0]->{$field} == ''){
            return ['items' => []];
        }else{
        $properties = explode(',',$e[0]->{$field});

        $datas   = Db::table('olmo_address')->whereIn('id',$properties)
        ->select('name_txt_general','typeaddress_select_general as type_txt_general','default_is_general','enabled_is_general','id')
        ->get()->toArray();


        $attribute  = Customer::getAttr();
        $attribute['table'] = 'olmo_address';
        return  $this->getList($datas,$attribute);
        }
       
    }


    public function findByAddress(CreateCustomerRequest $request)
    { 

        $lang       = $request->lang;
        $attribute  = Customer::getAttr();
        $attribute['table'] = 'olmo_address';
        $id         = $request->address_id;
        $datas  = Db::table('olmo_address')->where('id',$id)->get()->toArray();
        if($id == 0){
            return $this->emptyItem($attribute,$lang);
        }else{
        return  $this->getSingleList($datas,$attribute,$id,$lang);
        }
    }
    public function createUpdateAddressV1(CreateCustomerRequest $request)
    {
        $attribute  = Customer::getAttr();
        $attribute['table']               =  'olmo_address';
        $attribute['lang']                =  false;
        $name_property                    =  'address_list_general';
        $model                            =  $request->model;
        $table                            = 'olmo_'.$model;
        $id_model                         =  $request->id_model;
        $id_address                       =  $request->id_address;
        $request->id                      =  $id_address;
        $request->customer_hidden_general =  $request->id_model;
    	$res                              =  $this->createUpdate($request,$attribute);
        $id_address                       =  @$res[0]['value'];
        $current_val                      =  @Db::table($table)->where('id',$id_model)->first()->{$name_property};

  
        $check = false;
        if($current_val != null){
            $values = explode(',',$current_val);
            if(in_array($id_address,$values)){
                $check = true;
            }
        }

        //return [$table,$current_val,$check,$id_model,$id_slider];
        if($check == false){
            if($current_val == null ){
                Db::table($table)->where('id',$id_model)->update([$name_property => $id_address ]);
            }
            else{
               Db::table($table)->where('id',$id_model)->update([$name_property => $current_val.','.$id_address]);
            }
        }
    }



}
