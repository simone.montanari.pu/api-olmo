<?php

namespace App\Containers\Backoffice\Customer\UI\API\Transformers;

use App\Containers\Backoffice\Customer\Models\Customer;
use App\Ship\Parents\Transformers\Transformer;

class CustomerTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Customer $customer): array
    {
        $response = [
            'object' => $customer->getResourceKey(),
            'id' => $customer->getHashedKey(),
            'created_at' => $customer->created_at,
            'updated_at' => $customer->updated_at,
            'readable_created_at' => $customer->created_at->diffForHumans(),
            'readable_updated_at' => $customer->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $customer->id,
            // 'deleted_at' => $customer->deleted_at,
        ], $response);

        return $response;
    }
}
