<?php

/**
 * @apiGroup           Customer
 * @apiName            createCustomer
 *
 * @api                {POST} /v1/customers Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Customer\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::put('en/{model}/{id_model}/address/{id_address}', [Controller::class, 'createUpdateAddressV1'])->middleware(['auth:api']);

Route::put('{lang}/customer/{id}', [Controller::class, 'createUpdateV1'])->middleware(['auth:api']);

Route::get('{lang}/customer/{id}', [Controller::class, 'findById']);

Route::get('{lang}/customer/{id}/address/{address_id}', [Controller::class, 'findByAddress']);

Route::post('{lang}/customer/{id}/address', [Controller::class, 'addressList']);

Route::post('{lang}/customer', [Controller::class, 'getListV1'])->middleware(['auth:api']);




