<?php

namespace App\Containers\Backoffice\Category\Models;

use App\Ship\Parents\Models\Model;

class Category extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $table = 'olmo_category';

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Category';

    public static function getAttr(){
        return [
            'table' => 'olmo_category',
            'lang'  => true,
            'required' =>[
                'name_txt_general',
                'slug_txt_general',
                'template_id_general'
            ],
            'rules' => [
                [
                    'type' => 'unique',
                    'field' => 'slug_txt_general',
                    'table' => 'olmo_category',
                    'errortxt' => 'Slug duplicate'
                ],
                [
                    'type' => 'unique',
                    'field' => 'name_txt_general',
                    'table' => 'olmo_category',
                    'errortxt' => 'Name duplicate'
                ]                
            ] 
        ];
    }

}
