<?php

/**
 * @apiGroup           Category
 * @apiName            getAllCategories
 *
 * @api                {GET} /v1/categories Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Category\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::post('{lang}/category', [Controller::class, 'getListV1'])->middleware(['auth:api']);