<?php

namespace App\Containers\Backoffice\Category\UI\API\Controllers;

use App\Containers\Backoffice\Category\UI\API\Requests\CreateCategoryRequest;
use App\Containers\Backoffice\Category\UI\API\Requests\DeleteCategoryRequest;
use App\Containers\Backoffice\Category\UI\API\Requests\GetAllCategoriesRequest;
use App\Containers\Backoffice\Category\UI\API\Requests\FindCategoryByIdRequest;
use App\Containers\Backoffice\Category\UI\API\Requests\UpdateCategoryRequest;
use App\Containers\Backoffice\Category\UI\API\Transformers\CategoryTransformer;
use App\Containers\Backoffice\Category\Actions\CreateCategoryAction;
use App\Containers\Backoffice\Category\Actions\FindCategoryByIdAction;
use App\Containers\Backoffice\Category\Actions\GetAllCategoriesAction;
use App\Containers\Backoffice\Category\Actions\UpdateCategoryAction;
use App\Containers\Backoffice\Category\Actions\DeleteCategoryAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use App\Containers\Backoffice\Category\Models\Category;
use App\Ship\Parents\Controllers\ApiHelper;
use Illuminate\Support\Facades\DB;

class Controller extends ApiController
{
    public function getListV1(GetAllCategoriesRequest $request)
    {
        $lang = $request->lang;
    	$datas   = Category::select('name_txt_general','slug_txt_general','category_id_general','enabled_is_general','id')  
        ->where('locale_hidden_general',$lang)
        ->get()
        ->toArray();

        $attribute  = Category::getAttr();
        return  $this->getList($datas,$attribute);

    }


    public function createUpdateV1(GetAllCategoriesRequest $request)
    {
        $attribute  = Category::getAttr();
        $rules       = $attribute['rules'];
        $required    = $attribute['required'];
        if($required){
            $res = ApiHelper::CheckRequired($request,$required);
            if($res['valid'] == false){
                return response($res['err'], 400);
                exit();
            }
        }
        if($rules){
            $res = ApiHelper::CheckRules($request,$rules);
            if($res['valid'] == false){
                return response($res['err'], 400);
                exit();
            }
        }
    	return $this->createUpdate($request,$attribute);
    }

    public function findById(GetAllCategoriesRequest $request)
    { 
        $attribute  = Category::getAttr();
        $lang       = $request->lang;
        $id         = $request->id;
        if($id == 0){
            return $this->emptyItem($attribute,$lang);
        }else{
           $datas      = Category::where('id',$id)->get()->toArray();
           $attribute  = Category::getAttr();
           return  $this->getSingleList($datas,$attribute,$id,$lang);
        }
    }

}
