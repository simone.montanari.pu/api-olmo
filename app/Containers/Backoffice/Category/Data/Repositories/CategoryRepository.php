<?php

namespace App\Containers\Backoffice\Category\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class CategoryRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
