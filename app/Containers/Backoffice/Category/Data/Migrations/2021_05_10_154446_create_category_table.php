<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Ship\Parents\Controllers\ApiMigration;

class CreateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('olmo_category')) {
            Schema::table('olmo_category', function (Blueprint $table) {
                // Update existing table...
            });
        } else {
            Schema::create('olmo_category', function (Blueprint $table) {
                // Create new table...
                $table->charset = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';
                // General
                ApiMigration::Page($table);
                $table->integer('category_id_general')->nullable();
                // SEO
                ApiMigration::Seo($table);
                // Sitemap
                ApiMigration::Sitemap($table);                
                // Custom
                $table->text('title_txt_content')->nullable();
                $table->text('subtitle_txt_content')->nullable();
                $table->text('abstract_txt_content')->nullable();
                $table->text('content_editor_content')->nullable();
                $table->text('cover_img_content')->nullable();
                $table->text('cta_txt_content')->nullable();
                // DND
                $table->text('product_dnd_order')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_category');
    }
}
