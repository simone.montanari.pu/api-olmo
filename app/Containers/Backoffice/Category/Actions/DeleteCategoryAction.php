<?php

namespace App\Containers\Backoffice\Category\Actions;

use App\Containers\Backoffice\Category\Tasks\DeleteCategoryTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteCategoryAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteCategoryTask::class)->run($request->id);
    }
}
