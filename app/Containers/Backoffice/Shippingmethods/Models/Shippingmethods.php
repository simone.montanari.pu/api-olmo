<?php

namespace App\Containers\Backoffice\Shippingmethods\Models;

use App\Ship\Parents\Models\Model;

class Shippingmethods extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Shippingmethods';
}
