<?php

namespace App\Containers\Backoffice\Shippingmethods\Actions;

use App\Containers\Backoffice\Shippingmethods\Tasks\DeleteShippingmethodsTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteShippingmethodsAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteShippingmethodsTask::class)->run($request->id);
    }
}
