<?php

namespace App\Containers\Backoffice\Shippingmethods\Actions;

use App\Containers\Backoffice\Shippingmethods\Models\Shippingmethods;
use App\Containers\Backoffice\Shippingmethods\Tasks\CreateShippingmethodsTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateShippingmethodsAction extends Action
{
    public function run(Request $request): Shippingmethods
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateShippingmethodsTask::class)->run($data);
    }
}
