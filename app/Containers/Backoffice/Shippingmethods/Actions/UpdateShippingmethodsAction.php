<?php

namespace App\Containers\Backoffice\Shippingmethods\Actions;

use App\Containers\Backoffice\Shippingmethods\Models\Shippingmethods;
use App\Containers\Backoffice\Shippingmethods\Tasks\UpdateShippingmethodsTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateShippingmethodsAction extends Action
{
    public function run(Request $request): Shippingmethods
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateShippingmethodsTask::class)->run($request->id, $data);
    }
}
