<?php

namespace App\Containers\Backoffice\Shippingmethods\Actions;

use App\Containers\Backoffice\Shippingmethods\Models\Shippingmethods;
use App\Containers\Backoffice\Shippingmethods\Tasks\FindShippingmethodsByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindShippingmethodsByIdAction extends Action
{
    public function run(Request $request): Shippingmethods
    {
        return app(FindShippingmethodsByIdTask::class)->run($request->id);
    }
}
