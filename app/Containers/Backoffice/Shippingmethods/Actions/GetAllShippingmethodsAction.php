<?php

namespace App\Containers\Backoffice\Shippingmethods\Actions;

use App\Containers\Backoffice\Shippingmethods\Tasks\GetAllShippingmethodsTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllShippingmethodsAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllShippingmethodsTask::class)->addRequestCriteria()->run();
    }
}
