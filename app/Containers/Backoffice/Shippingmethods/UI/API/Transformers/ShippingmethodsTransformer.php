<?php

namespace App\Containers\Backoffice\Shippingmethods\UI\API\Transformers;

use App\Containers\Backoffice\Shippingmethods\Models\Shippingmethods;
use App\Ship\Parents\Transformers\Transformer;

class ShippingmethodsTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Shippingmethods $shippingmethods): array
    {
        $response = [
            'object' => $shippingmethods->getResourceKey(),
            'id' => $shippingmethods->getHashedKey(),
            'created_at' => $shippingmethods->created_at,
            'updated_at' => $shippingmethods->updated_at,
            'readable_created_at' => $shippingmethods->created_at->diffForHumans(),
            'readable_updated_at' => $shippingmethods->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $shippingmethods->id,
            // 'deleted_at' => $shippingmethods->deleted_at,
        ], $response);

        return $response;
    }
}
