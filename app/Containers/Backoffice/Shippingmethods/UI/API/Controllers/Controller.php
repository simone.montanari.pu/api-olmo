<?php

namespace App\Containers\Backoffice\Shippingmethods\UI\API\Controllers;

use App\Containers\Backoffice\Shippingmethods\UI\API\Requests\CreateShippingmethodsRequest;
use App\Containers\Backoffice\Shippingmethods\UI\API\Requests\DeleteShippingmethodsRequest;
use App\Containers\Backoffice\Shippingmethods\UI\API\Requests\GetAllShippingmethodsRequest;
use App\Containers\Backoffice\Shippingmethods\UI\API\Requests\FindShippingmethodsByIdRequest;
use App\Containers\Backoffice\Shippingmethods\UI\API\Requests\UpdateShippingmethodsRequest;
use App\Containers\Backoffice\Shippingmethods\UI\API\Transformers\ShippingmethodsTransformer;
use App\Containers\Backoffice\Shippingmethods\Actions\CreateShippingmethodsAction;
use App\Containers\Backoffice\Shippingmethods\Actions\FindShippingmethodsByIdAction;
use App\Containers\Backoffice\Shippingmethods\Actions\GetAllShippingmethodsAction;
use App\Containers\Backoffice\Shippingmethods\Actions\UpdateShippingmethodsAction;
use App\Containers\Backoffice\Shippingmethods\Actions\DeleteShippingmethodsAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;

class Controller extends ApiController
{
    public function createShippingmethods(CreateShippingmethodsRequest $request): JsonResponse
    {
        $shippingmethods = app(CreateShippingmethodsAction::class)->run($request);
        return $this->created($this->transform($shippingmethods, ShippingmethodsTransformer::class));
    }

    public function findShippingmethodsById(FindShippingmethodsByIdRequest $request): array
    {
        $shippingmethods = app(FindShippingmethodsByIdAction::class)->run($request);
        return $this->transform($shippingmethods, ShippingmethodsTransformer::class);
    }

    public function getAllShippingmethods(GetAllShippingmethodsRequest $request): array
    {
        $shippingmethods = app(GetAllShippingmethodsAction::class)->run($request);
        return $this->transform($shippingmethods, ShippingmethodsTransformer::class);
    }

    public function updateShippingmethods(UpdateShippingmethodsRequest $request): array
    {
        $shippingmethods = app(UpdateShippingmethodsAction::class)->run($request);
        return $this->transform($shippingmethods, ShippingmethodsTransformer::class);
    }

    public function deleteShippingmethods(DeleteShippingmethodsRequest $request): JsonResponse
    {
        app(DeleteShippingmethodsAction::class)->run($request);
        return $this->noContent();
    }
}
