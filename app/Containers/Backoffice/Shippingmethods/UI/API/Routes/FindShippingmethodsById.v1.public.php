<?php

/**
 * @apiGroup           Shippingmethods
 * @apiName            findShippingmethodsById
 *
 * @api                {GET} /v1/shippingmethods/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Shippingmethods\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('shippingmethods/{id}', [Controller::class, 'findShippingmethodsById'])
    ->name('api_shippingmethods_find_shippingmethods_by_id')
    ->middleware(['auth:api']);

