<?php

/**
 * @apiGroup           Shippingmethods
 * @apiName            deleteShippingmethods
 *
 * @api                {DELETE} /v1/shippingmethods/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Shippingmethods\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::delete('shippingmethods/{id}', [Controller::class, 'deleteShippingmethods'])
    ->name('api_shippingmethods_delete_shippingmethods')
    ->middleware(['auth:api']);

