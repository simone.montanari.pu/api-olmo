<?php

namespace App\Containers\Backoffice\Shippingmethods\Data\Seeders;

use App\Ship\Parents\Seeders\Seeder;
use Illuminate\Support\Facades\DB;


class ShippingmethodsSeeder extends Seeder
{
    public function run()
    {
       
		DB::table('olmo_shippingmethod')->insert([
			'code_txt_general' 				=> 'DHL',
			'name_txt_general' 				=> 'Corriere espresso',
			'description_txt_general'       => "Entro 3 giorni lavorativi",
			'cost_txt_general'				=> '10',
			'ship_is_general'				=> 'true',
			'billing_is_general'            => 'true',
			'locale_hidden_general'         => "en",
			'parentid_hidden_general'		=> "0"
		]);

	
		DB::table('olmo_shippingmethod')->insert([
			'code_txt_general' 				=> 'Poste',
			'name_txt_general' 				=> 'Posta ordinaria',
			'description_txt_general'       => "Entro 10 giorni lavorativi",
			'cost_txt_general'				=> '130',
			'ship_is_general'				=> 'true',
			'billing_is_general'            => 'true',
			'locale_hidden_general'         => "en",
			'parentid_hidden_general'		=> "0"
		]);


    }
}