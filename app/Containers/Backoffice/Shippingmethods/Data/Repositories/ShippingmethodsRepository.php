<?php

namespace App\Containers\Backoffice\Shippingmethods\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class ShippingmethodsRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
