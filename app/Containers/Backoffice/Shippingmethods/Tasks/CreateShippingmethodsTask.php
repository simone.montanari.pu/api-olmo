<?php

namespace App\Containers\Backoffice\Shippingmethods\Tasks;

use App\Containers\Backoffice\Shippingmethods\Data\Repositories\ShippingmethodsRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateShippingmethodsTask extends Task
{
    protected ShippingmethodsRepository $repository;

    public function __construct(ShippingmethodsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
