<?php

namespace App\Containers\Backoffice\Shippingmethods\Tasks;

use App\Containers\Backoffice\Shippingmethods\Data\Repositories\ShippingmethodsRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteShippingmethodsTask extends Task
{
    protected ShippingmethodsRepository $repository;

    public function __construct(ShippingmethodsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id): ?int
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
