<?php

namespace App\Containers\Backoffice\Shippingmethods\Tasks;

use App\Containers\Backoffice\Shippingmethods\Data\Repositories\ShippingmethodsRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindShippingmethodsByIdTask extends Task
{
    protected ShippingmethodsRepository $repository;

    public function __construct(ShippingmethodsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
