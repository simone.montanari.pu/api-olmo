<?php

namespace App\Containers\Backoffice\Shippingmethods\Tasks;

use App\Containers\Backoffice\Shippingmethods\Data\Repositories\ShippingmethodsRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateShippingmethodsTask extends Task
{
    protected ShippingmethodsRepository $repository;

    public function __construct(ShippingmethodsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
