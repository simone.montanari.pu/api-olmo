<?php

namespace App\Containers\Backoffice\Shippingmethods\Tasks;

use App\Containers\Backoffice\Shippingmethods\Data\Repositories\ShippingmethodsRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllShippingmethodsTask extends Task
{
    protected ShippingmethodsRepository $repository;

    public function __construct(ShippingmethodsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
