<?php

namespace App\Containers\Backoffice\Visual\Tasks;

use App\Containers\Backoffice\Visual\Data\Repositories\VisualRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindVisualByIdTask extends Task
{
    protected VisualRepository $repository;

    public function __construct(VisualRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
