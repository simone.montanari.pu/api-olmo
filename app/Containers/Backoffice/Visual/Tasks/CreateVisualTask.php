<?php

namespace App\Containers\Backoffice\Visual\Tasks;

use App\Containers\Backoffice\Visual\Data\Repositories\VisualRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateVisualTask extends Task
{
    protected VisualRepository $repository;

    public function __construct(VisualRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
