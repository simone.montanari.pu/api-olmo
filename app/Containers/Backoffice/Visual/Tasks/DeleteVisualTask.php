<?php

namespace App\Containers\Backoffice\Visual\Tasks;

use App\Containers\Backoffice\Visual\Data\Repositories\VisualRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteVisualTask extends Task
{
    protected VisualRepository $repository;

    public function __construct(VisualRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id): ?int
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
