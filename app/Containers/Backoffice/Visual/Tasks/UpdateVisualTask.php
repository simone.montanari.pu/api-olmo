<?php

namespace App\Containers\Backoffice\Visual\Tasks;

use App\Containers\Backoffice\Visual\Data\Repositories\VisualRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateVisualTask extends Task
{
    protected VisualRepository $repository;

    public function __construct(VisualRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
