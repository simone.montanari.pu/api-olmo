<?php

namespace App\Containers\Backoffice\Visual\Tasks;

use App\Containers\Backoffice\Visual\Data\Repositories\VisualRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllVisualsTask extends Task
{
    protected VisualRepository $repository;

    public function __construct(VisualRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
