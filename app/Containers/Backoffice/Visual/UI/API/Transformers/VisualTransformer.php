<?php

namespace App\Containers\Backoffice\Visual\UI\API\Transformers;

use App\Containers\Backoffice\Visual\Models\Visual;
use App\Ship\Parents\Transformers\Transformer;

class VisualTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Visual $visual): array
    {
        $response = [
            'object' => $visual->getResourceKey(),
            'id' => $visual->getHashedKey(),
            'created_at' => $visual->created_at,
            'updated_at' => $visual->updated_at,
            'readable_created_at' => $visual->created_at->diffForHumans(),
            'readable_updated_at' => $visual->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $visual->id,
            // 'deleted_at' => $visual->deleted_at,
        ], $response);

        return $response;
    }
}
