<?php

namespace App\Containers\Backoffice\Visual\UI\API\Controllers;

use App\Containers\Backoffice\Visual\UI\API\Requests\CreateVisualRequest;
use App\Containers\Backoffice\Visual\UI\API\Requests\DeleteVisualRequest;
use App\Containers\Backoffice\Visual\UI\API\Requests\GetAllVisualsRequest;
use App\Containers\Backoffice\Visual\UI\API\Requests\FindVisualByIdRequest;
use App\Containers\Backoffice\Visual\UI\API\Requests\UpdateVisualRequest;
use App\Containers\Backoffice\Visual\UI\API\Transformers\VisualTransformer;
use App\Containers\Backoffice\Visual\Actions\CreateVisualAction;
use App\Containers\Backoffice\Visual\Actions\FindVisualByIdAction;
use App\Containers\Backoffice\Visual\Actions\GetAllVisualsAction;
use App\Containers\Backoffice\Visual\Actions\UpdateVisualAction;
use App\Containers\Backoffice\Visual\Actions\DeleteVisualAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use App\Ship\Parents\Controllers\ApiCollection;
use Illuminate\Support\Facades\DB;

class Controller extends ApiController
{
    public function getComponentVisual(CreateVisualRequest $request)
    {
       return ApiCollection::visualComponent();
    }

    public function addVisual(CreateVisualRequest $request)
    {
        $visual = ApiCollection::visualComponent();
        $component_block = $visual['blocks'][$request->id_visual];

        $model       = $request->model;
        $model_id    = $request->model_id;
        $model_field = $request->model_field;
        $table       = 'olmo_'.$model;

        $block =   json_decode(@Db::table($table)->where('id',$model_id)->first()->{$model_field});

        $cnt   = 0;
        if($block){
        foreach($block as $b){
            foreach($b  as $a){
                if($a->name == 'id')
                {
                   $part = explode('-',$a->value);
                   if($part[0] == $request->id_visual){
                       if(isset($part[1])){
                           $cnt = $part[1] +1;
                       }else{
                           $cnt++;
                       }
                   }
                }
            }
        }
        }

        $new_block = [];
        $n         = [];

        $n['name']     = 'id';
        $n['value']    = $request->id_visual.'-'.$cnt;
        $n['label']    = $component_block['name'];
        $n['type']     = 'hidden';
        $n['required'] = 'false';

        $new_block[] = $n;

        $n['name']     = 'testo';
        $n['value']    = '';
        $n['label']    = 'testo';
        $n['type']     = 'text';
        $n['required'] = 'false';

        $new_block[] = $n;

        $n['name']     = 'texteditor';
        $n['value']    = '';
        $n['label']    = 'texteditor';
        $n['type']     = 'texteditor';
        $n['required'] = 'false';

        $new_block[] = $n;
        
        $n['name']     = 'textarea';
        $n['value']    = '';
        $n['label']    = 'textarea';
        $n['type']     = 'textarea';
        $n['required'] = 'false';

        $new_block[] = $n;  

        $n['name']     = 'checkbox';
        $n['value']    = '';
        $n['label']    = 'checkbox';
        $n['type']     = 'checkbox';
        $n['required'] = 'false';

        $new_block[] = $n;         
        
        $n['name']     = 'singleimage';
        $n['value']    = '';
        $n['label']    = 'singleimage';
        $n['type']     = 'singleimage';
        $n['required'] = 'false';

        $new_block[] = $n; 
        
        $n['name']     = 'slider';
        $n['value']    = '';
        $n['label']    = 'slider';
        $n['type']     = 'slider';
        $n['required'] = 'false';

        $new_block[] = $n;         


        $block[] = $new_block;


        Db::table($table)->where('id',$model_id)->update([$model_field => json_encode($block)]);

        return $block;


        //print_r($block);
    }


}
