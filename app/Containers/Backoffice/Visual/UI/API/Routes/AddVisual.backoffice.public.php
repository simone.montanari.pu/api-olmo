<?php

/**
 * @apiGroup           Visual
 * @apiName            updateVisual
 *
 * @api                {PATCH} /v1/visuals/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Visual\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::put('en/{model}/{model_id}/visual/{model_field}/{id_visual}', [Controller::class, 'addVisual']);

