<?php

/**
 * @apiGroup           Visual
 * @apiName            createVisual
 *
 * @api                {POST} /v1/visuals Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Visual\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::post('visuals', [Controller::class, 'createVisual'])
    ->name('api_visual_create_visual')
    ->middleware(['auth:api']);

