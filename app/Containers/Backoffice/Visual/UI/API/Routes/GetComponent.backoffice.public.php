<?php

/**
 * @apiGroup           Visual
 * @apiName            findVisualById
 *
 * @api                {GET} /v1/visuals/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Visual\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('visual', [Controller::class, 'getComponentVisual']);
