<?php

namespace App\Containers\Backoffice\Visual\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class VisualRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
