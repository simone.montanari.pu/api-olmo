<?php

namespace App\Containers\Backoffice\Visual\Actions;

use App\Containers\Backoffice\Visual\Tasks\GetAllVisualsTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllVisualsAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllVisualsTask::class)->addRequestCriteria()->run();
    }
}
