<?php

namespace App\Containers\Backoffice\Visual\Actions;

use App\Containers\Backoffice\Visual\Tasks\DeleteVisualTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteVisualAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteVisualTask::class)->run($request->id);
    }
}
