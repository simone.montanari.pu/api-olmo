<?php

namespace App\Containers\Backoffice\Visual\Actions;

use App\Containers\Backoffice\Visual\Models\Visual;
use App\Containers\Backoffice\Visual\Tasks\CreateVisualTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateVisualAction extends Action
{
    public function run(Request $request): Visual
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateVisualTask::class)->run($data);
    }
}
