<?php

namespace App\Containers\Backoffice\Visual\Actions;

use App\Containers\Backoffice\Visual\Models\Visual;
use App\Containers\Backoffice\Visual\Tasks\UpdateVisualTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateVisualAction extends Action
{
    public function run(Request $request): Visual
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateVisualTask::class)->run($request->id, $data);
    }
}
