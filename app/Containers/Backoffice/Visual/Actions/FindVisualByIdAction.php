<?php

namespace App\Containers\Backoffice\Visual\Actions;

use App\Containers\Backoffice\Visual\Models\Visual;
use App\Containers\Backoffice\Visual\Tasks\FindVisualByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindVisualByIdAction extends Action
{
    public function run(Request $request): Visual
    {
        return app(FindVisualByIdTask::class)->run($request->id);
    }
}
