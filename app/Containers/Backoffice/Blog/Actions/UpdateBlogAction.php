<?php

namespace App\Containers\Backoffice\Blog\Actions;

use App\Containers\Backoffice\Blog\Models\Blog;
use App\Containers\Backoffice\Blog\Tasks\UpdateBlogTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateBlogAction extends Action
{
    public function run(Request $request): Blog
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateBlogTask::class)->run($request->id, $data);
    }
}
