<?php

namespace App\Containers\Backoffice\Blog\Actions;

use App\Containers\Backoffice\Blog\Models\Blog;
use App\Containers\Backoffice\Blog\Tasks\FindBlogByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindBlogByIdAction extends Action
{
    public function run(Request $request): Blog
    {
        return app(FindBlogByIdTask::class)->run($request->id);
    }
}
