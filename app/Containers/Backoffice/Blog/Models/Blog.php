<?php

namespace App\Containers\Backoffice\Blog\Models;

use App\Ship\Parents\Models\Model;
use Illuminate\Support\Facades\DB;

class Blog extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $table = 'olmo_blog';

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Blog';

    public static function getAttr(){
        return [
            'table' => 'olmo_blog',
            'lang'  => true,
            'required' =>[
                'name_txt_general',
                'slug_txt_general',
                'template_id_general'
            ]
        ];
    }    
}
