<?php

namespace App\Containers\Backoffice\Blog\UI\API\Controllers;

use App\Containers\Backoffice\Blog\UI\API\Requests\CreateBlogRequest;
use App\Containers\Backoffice\Blog\UI\API\Requests\DeleteBlogRequest;
use App\Containers\Backoffice\Blog\UI\API\Requests\GetAllBlogsRequest;
use App\Containers\Backoffice\Blog\UI\API\Requests\FindBlogByIdRequest;
use App\Containers\Backoffice\Blog\UI\API\Requests\UpdateBlogRequest;
use App\Containers\Backoffice\Blog\UI\API\Transformers\BlogTransformer;
use App\Containers\Backoffice\Blog\Actions\CreateBlogAction;
use App\Containers\Backoffice\Blog\Actions\FindBlogByIdAction;
use App\Containers\Backoffice\Blog\Actions\GetAllBlogsAction;
use App\Containers\Backoffice\Blog\Actions\UpdateBlogAction;
use App\Containers\Backoffice\Blog\Actions\DeleteBlogAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use App\Containers\Backoffice\Blog\Models\Blog;


class Controller extends ApiController
{
   
    public function getListV1(GetAllBlogsRequest $request)
    {
        $lang = $request->lang;
        $datas      = Blog::select('name_txt_general','cover_img_content',"lang_langs_general",'enabled_is_general','template_id_general','id')
        ->where('locale_hidden_general',$lang)
        ->get()
        ->toArray();
        $attribute  = Blog::getAttr();
        return  $this->getList($datas,$attribute);

    }

    public function findById(GetAllBlogsRequest $request)
    { 

        $attribute  = Blog::getAttr();
    	$id 		= $request->id;
        $lang       = $request->lang;
        if($id == 0){
            return $this->emptyItem($attribute,$lang);
        }else{
    	   $datas      = Blog::where('id',$id)->get()->toArray();
           $attribute  = Blog::getAttr();
           return  $this->getSingleList($datas,$attribute,$id,$lang);
        }
    }


    public function createUpdateV1(GetAllBlogsRequest $request)
    {
    	$attribute  = Blog::getAttr();
    	return $this->createUpdate($request,$attribute);
    }

   
}