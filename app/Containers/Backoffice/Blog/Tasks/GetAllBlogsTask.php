<?php

namespace App\Containers\Backoffice\Blog\Tasks;

use App\Containers\Backoffice\Blog\Data\Repositories\BlogRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllBlogsTask extends Task
{
    protected BlogRepository $repository;

    public function __construct(BlogRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
