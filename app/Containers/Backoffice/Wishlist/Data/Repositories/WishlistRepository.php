<?php

namespace App\Containers\Backoffice\Wishlist\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class WishlistRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
