<?php

namespace App\Containers\Backoffice\Wishlist\UI\API\Transformers;

use App\Containers\Backoffice\Wishlist\Models\Wishlist;
use App\Ship\Parents\Transformers\Transformer;

class WishlistTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Wishlist $wishlist): array
    {
        $response = [
            'object' => $wishlist->getResourceKey(),
            'id' => $wishlist->getHashedKey(),
            'created_at' => $wishlist->created_at,
            'updated_at' => $wishlist->updated_at,
            'readable_created_at' => $wishlist->created_at->diffForHumans(),
            'readable_updated_at' => $wishlist->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $wishlist->id,
            // 'deleted_at' => $wishlist->deleted_at,
        ], $response);

        return $response;
    }
}
