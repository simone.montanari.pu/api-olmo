<?php

namespace App\Containers\Backoffice\Wishlist\UI\API\Controllers;

use App\Containers\Backoffice\Wishlist\UI\API\Requests\CreateWishlistRequest;
use App\Containers\Backoffice\Wishlist\UI\API\Requests\DeleteWishlistRequest;
use App\Containers\Backoffice\Wishlist\UI\API\Requests\GetAllWishlistsRequest;
use App\Containers\Backoffice\Wishlist\UI\API\Requests\FindWishlistByIdRequest;
use App\Containers\Backoffice\Wishlist\UI\API\Requests\UpdateWishlistRequest;
use App\Containers\Backoffice\Wishlist\UI\API\Transformers\WishlistTransformer;
use App\Containers\Backoffice\Wishlist\Actions\CreateWishlistAction;
use App\Containers\Backoffice\Wishlist\Actions\FindWishlistByIdAction;
use App\Containers\Backoffice\Wishlist\Actions\GetAllWishlistsAction;
use App\Containers\Backoffice\Wishlist\Actions\UpdateWishlistAction;
use App\Containers\Backoffice\Wishlist\Actions\DeleteWishlistAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;

class Controller extends ApiController
{
    public function createWishlist(CreateWishlistRequest $request): JsonResponse
    {
        $wishlist = app(CreateWishlistAction::class)->run($request);
        return $this->created($this->transform($wishlist, WishlistTransformer::class));
    }

    public function findWishlistById(FindWishlistByIdRequest $request): array
    {
        $wishlist = app(FindWishlistByIdAction::class)->run($request);
        return $this->transform($wishlist, WishlistTransformer::class);
    }

    public function getAllWishlists(GetAllWishlistsRequest $request): array
    {
        $wishlists = app(GetAllWishlistsAction::class)->run($request);
        return $this->transform($wishlists, WishlistTransformer::class);
    }

    public function updateWishlist(UpdateWishlistRequest $request): array
    {
        $wishlist = app(UpdateWishlistAction::class)->run($request);
        return $this->transform($wishlist, WishlistTransformer::class);
    }

    public function deleteWishlist(DeleteWishlistRequest $request): JsonResponse
    {
        app(DeleteWishlistAction::class)->run($request);
        return $this->noContent();
    }
}
