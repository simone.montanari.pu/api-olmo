<?php

/**
 * @apiGroup           Wishlist
 * @apiName            deleteWishlist
 *
 * @api                {DELETE} /v1/wishlists/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Wishlist\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::delete('wishlists/{id}', [Controller::class, 'deleteWishlist'])
    ->name('api_wishlist_delete_wishlist')
    ->middleware(['auth:api']);

