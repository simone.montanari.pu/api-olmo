<?php

/**
 * @apiGroup           Wishlist
 * @apiName            getAllWishlists
 *
 * @api                {GET} /v1/wishlists Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Wishlist\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('wishlists', [Controller::class, 'getAllWishlists'])
    ->name('api_wishlist_get_all_wishlists')
    ->middleware(['auth:api']);

