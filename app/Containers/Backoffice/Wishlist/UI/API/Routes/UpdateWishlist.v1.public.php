<?php

/**
 * @apiGroup           Wishlist
 * @apiName            updateWishlist
 *
 * @api                {PATCH} /v1/wishlists/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Wishlist\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::patch('wishlists/{id}', [Controller::class, 'updateWishlist'])
    ->name('api_wishlist_update_wishlist')
    ->middleware(['auth:api']);

