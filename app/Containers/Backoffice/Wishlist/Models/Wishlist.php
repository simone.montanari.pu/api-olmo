<?php

namespace App\Containers\Backoffice\Wishlist\Models;

use App\Ship\Parents\Models\Model;

class Wishlist extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $table = 'olmo_wishlist';

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Wishlist';

    public static function getAttr(){
        return ['table' => 'olmo_wishlist'];
    }
}
