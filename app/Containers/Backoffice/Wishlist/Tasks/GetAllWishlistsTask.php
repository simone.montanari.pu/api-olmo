<?php

namespace App\Containers\Backoffice\Wishlist\Tasks;

use App\Containers\Backoffice\Wishlist\Data\Repositories\WishlistRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllWishlistsTask extends Task
{
    protected WishlistRepository $repository;

    public function __construct(WishlistRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
