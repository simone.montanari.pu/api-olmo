<?php

namespace App\Containers\Backoffice\Wishlist\Tasks;

use App\Containers\Backoffice\Wishlist\Data\Repositories\WishlistRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindWishlistByIdTask extends Task
{
    protected WishlistRepository $repository;

    public function __construct(WishlistRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
