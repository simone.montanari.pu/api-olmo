<?php

namespace App\Containers\Backoffice\Wishlist\Tasks;

use App\Containers\Backoffice\Wishlist\Data\Repositories\WishlistRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteWishlistTask extends Task
{
    protected WishlistRepository $repository;

    public function __construct(WishlistRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id): ?int
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
