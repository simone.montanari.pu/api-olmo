<?php

namespace App\Containers\Backoffice\Wishlist\Tasks;

use App\Containers\Backoffice\Wishlist\Data\Repositories\WishlistRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateWishlistTask extends Task
{
    protected WishlistRepository $repository;

    public function __construct(WishlistRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
