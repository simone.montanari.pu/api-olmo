<?php

namespace App\Containers\Backoffice\Wishlist\Tasks;

use App\Containers\Backoffice\Wishlist\Data\Repositories\WishlistRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateWishlistTask extends Task
{
    protected WishlistRepository $repository;

    public function __construct(WishlistRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
