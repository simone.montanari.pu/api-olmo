<?php

namespace App\Containers\Backoffice\Wishlist\Actions;

use App\Containers\Backoffice\Wishlist\Tasks\DeleteWishlistTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteWishlistAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteWishlistTask::class)->run($request->id);
    }
}
