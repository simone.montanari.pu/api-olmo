<?php

namespace App\Containers\Backoffice\Wishlist\Actions;

use App\Containers\Backoffice\Wishlist\Models\Wishlist;
use App\Containers\Backoffice\Wishlist\Tasks\FindWishlistByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindWishlistByIdAction extends Action
{
    public function run(Request $request): Wishlist
    {
        return app(FindWishlistByIdTask::class)->run($request->id);
    }
}
