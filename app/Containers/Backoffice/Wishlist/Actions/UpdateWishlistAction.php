<?php

namespace App\Containers\Backoffice\Wishlist\Actions;

use App\Containers\Backoffice\Wishlist\Models\Wishlist;
use App\Containers\Backoffice\Wishlist\Tasks\UpdateWishlistTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateWishlistAction extends Action
{
    public function run(Request $request): Wishlist
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateWishlistTask::class)->run($request->id, $data);
    }
}
