<?php

namespace App\Containers\Backoffice\Wishlist\Actions;

use App\Containers\Backoffice\Wishlist\Tasks\GetAllWishlistsTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllWishlistsAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllWishlistsTask::class)->addRequestCriteria()->run();
    }
}
