<?php

/**
 * @apiGroup           Product
 * @apiName            getAllProducts
 *
 * @api                {GET} /v1/products Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
 */

use App\Containers\Backoffice\Product\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::post('{lang}/product', [Controller::class, 'getListV1'])->middleware(['auth:api']);