<?php

/**
 * @apiGroup           Product
 * @apiName            deleteProduct
 *
 * @api                {DELETE} /v1/products/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
 */

use App\Containers\Backoffice\Product\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::delete('products/{id}', [Controller::class, 'deleteProduct'])
    ->name('api_product_delete_product')
    ->middleware(['auth:api']);

