<?php

namespace App\Containers\Backoffice\Product\UI\API\Transformers;

use App\Containers\Backoffice\Product\Models\Product;
use App\Ship\Parents\Transformers\Transformer;

class ProductTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Product $product): array
    {
        $response = [
            'object' => $product->getResourceKey(),
            'id' => $product->getHashedKey(),
            'created_at' => $product->created_at,
            'updated_at' => $product->updated_at,
            'readable_created_at' => $product->created_at->diffForHumans(),
            'readable_updated_at' => $product->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $product->id,
            // 'deleted_at' => $product->deleted_at,
        ], $response);

        return $response;
    }
}
