<?php

namespace App\Containers\Backoffice\Product\UI\API\Controllers;

use App\Containers\Backoffice\Product\UI\API\Requests\CreateProductRequest;
use App\Containers\Backoffice\Product\UI\API\Requests\DeleteProductRequest;
use App\Containers\Backoffice\Product\UI\API\Requests\GetAllProductsRequest;
use App\Containers\Backoffice\Product\UI\API\Requests\FindProductByIdRequest;
use App\Containers\Backoffice\Product\UI\API\Requests\UpdateProductRequest;
use App\Containers\Backoffice\Product\UI\API\Transformers\ProductTransformer;
use App\Containers\Backoffice\Product\Actions\CreateProductAction;
use App\Containers\Backoffice\Product\Actions\FindProductByIdAction;
use App\Containers\Backoffice\Product\Actions\GetAllProductsAction;
use App\Containers\Backoffice\Product\Actions\UpdateProductAction;
use App\Containers\Backoffice\Product\Actions\DeleteProductAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use App\Containers\Backoffice\Product\Models\Product;
use App\Ship\Parents\Controllers\ApiHelper;
use Illuminate\Support\Facades\DB;


class Controller extends ApiController
{
   
    public function getListV1(GetAllProductsRequest $request)
    {
        $lang = $request->lang;
        $datas      = Product::select('name_txt_general','cover_img_content',"lang_langs_general",'enabled_is_general','template_id_general','id')
        ->where('locale_hidden_general',$lang)
        ->get()
        ->toArray();
        $attribute  = Product::getAttr();
        return  $this->getList($datas,$attribute);

    }

    public function findById(GetAllProductsRequest $request)
    { 

        $attribute  = Product::getAttr();
    	$id 		= $request->id;
        $lang       = $request->lang;
        if($id == 0){
            return $this->emptyItem($attribute,$lang);
        }else{
    	   $datas      = Product::where('id',$id)->get()->toArray();
           $attribute  = Product::getAttr();
           return  $this->getSingleList($datas,$attribute,$id,$lang);
        }
    }


    public function createUpdateV1(GetAllProductsRequest $request)
    {
        $attribute  = Product::getAttr();
        $rules       = $attribute['rules'];
        $required    = $attribute['required'];
        if($required){
            $res = ApiHelper::CheckRequired($request,$required);
            if($res['valid'] == false){
                return response($res['err'], 400);
                exit();
            }
        }
        if($rules){
            $res = ApiHelper::CheckRules($request,$rules);
            if($res['valid'] == false){
                return response($res['err'], 400);
                exit();
            }
        }
    	return $this->createUpdate($request,$attribute);
    }

   
}
