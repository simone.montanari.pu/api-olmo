<?php

namespace App\Containers\Backoffice\Product\Models;

use App\Ship\Parents\Models\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $table = 'olmo_product';

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Product';

    public static function getAttr(){
        return [
            'table' => 'olmo_product',
            'lang'  => true,
            'default' =>[
                'qty_spin_general' => '0'
            ],
            'required' =>[
                'name_txt_general',
                'slug_txt_general',
                'template_id_general'
            ],
            'rules' => [
                [
                    'type' => 'unique',
                    'field' => 'slug_txt_general',
                    'table' => 'olmo_product',
                    'errortxt' => 'Slug duplicate in product'
                ]               
            ]            
        ];
    }

}
