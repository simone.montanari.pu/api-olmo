<?php

namespace App\Containers\Backoffice\Product\Tasks;

use App\Containers\Backoffice\Product\Data\Repositories\ProductRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllProductsTask extends Task
{
    protected ProductRepository $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
