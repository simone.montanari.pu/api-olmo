<?php

namespace App\Containers\Backoffice\Product\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class ProductRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
