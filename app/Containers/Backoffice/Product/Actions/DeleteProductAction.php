<?php

namespace App\Containers\Backoffice\Product\Actions;

use App\Containers\Backoffice\Product\Tasks\DeleteProductTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteProductAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteProductTask::class)->run($request->id);
    }
}
