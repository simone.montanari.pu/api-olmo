<?php

namespace App\Containers\Backoffice\Product\Actions;

use App\Containers\Backoffice\Product\Tasks\GetAllProductsTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllProductsAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllProductsTask::class)->addRequestCriteria()->run();
    }
}
