<?php

namespace App\Containers\Backoffice\Product\Actions;

use App\Containers\Backoffice\Product\Models\Product;
use App\Containers\Backoffice\Product\Tasks\FindProductByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindProductByIdAction extends Action
{
    public function run(Request $request): Product
    {
        return app(FindProductByIdTask::class)->run($request->id);
    }
}
