<?php

namespace App\Containers\Backoffice\Product\Actions;

use App\Containers\Backoffice\Product\Models\Product;
use App\Containers\Backoffice\Product\Tasks\CreateProductTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateProductAction extends Action
{
    public function run(Request $request): Product
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateProductTask::class)->run($data);
    }
}
