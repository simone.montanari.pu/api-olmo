<?php

namespace App\Containers\Backoffice\Template\UI\API\Controllers;

use App\Containers\Backoffice\Template\UI\API\Requests\CreateTemplateRequest;
use App\Containers\Backoffice\Template\UI\API\Requests\DeleteTemplateRequest;
use App\Containers\Backoffice\Template\UI\API\Requests\GetAllTemplatesRequest;
use App\Containers\Backoffice\Template\UI\API\Requests\FindTemplateByIdRequest;
use App\Containers\Backoffice\Template\UI\API\Requests\UpdateTemplateRequest;
use App\Containers\Backoffice\Template\UI\API\Transformers\TemplateTransformer;
use App\Containers\Backoffice\Template\Actions\CreateTemplateAction;
use App\Containers\Backoffice\Template\Actions\FindTemplateByIdAction;
use App\Containers\Backoffice\Template\Actions\GetAllTemplatesAction;
use App\Containers\Backoffice\Template\Actions\UpdateTemplateAction;
use App\Containers\Backoffice\Template\Actions\DeleteTemplateAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use App\Containers\Backoffice\Template\Models\Template;

class Controller extends ApiController
{
    public function getListV1(GetAllTemplatesRequest $request)
    {
        $lang       = $request->lang;
    	$datas   = Template::select('name_txt_general','slug_txt_general','lang_langs_general','model_select_general as model_txt_general','enabled_is_general','id')
        ->where('locale_hidden_general',$lang)->get()->toArray();

        $attribute  = Template::getAttr();
        return  $this->getList($datas,$attribute);

    }


    public function createUpdateV1(GetAllTemplatesRequest $request)
    {
    	$attribute  = Template::getAttr();
    	return $this->createUpdate($request,$attribute);
    }

    public function findById(GetAllTemplatesRequest $request)
    { 
        $lang       = $request->lang;
        $attribute  = Template::getAttr();
        $id         = $request->id;
        if($id == 0){
            return $this->emptyItem($attribute,$lang);
        }else{
           $datas      = Template::where('id',$id)->get()->toArray();
           $attribute  = Template::getAttr();
           return  $this->getSingleList($datas,$attribute,$id,$lang);
        }
    }

}
