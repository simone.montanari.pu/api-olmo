<?php

namespace App\Containers\Backoffice\Template\UI\API\Transformers;

use App\Containers\Backoffice\Template\Models\Template;
use App\Ship\Parents\Transformers\Transformer;

class TemplateTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Template $template): array
    {
        $response = [
            'object' => $template->getResourceKey(),
            'id' => $template->getHashedKey(),
            'created_at' => $template->created_at,
            'updated_at' => $template->updated_at,
            'readable_created_at' => $template->created_at->diffForHumans(),
            'readable_updated_at' => $template->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $template->id,
            // 'deleted_at' => $template->deleted_at,
        ], $response);

        return $response;
    }
}
