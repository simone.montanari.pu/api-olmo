<?php

/**
 * @apiGroup           Template
 * @apiName            deleteTemplate
 *
 * @api                {DELETE} /v1/templates/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Template\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::delete('templates/{id}', [Controller::class, 'deleteTemplate'])
    ->name('api_template_delete_template')
    ->middleware(['auth:api']);

