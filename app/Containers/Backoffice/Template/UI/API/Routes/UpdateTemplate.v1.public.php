<?php

/**
 * @apiGroup           Template
 * @apiName            updateTemplate
 *
 * @api                {PATCH} /v1/templates/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Template\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::patch('templates/{id}', [Controller::class, 'updateTemplate'])
    ->name('api_template_update_template')
    ->middleware(['auth:api']);

