<?php

namespace App\Containers\Backoffice\Template\Tasks;

use App\Containers\Backoffice\Template\Data\Repositories\TemplateRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllTemplatesTask extends Task
{
    protected TemplateRepository $repository;

    public function __construct(TemplateRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
