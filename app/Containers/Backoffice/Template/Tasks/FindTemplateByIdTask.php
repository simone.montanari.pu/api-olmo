<?php

namespace App\Containers\Backoffice\Template\Tasks;

use App\Containers\Backoffice\Template\Data\Repositories\TemplateRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindTemplateByIdTask extends Task
{
    protected TemplateRepository $repository;

    public function __construct(TemplateRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
