<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Ship\Parents\Controllers\ApiMigration;

class CreateTemplateTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('olmo_template')) {
            Schema::table('olmo_template', function (Blueprint $table) {
                // Update existing table...
            });
        } else {
            Schema::create('olmo_template', function (Blueprint $table) {
                // Create new table...
                $table->charset = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';
                // General
                $table->increments('id')->unsigned();
                $table->text('lang_langs_general')->nullable();
                $table->string('enabled_is_general', 5)->nullable();
                $table->text('name_txt_general')->nullable();
                $table->text('slug_txt_general')->nullable();
                $table->text('model_select_general')->nullable();                
                $table->text('locale_hidden_general')->nullable();
                $table->text('parentid_hidden_general')->nullable();                
                // Sitemap
                $table->string('activate_is_sitemap', 5)->nullable();
                $table->text('frequency_select_sitemap')->nullable();
                $table->text('priority_select_sitemap')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_template');
    }
}
