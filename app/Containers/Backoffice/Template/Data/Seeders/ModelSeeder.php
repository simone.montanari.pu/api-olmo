<?php

namespace App\Containers\Backoffice\Template\Data\Seeders;

use App\Ship\Parents\Seeders\Seeder;
use Illuminate\Support\Facades\DB;

class ModelSeeder extends Seeder
{
    public function run()
    {
        // ...
        DB::table('olmo_model')->truncate();
       
		DB::table('olmo_model')->insert([
			'name_txt_general'      => 'page'
		]);

		DB::table('olmo_model')->insert([
			'name_txt_general'      => 'product'
		]);

		DB::table('olmo_model')->insert([
			'name_txt_general'      => 'category'
		]);

		DB::table('olmo_model')->insert([
			'name_txt_general'      => 'blog'
		]);

        DB::table('olmo_model')->insert([
			'name_txt_general'      => 'casehistory'
		]);

        DB::table('olmo_model')->insert([
			'name_txt_general'      => 'service'
		]);        

    }
}
