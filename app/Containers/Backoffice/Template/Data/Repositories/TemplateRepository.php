<?php

namespace App\Containers\Backoffice\Template\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class TemplateRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
