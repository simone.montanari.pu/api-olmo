<?php

namespace App\Containers\Backoffice\Template\Models;

use App\Ship\Parents\Models\Model;

class Template extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $table = 'olmo_template';

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Template';

    public static function getAttr(){
        return ['table' => 'olmo_template','lang'  => true];
    }

}
