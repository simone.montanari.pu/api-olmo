<?php

namespace App\Containers\Backoffice\Template\Actions;

use App\Containers\Backoffice\Template\Tasks\GetAllTemplatesTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllTemplatesAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllTemplatesTask::class)->addRequestCriteria()->run();
    }
}
