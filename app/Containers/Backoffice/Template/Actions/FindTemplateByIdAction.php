<?php

namespace App\Containers\Backoffice\Template\Actions;

use App\Containers\Backoffice\Template\Models\Template;
use App\Containers\Backoffice\Template\Tasks\FindTemplateByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindTemplateByIdAction extends Action
{
    public function run(Request $request): Template
    {
        return app(FindTemplateByIdTask::class)->run($request->id);
    }
}
