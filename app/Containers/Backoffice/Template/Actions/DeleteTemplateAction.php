<?php

namespace App\Containers\Backoffice\Template\Actions;

use App\Containers\Backoffice\Template\Tasks\DeleteTemplateTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteTemplateAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteTemplateTask::class)->run($request->id);
    }
}
