<?php

namespace App\Containers\Backoffice\Template\Actions;

use App\Containers\Backoffice\Template\Models\Template;
use App\Containers\Backoffice\Template\Tasks\UpdateTemplateTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateTemplateAction extends Action
{
    public function run(Request $request): Template
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateTemplateTask::class)->run($request->id, $data);
    }
}
