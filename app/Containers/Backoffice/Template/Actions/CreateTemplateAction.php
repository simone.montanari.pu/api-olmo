<?php

namespace App\Containers\Backoffice\Template\Actions;

use App\Containers\Backoffice\Template\Models\Template;
use App\Containers\Backoffice\Template\Tasks\CreateTemplateTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateTemplateAction extends Action
{
    public function run(Request $request): Template
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateTemplateTask::class)->run($data);
    }
}
