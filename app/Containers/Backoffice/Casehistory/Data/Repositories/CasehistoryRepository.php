<?php

namespace App\Containers\Backoffice\Casehistory\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class CasehistoryRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
