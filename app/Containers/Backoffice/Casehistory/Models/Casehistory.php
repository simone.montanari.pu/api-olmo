<?php

namespace App\Containers\Backoffice\Casehistory\Models;

use App\Ship\Parents\Models\Model;

class Casehistory extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public $table = 'olmo_casehistory';

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Casehistory';

    public static function getAttr(){
        return [
            'table' => 'olmo_casehistory',
            'lang'  => true,
            'required' =>[
                'name_txt_general',
                'slug_txt_general',
                'template_id_general'
            ],
            'rules' => [
                [
                    'type' => 'unique',
                    'field' => 'slug_txt_general',
                    'table' => 'olmo_casehistory',
                    'errortxt' => 'Slug duplicate'
                ]               
            ] 
        ];
    }

}
