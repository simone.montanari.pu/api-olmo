<?php

namespace App\Containers\Backoffice\Casehistory\Actions;

use App\Containers\Backoffice\Casehistory\Models\Casehistory;
use App\Containers\Backoffice\Casehistory\Tasks\FindCasehistoryByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindCasehistoryByIdAction extends Action
{
    public function run(Request $request): Casehistory
    {
        return app(FindCasehistoryByIdTask::class)->run($request->id);
    }
}
