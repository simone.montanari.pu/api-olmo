<?php

namespace App\Containers\Backoffice\Casehistory\Actions;

use App\Containers\Backoffice\Casehistory\Tasks\GetAllCasehistoriesTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllCasehistoriesAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllCasehistoriesTask::class)->addRequestCriteria()->run();
    }
}
