<?php

namespace App\Containers\Backoffice\Casehistory\Actions;

use App\Containers\Backoffice\Casehistory\Models\Casehistory;
use App\Containers\Backoffice\Casehistory\Tasks\CreateCasehistoryTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateCasehistoryAction extends Action
{
    public function run(Request $request): Casehistory
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateCasehistoryTask::class)->run($data);
    }
}
