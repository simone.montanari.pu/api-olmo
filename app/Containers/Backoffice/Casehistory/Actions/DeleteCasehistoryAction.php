<?php

namespace App\Containers\Backoffice\Casehistory\Actions;

use App\Containers\Backoffice\Casehistory\Tasks\DeleteCasehistoryTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteCasehistoryAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteCasehistoryTask::class)->run($request->id);
    }
}
