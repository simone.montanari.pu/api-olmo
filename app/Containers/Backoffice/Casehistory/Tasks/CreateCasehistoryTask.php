<?php

namespace App\Containers\Backoffice\Casehistory\Tasks;

use App\Containers\Backoffice\Casehistory\Data\Repositories\CasehistoryRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateCasehistoryTask extends Task
{
    protected CasehistoryRepository $repository;

    public function __construct(CasehistoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
