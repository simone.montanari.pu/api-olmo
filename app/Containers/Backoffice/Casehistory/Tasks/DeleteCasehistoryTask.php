<?php

namespace App\Containers\Backoffice\Casehistory\Tasks;

use App\Containers\Backoffice\Casehistory\Data\Repositories\CasehistoryRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteCasehistoryTask extends Task
{
    protected CasehistoryRepository $repository;

    public function __construct(CasehistoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id): ?int
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
