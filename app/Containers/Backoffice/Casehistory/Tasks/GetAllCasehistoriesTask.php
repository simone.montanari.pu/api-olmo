<?php

namespace App\Containers\Backoffice\Casehistory\Tasks;

use App\Containers\Backoffice\Casehistory\Data\Repositories\CasehistoryRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllCasehistoriesTask extends Task
{
    protected CasehistoryRepository $repository;

    public function __construct(CasehistoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
