<?php

namespace App\Containers\Backoffice\Casehistory\Tasks;

use App\Containers\Backoffice\Casehistory\Data\Repositories\CasehistoryRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindCasehistoryByIdTask extends Task
{
    protected CasehistoryRepository $repository;

    public function __construct(CasehistoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
