<?php

namespace App\Containers\Backoffice\Casehistory\Tasks;

use App\Containers\Backoffice\Casehistory\Data\Repositories\CasehistoryRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateCasehistoryTask extends Task
{
    protected CasehistoryRepository $repository;

    public function __construct(CasehistoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
