<?php

namespace App\Containers\Backoffice\Casehistory\UI\API\Transformers;

use App\Containers\Backoffice\Casehistory\Models\Casehistory;
use App\Ship\Parents\Transformers\Transformer;

class CasehistoryTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Casehistory $casehistory): array
    {
        $response = [
            'object' => $casehistory->getResourceKey(),
            'id' => $casehistory->getHashedKey(),
            'created_at' => $casehistory->created_at,
            'updated_at' => $casehistory->updated_at,
            'readable_created_at' => $casehistory->created_at->diffForHumans(),
            'readable_updated_at' => $casehistory->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $casehistory->id,
            // 'deleted_at' => $casehistory->deleted_at,
        ], $response);

        return $response;
    }
}
