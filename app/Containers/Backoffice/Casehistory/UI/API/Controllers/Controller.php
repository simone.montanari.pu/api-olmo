<?php

namespace App\Containers\Backoffice\Casehistory\UI\API\Controllers;

use App\Containers\Backoffice\Casehistory\UI\API\Requests\CreateCasehistoryRequest;
use App\Containers\Backoffice\Casehistory\UI\API\Requests\DeleteCasehistoryRequest;
use App\Containers\Backoffice\Casehistory\UI\API\Requests\GetAllCasehistoriesRequest;
use App\Containers\Backoffice\Casehistory\UI\API\Requests\FindCasehistoryByIdRequest;
use App\Containers\Backoffice\Casehistory\UI\API\Requests\UpdateCasehistoryRequest;
use App\Containers\Backoffice\Casehistory\UI\API\Transformers\CasehistoryTransformer;
use App\Containers\Backoffice\Casehistory\Actions\CreateCasehistoryAction;
use App\Containers\Backoffice\Casehistory\Actions\FindCasehistoryByIdAction;
use App\Containers\Backoffice\Casehistory\Actions\GetAllCasehistoriesAction;
use App\Containers\Backoffice\Casehistory\Actions\UpdateCasehistoryAction;
use App\Containers\Backoffice\Casehistory\Actions\DeleteCasehistoryAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use App\Containers\Backoffice\Casehistory\Models\Casehistory;
use App\Ship\Parents\Controllers\ApiHelper;
use Illuminate\Support\Facades\DB;

class Controller extends ApiController
{
    public function getListV1(GetAllCasehistoriesRequest $request)
    {
        $lang = $request->lang;
    	$datas   = Casehistory::select('name_txt_general','slug_txt_general','enabled_is_general','id')  
        ->where('locale_hidden_general',$lang)
        ->get()
        ->toArray();

        $attribute  = Casehistory::getAttr();
        return  $this->getList($datas,$attribute);

    }


    public function createUpdateV1(GetAllCasehistoriesRequest $request)
    {
        $attribute  = Casehistory::getAttr();
        $rules       = $attribute['rules'];
        $required    = $attribute['required'];
        if($required){
            $res = ApiHelper::CheckRequired($request,$required);
            if($res['valid'] == false){
                return response($res['err'], 400);
                exit();
            }
        }
        if($rules){
            $res = ApiHelper::CheckRules($request,$rules);
            if($res['valid'] == false){
                return response($res['err'], 400);
                exit();
            }
        }
    	return $this->createUpdate($request,$attribute);
    }

    public function findById(GetAllCasehistoriesRequest $request)
    { 
        $attribute  = Casehistory::getAttr();
        $lang       = $request->lang;
        $id         = $request->id;
        if($id == 0){
            return $this->emptyItem($attribute,$lang);
        }else{
           $datas      = Casehistory::where('id',$id)->get()->toArray();
           $attribute  = Casehistory::getAttr();
           return  $this->getSingleList($datas,$attribute,$id,$lang);
        }
    }

}
