<?php

namespace App\Containers\Backoffice\Page\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class PageRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
