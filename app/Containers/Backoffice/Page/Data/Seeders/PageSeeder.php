<?php

namespace App\Containers\Backoffice\Page\Data\Seeders;

use App\Ship\Parents\Seeders\Seeder;
use Illuminate\Support\Facades\DB;

class PageSeeder extends Seeder
{
    public function run()
    {  
        DB::table('olmo_page')->insert([
            'enabled_is_general'        => 'true',
            'name_txt_general'          => 'Home',
            'locale_hidden_general'     => 'en',
            'slug_txt_general'          => '/',
            'menu_is_general'           => 'false',
            'position_ord_general'      => '0',
            'template_id_general'       => '1'
        ]);

        DB::table('olmo_page')->insert([
            'enabled_is_general'        => 'true',
            'locale_hidden_general'     => 'en',
            'name_txt_general'          => 'checkoutdetails',
            'slug_txt_general'          => 'shipping',
            'menu_is_general'           => 'false',
            'position_ord_general'      => '0',
            'template_id_general'       => '1'
        ]);
        
        DB::table('olmo_page')->insert([
            'enabled_is_general'        => 'true',
            'locale_hidden_general'     => 'en',
            'name_txt_general'          => 'checkoutpayment',
            'slug_txt_general'          => 'payment',
            'menu_is_general'           => 'false',
            'position_ord_general'      => '0',
            'template_id_general'       => '1'
        ]); 

        DB::table('olmo_page')->insert([
            'enabled_is_general'        => 'true',
            'locale_hidden_general'     => 'en',
            'name_txt_general'          => 'checkoutsummary',
            'slug_txt_general'          => 'summary',
            'menu_is_general'           => 'false',
            'position_ord_general'      => '0',
            'template_id_general'       => '1'
        ]);
        
        DB::table('olmo_page')->insert([
            'enabled_is_general'        => 'true',
            'locale_hidden_general'     => 'en',
            'name_txt_general'          => 'checkoutcompleted',
            'slug_txt_general'          => 'order-completed',
            'menu_is_general'           => 'false',
            'position_ord_general'      => '0',
            'template_id_general'       => '1'
        ]);         
    }
}