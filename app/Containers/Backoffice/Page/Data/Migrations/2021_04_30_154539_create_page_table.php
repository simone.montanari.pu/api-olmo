<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use App\Ship\Parents\Controllers\ApiMigration;

class CreatePageTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (Schema::hasTable('olmo_page')) {
            Schema::table('olmo_page', function (Blueprint $table) {
                // Update existing table...
            });
        } else {
            Schema::create('olmo_page', function (Blueprint $table) {
                // Create new table...
                $table->charset = 'utf8mb4';
                $table->collation = 'utf8mb4_unicode_ci';
                // General
                ApiMigration::Page($table);
                $table->text('page_id_general')->nullable();
                $table->text('category_multid_general')->nullable();
                $table->text('blog_multid_general')->nullable();
                // Custom
                /** 
                 * Prossima possibile implementazione è creare un metodo che se compilato tramite un 
                 * array fa il controllo se i valori inseriti sono corretti 
                 * */
                // ApiMigration::custom($table,[
                //     'title_text_content'
                // ])                
                $table->text('title_txt_content')->nullable();
                $table->text('subtitle_txt_content')->nullable();
                $table->text('abstract_txt_content')->nullable();
                $table->text('catalogue_media_content')->nullable();
                $table->text('cover_filemanager_content')->nullable();
                $table->text('cta_txt_content')->nullable();
                $table->text('content1_editor_content')->nullable();
                $table->text('image1_img_content')->nullable();                
                $table->text('content2_editor_content')->nullable();
                $table->text('image2_img_content')->nullable();
                $table->text('content3_editor_content')->nullable();
                $table->text('image3_img_content')->nullable();
                $table->text('content4_editor_content')->nullable();
                $table->text('image4_img_content')->nullable();
                $table->text('slider_slider_content')->nullable();                
                // Blocks
                $table->text('blocks_visual_blocks')->nullable();
                // SEO
                ApiMigration::Seo($table);
                // Sitemap
                ApiMigration::Sitemap($table); 

            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('olmo_page');
    }
}
