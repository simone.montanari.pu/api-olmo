<?php

namespace App\Containers\Backoffice\Page\Models;

use App\Ship\Parents\Models\Model;

class Page extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

  

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Page';

    public $table = 'olmo_page';
    
    public static function getAttr(){
        return [
            'table' => 'olmo_page',
            'lang'  => true,
            'required' =>[
                'name_txt_general',
                'slug_txt_general',
                'template_id_general'
            ],
            'rules' => [
                [
                    'type' => 'unique',
                    'field' => 'slug_txt_general',
                    'table' => 'olmo_product',
                    'errortxt' => 'Slug duplicate in page'
                ]               
            ]  
        ];
    }

}
