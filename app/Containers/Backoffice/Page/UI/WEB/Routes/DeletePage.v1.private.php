<?php

use App\Containers\Backoffice\Page\UI\WEB\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::delete('pages/{id}', [Controller::class, 'destroy'])
    ->name('web_page_destroy')
    ->middleware(['auth:web']);

