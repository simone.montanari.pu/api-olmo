<?php

use App\Containers\Backoffice\Page\UI\WEB\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::post('pages/store', [Controller::class, 'store'])
    ->name('web_page_store')
    ->middleware(['auth:web']);

