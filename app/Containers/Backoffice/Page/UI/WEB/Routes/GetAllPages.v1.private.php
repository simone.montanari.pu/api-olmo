<?php

use App\Containers\Backoffice\Page\UI\WEB\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('pages', [Controller::class, 'index'])
    ->name('web_page_index')
    ->middleware(['auth:web']);

