<?php

use App\Containers\Backoffice\Page\UI\WEB\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('pages/{id}', [Controller::class, 'show'])
    ->name('web_page_show')
    ->middleware(['auth:web']);

