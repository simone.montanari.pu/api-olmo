<?php

use App\Containers\Backoffice\Page\UI\WEB\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('pages/{id}/edit', [Controller::class, 'edit'])
    ->name('web_page_edit')
    ->middleware(['auth:web']);

