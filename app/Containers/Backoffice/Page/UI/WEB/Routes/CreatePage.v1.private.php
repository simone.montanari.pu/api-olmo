<?php

use App\Containers\Backoffice\Page\UI\WEB\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('pages/create', [Controller::class, 'create'])
    ->name('web_page_create')
    ->middleware(['auth:web']);

