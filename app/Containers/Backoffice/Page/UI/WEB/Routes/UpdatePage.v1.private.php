<?php

use App\Containers\Backoffice\Page\UI\WEB\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::patch('pages/{id}', [Controller::class, 'update'])
    ->name('web_page_update')
    ->middleware(['auth:web']);

