<?php

namespace App\Containers\Backoffice\Page\UI\WEB\Controllers;

use App\Containers\Backoffice\Page\UI\WEB\Requests\CreatePageRequest;
use App\Containers\Backoffice\Page\UI\WEB\Requests\DeletePageRequest;
use App\Containers\Backoffice\Page\UI\WEB\Requests\GetAllPagesRequest;
use App\Containers\Backoffice\Page\UI\WEB\Requests\FindPageByIdRequest;
use App\Containers\Backoffice\Page\UI\WEB\Requests\UpdatePageRequest;
use App\Containers\Backoffice\Page\UI\WEB\Requests\StorePageRequest;
use App\Containers\Backoffice\Page\UI\WEB\Requests\EditPageRequest;
use App\Containers\Backoffice\Page\Actions\CreatePageAction;
use App\Containers\Backoffice\Page\Actions\FindPageByIdAction;
use App\Containers\Backoffice\Page\Actions\GetAllPagesAction;
use App\Containers\Backoffice\Page\Actions\UpdatePageAction;
use App\Containers\Backoffice\Page\Actions\DeletePageAction;
use App\Ship\Parents\Controllers\WebController;

class Controller extends WebController
{
    public function index(GetAllPagesRequest $request)
    {
        $pages = app(GetAllPagesAction::class)->run($request);
        // ..
    }

    public function show(FindPageByIdRequest $request)
    {
        $page = app(FindPageByIdAction::class)->run($request);
        // ..
    }

    public function create(CreatePageRequest $request)
    {
        // ..
    }

    public function store(StorePageRequest $request)
    {
        $page = app(CreatePageAction::class)->run($request);
        // ..
    }

    public function edit(EditPageRequest $request)
    {
        $page = app(FindPageByIdAction::class)->run($request);
        // ..
    }

    public function update(UpdatePageRequest $request)
    {
        $page = app(UpdatePageAction::class)->run($request);
        // ..
    }

    public function destroy(DeletePageRequest $request)
    {
         $result = app(DeletePageAction::class)->run($request);
         // ..
    }
}
