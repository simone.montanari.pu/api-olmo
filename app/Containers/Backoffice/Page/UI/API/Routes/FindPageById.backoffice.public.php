<?php

/**
 * @apiGroup           Page
 * @apiName            findPageById
 *
 * @api                {GET} /v1/pages/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\Page\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('{lang}/page/{id}', [Controller::class, 'findById']);