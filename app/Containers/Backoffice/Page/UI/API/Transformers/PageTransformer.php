<?php

namespace App\Containers\Backoffice\Page\UI\API\Transformers;

use App\Containers\Backoffice\Page\Models\Page;
use App\Ship\Parents\Transformers\Transformer;

class PageTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Page $page): array
    {
        $response = [
            'object' => $page->getResourceKey(),
            'id' => $page->getHashedKey(),
            'created_at' => $page->created_at,
            'updated_at' => $page->updated_at,
            'readable_created_at' => $page->created_at->diffForHumans(),
            'readable_updated_at' => $page->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $page->id,
            // 'deleted_at' => $page->deleted_at,
        ], $response);

        return $response;
    }
}
