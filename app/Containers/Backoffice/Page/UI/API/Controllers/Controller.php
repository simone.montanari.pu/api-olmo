<?php

namespace App\Containers\Backoffice\Page\UI\API\Controllers;

use App\Containers\Backoffice\Page\UI\API\Requests\CreatePageRequest;
use App\Containers\Backoffice\Page\UI\API\Requests\DeletePageRequest;
use App\Containers\Backoffice\Page\UI\API\Requests\GetAllPagesRequest;
use App\Containers\Backoffice\Page\UI\API\Requests\FindPageByIdRequest;
use App\Containers\Backoffice\Page\UI\API\Requests\UpdatePageRequest;
use App\Containers\Backoffice\Page\UI\API\Transformers\PageTransformer;
use App\Containers\Backoffice\Page\Actions\CreatePageAction;
use App\Containers\Backoffice\Page\Actions\FindPageByIdAction;
use App\Containers\Backoffice\Page\Actions\GetAllPagesAction;
use App\Containers\Backoffice\Page\Actions\UpdatePageAction;
use App\Containers\Backoffice\Page\Actions\DeletePageAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use App\Containers\Backoffice\Page\Models\Page;
use App\Ship\Parents\Controllers\ApiHelper;
use Illuminate\Support\Facades\DB;

class Controller extends ApiController
{
    public function getListV1(GetAllPagesRequest $request)
    {
        $lang = $request->lang;
    	$datas   = Page::select('name_txt_general','slug_txt_general',"lang_langs_general",'template_id_general','enabled_is_general','id')
        ->where('locale_hidden_general',$lang)
        ->get()->toArray();


        $attribute  = Page::getAttr();
        return  $this->getList($datas,$attribute);

    }


    public function createUpdateV1(GetAllPagesRequest $request)
    {
    	$attribute  = Page::getAttr();
        $rules       = $attribute['rules'];
        $required    = $attribute['required'];
        if($required){
            $res = ApiHelper::CheckRequired($request,$required);
            if($res['valid'] == false){
                return response($res['err'], 400);
                exit();
            }
        }
        if($rules){
            $res = ApiHelper::CheckRules($request,$rules);
            if($res['valid'] == false){
                return response($res['err'], 400);
                exit();
            }
        }
    	return $this->createUpdate($request,$attribute);
    }

    public function findById(GetAllPagesRequest $request)
    { 
        $lang       = $request->lang;
        $attribute  = Page::getAttr();
        $id         = $request->id;
        if($id == 0){
            return $this->emptyItem($attribute,$lang);
        }else{
           $datas      = Page::where('id',$id)->get()->toArray();
           $attribute  = Page::getAttr();
           return  $this->getSingleList($datas,$attribute,$id,$lang);
        }
    }

}
