<?php

namespace App\Containers\Backoffice\Page\Actions;

use App\Containers\Backoffice\Page\Models\Page;
use App\Containers\Backoffice\Page\Tasks\CreatePageTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreatePageAction extends Action
{
    public function run(Request $request): Page
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreatePageTask::class)->run($data);
    }
}
