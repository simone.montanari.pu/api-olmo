<?php

namespace App\Containers\Backoffice\Page\Actions;

use App\Containers\Backoffice\Page\Models\Page;
use App\Containers\Backoffice\Page\Tasks\UpdatePageTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdatePageAction extends Action
{
    public function run(Request $request): Page
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdatePageTask::class)->run($request->id, $data);
    }
}
