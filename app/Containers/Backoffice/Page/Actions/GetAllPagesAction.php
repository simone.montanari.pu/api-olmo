<?php

namespace App\Containers\Backoffice\Page\Actions;

use App\Containers\Backoffice\Page\Tasks\GetAllPagesTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllPagesAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllPagesTask::class)->addRequestCriteria()->run();
    }
}
