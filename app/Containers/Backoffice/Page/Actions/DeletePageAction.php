<?php

namespace App\Containers\Backoffice\Page\Actions;

use App\Containers\Backoffice\Page\Tasks\DeletePageTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeletePageAction extends Action
{
    public function run(Request $request)
    {
        return app(DeletePageTask::class)->run($request->id);
    }
}
