<?php

namespace App\Containers\Backoffice\Page\Actions;

use App\Containers\Backoffice\Page\Models\Page;
use App\Containers\Backoffice\Page\Tasks\FindPageByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindPageByIdAction extends Action
{
    public function run(Request $request): Page
    {
        return app(FindPageByIdTask::class)->run($request->id);
    }
}
