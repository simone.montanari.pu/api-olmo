<?php

namespace App\Containers\Backoffice\Page\Tasks;

use App\Containers\Backoffice\Page\Data\Repositories\PageRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllPagesTask extends Task
{
    protected PageRepository $repository;

    public function __construct(PageRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
