<?php

namespace App\Containers\Backoffice\General\UI\API\Controllers;

use App\Containers\Backoffice\General\UI\API\Requests\CreateGeneralRequest;
use App\Containers\Backoffice\General\UI\API\Requests\DeleteGeneralRequest;
use App\Containers\Backoffice\General\UI\API\Requests\GetAllGeneralsRequest;
use App\Containers\Backoffice\General\UI\API\Requests\FindGeneralByIdRequest;
use App\Containers\Backoffice\General\UI\API\Requests\UpdateGeneralRequest;
use App\Containers\Backoffice\General\UI\API\Transformers\GeneralTransformer;
use App\Containers\Backoffice\General\Actions\CreateGeneralAction;
use App\Containers\Backoffice\General\Actions\FindGeneralByIdAction;
use App\Containers\Backoffice\General\Actions\GetAllGeneralsAction;
use App\Containers\Backoffice\General\Actions\UpdateGeneralAction;
use App\Containers\Backoffice\General\Actions\DeleteGeneralAction;
use App\Ship\Parents\Controllers\ApiController;
use App\Ship\Parents\Controllers\ApiPermissions;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class Controller extends ApiController
{

    public function cacheClear(CreateGeneralRequest $request)
    {
     
      return file_get_contents(env('APP_URL').'/_/hooks/cache/clear-data/?forceclear');

    }

    public function cacheImageClear(CreateGeneralRequest $request)
    {
     
      return file_get_contents(env('APP_URL').'/_/hooks/cache/clear-img/?forceclear');

    }


    public function duplicateModel(CreateGeneralRequest $request)
    {
     
       $id         = $request->id;
       $model      = $request->model;
       $destlang   = $request->destlang;
       $originlang = $request->originlang;

       $e     = ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model))::where('id',$id)
       ->where('locale_hidden_general',$originlang)
       ->first();

       return $e;
    }
    
    public function getMenu(CreateGeneralRequest $request)
    {
      return ApiPermissions::getMenu($request);
    }


}
