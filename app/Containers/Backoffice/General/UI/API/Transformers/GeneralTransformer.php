<?php

namespace App\Containers\Backoffice\General\UI\API\Transformers;

use App\Containers\Backoffice\General\Models\General;
use App\Ship\Parents\Transformers\Transformer;

class GeneralTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(General $general): array
    {
        $response = [
            'object' => $general->getResourceKey(),
            'id' => $general->getHashedKey(),
            'created_at' => $general->created_at,
            'updated_at' => $general->updated_at,
            'readable_created_at' => $general->created_at->diffForHumans(),
            'readable_updated_at' => $general->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $general->id,
            // 'deleted_at' => $general->deleted_at,
        ], $response);

        return $response;
    }
}
