<?php

/**
 * @apiGroup           General
 * @apiName            updateGeneral
 *
 * @api                {PATCH} /v1/generals/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Backoffice\General\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

/**
 * Clear cache from backoffice, currently just data has been connected with the backoffice
 */
Route::get('cache/frontend/data/clear', [Controller::class, 'cacheClear'])->middleware(['auth:api']);
Route::get('cache/frontend/data/image/clear', [Controller::class, 'cacheImageClear'])->middleware(['auth:api']);

/**
 * The backoffice menu
 */

Route::get('menu', [Controller::class, 'getMenu'])->middleware(['auth:api']);
 

