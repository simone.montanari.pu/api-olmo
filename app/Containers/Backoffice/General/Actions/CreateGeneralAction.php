<?php

namespace App\Containers\Backoffice\General\Actions;

use App\Containers\Backoffice\General\Models\General;
use App\Containers\Backoffice\General\Tasks\CreateGeneralTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateGeneralAction extends Action
{
    public function run(Request $request): General
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateGeneralTask::class)->run($data);
    }
}
