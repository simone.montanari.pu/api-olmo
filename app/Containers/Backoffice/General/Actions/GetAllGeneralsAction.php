<?php

namespace App\Containers\Backoffice\General\Actions;

use App\Containers\Backoffice\General\Tasks\GetAllGeneralsTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllGeneralsAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllGeneralsTask::class)->addRequestCriteria()->run();
    }
}
