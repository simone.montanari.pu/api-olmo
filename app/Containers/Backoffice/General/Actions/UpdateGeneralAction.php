<?php

namespace App\Containers\Backoffice\General\Actions;

use App\Containers\Backoffice\General\Models\General;
use App\Containers\Backoffice\General\Tasks\UpdateGeneralTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateGeneralAction extends Action
{
    public function run(Request $request): General
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateGeneralTask::class)->run($request->id, $data);
    }
}
