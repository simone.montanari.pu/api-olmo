<?php

namespace App\Containers\Backoffice\General\Actions;

use App\Containers\Backoffice\General\Tasks\DeleteGeneralTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteGeneralAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteGeneralTask::class)->run($request->id);
    }
}
