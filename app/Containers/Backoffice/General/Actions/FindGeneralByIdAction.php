<?php

namespace App\Containers\Backoffice\General\Actions;

use App\Containers\Backoffice\General\Models\General;
use App\Containers\Backoffice\General\Tasks\FindGeneralByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindGeneralByIdAction extends Action
{
    public function run(Request $request): General
    {
        return app(FindGeneralByIdTask::class)->run($request->id);
    }
}
