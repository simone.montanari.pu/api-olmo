<?php

namespace App\Containers\Backoffice\General\Tasks;

use App\Containers\Backoffice\General\Data\Repositories\GeneralRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateGeneralTask extends Task
{
    protected GeneralRepository $repository;

    public function __construct(GeneralRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
