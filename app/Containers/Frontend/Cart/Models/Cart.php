<?php

namespace App\Containers\Frontend\Cart\Models;

use App\Ship\Parents\Models\Model;

class Cart extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];


    public $table = 'olmo_cartitem';

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Cart';
}
