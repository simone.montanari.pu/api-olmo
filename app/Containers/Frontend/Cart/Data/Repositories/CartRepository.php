<?php

namespace App\Containers\Frontend\Cart\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class CartRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
