<?php

namespace App\Containers\Frontend\Cart\Tasks;

use App\Containers\Frontend\Cart\Data\Repositories\CartRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllCartsTask extends Task
{
    protected CartRepository $repository;

    public function __construct(CartRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
