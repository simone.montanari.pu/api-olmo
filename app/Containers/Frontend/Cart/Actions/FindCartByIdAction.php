<?php

namespace App\Containers\Frontend\Cart\Actions;

use App\Containers\Frontend\Cart\Models\Cart;
use App\Containers\Frontend\Cart\Tasks\FindCartByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindCartByIdAction extends Action
{
    public function run(Request $request): Cart
    {
        return app(FindCartByIdTask::class)->run($request->id);
    }
}
