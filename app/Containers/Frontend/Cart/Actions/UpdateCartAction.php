<?php

namespace App\Containers\Frontend\Cart\Actions;

use App\Containers\Frontend\Cart\Models\Cart;
use App\Containers\Frontend\Cart\Tasks\UpdateCartTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateCartAction extends Action
{
    public function run(Request $request): Cart
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateCartTask::class)->run($request->id, $data);
    }
}
