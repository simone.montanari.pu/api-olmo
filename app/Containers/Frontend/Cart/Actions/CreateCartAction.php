<?php

namespace App\Containers\Frontend\Cart\Actions;

use App\Containers\Frontend\Cart\Models\Cart;
use App\Containers\Frontend\Cart\Tasks\CreateCartTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateCartAction extends Action
{
    public function run(Request $request): Cart
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateCartTask::class)->run($data);
    }
}
