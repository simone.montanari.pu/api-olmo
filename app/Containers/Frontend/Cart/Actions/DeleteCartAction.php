<?php

namespace App\Containers\Frontend\Cart\Actions;

use App\Containers\Frontend\Cart\Tasks\DeleteCartTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteCartAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteCartTask::class)->run($request->id);
    }
}
