<?php

namespace App\Containers\Frontend\Cart\UI\API\Controllers;

use App\Containers\Frontend\Cart\UI\API\Requests\CreateCartRequest;
use App\Containers\Frontend\Cart\UI\API\Requests\DeleteCartRequest;
use App\Containers\Frontend\Cart\UI\API\Requests\GetAllCartsRequest;
use App\Containers\Frontend\Cart\UI\API\Requests\FindCartByIdRequest;
use App\Containers\Frontend\Cart\UI\API\Requests\UpdateCartRequest;
use App\Containers\Frontend\Cart\UI\API\Transformers\CartTransformer;
use App\Containers\Frontend\Cart\Actions\CreateCartAction;
use App\Containers\Frontend\Cart\Actions\FindCartByIdAction;
use App\Containers\Frontend\Cart\Actions\GetAllCartsAction;
use App\Containers\Frontend\Cart\Actions\UpdateCartAction;
use App\Containers\Frontend\Cart\Actions\DeleteCartAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use App\Ship\Parents\Controllers\ApiHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Ship\Parents\Controllers\Frontend\ApiCart;

class Controller extends ApiController
{
    public function addCart(CreateCartRequest $request)
    {
        $customer = ApiHelper::getCustomer();
        if(!($customer))
        {
           return response('Customer not found', 404);
           exit();
        }
         return ApiCart::Add($customer);

    }


}
