<?php

/**
 * @apiGroup           Cart
 * @apiName            findCartById
 *
 * @api                {GET} /v1/carts/:id Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Frontend\Cart\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('carts/{id}', [Controller::class, 'findCartById'])
    ->name('api_cart_find_cart_by_id')
    ->middleware(['auth:api']);

