<?php

/**
 * @apiGroup           Cart
 * @apiName            createCart
 *
 * @api                {POST} /v1/carts Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Frontend\Cart\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::post('carts', [Controller::class, 'createCart'])
    ->name('api_cart_create_cart')
    ->middleware(['auth:api']);

