<?php

namespace App\Containers\Frontend\Auth\Models;

use App\Ship\Parents\Models\Model;

class Auth extends Model
{
    protected $fillable = [

    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used in the serialized responses.
     */
    protected string $resourceKey = 'Auth';


    public $table = 'olmo_customer';

    public static function getAttr(){
        return [
            'table' => 'olmo_customer',
            'required' =>[
                'email_email_general'
            ],
            'rules' => [
                [
                    'equal','password_pwd_general','confirmpassword','Password not match',
                    'unique','email_email_general','olmo_customer','Email duplicate'
                
                ]
            ]
        
        ];
    }








}
