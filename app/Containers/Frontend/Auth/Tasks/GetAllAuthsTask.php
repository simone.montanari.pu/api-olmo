<?php

namespace App\Containers\Frontend\Auth\Tasks;

use App\Containers\Frontend\Auth\Data\Repositories\AuthRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllAuthsTask extends Task
{
    protected AuthRepository $repository;

    public function __construct(AuthRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
