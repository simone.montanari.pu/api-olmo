<?php

namespace App\Containers\Frontend\Auth\UI\API\Transformers;

use App\Containers\Frontend\Auth\Models\Auth;
use App\Ship\Parents\Transformers\Transformer;

class AuthTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Auth $auth): array
    {
        $response = [
            'object' => $auth->getResourceKey(),
            'id' => $auth->getHashedKey(),
            'created_at' => $auth->created_at,
            'updated_at' => $auth->updated_at,
            'readable_created_at' => $auth->created_at->diffForHumans(),
            'readable_updated_at' => $auth->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $auth->id,
            // 'deleted_at' => $auth->deleted_at,
        ], $response);

        return $response;
    }
}
