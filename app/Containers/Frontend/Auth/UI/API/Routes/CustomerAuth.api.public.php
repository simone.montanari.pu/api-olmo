<?php

/**
 * @apiGroup           Auth
 * @apiName            createAuth
 *
 * @api                {POST} /v1/auths Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Frontend\Auth\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::post('auth/login', [Controller::class, 'loginAuth']);

Route::post('auth/register', [Controller::class, 'registerAuth']);

Route::post('auth/logout', [Controller::class, 'logoutAuth']);


Route::post('auth/profile', [Controller::class, 'profileCustomer']);

Route::post('auth/activate', [Controller::class, 'profileActivate']);


/** Reset passowrd */

Route::post('auth/password-change', [Controller::class, 'profileChangePwd']);

Route::post('auth/password-recovery', [Controller::class, 'profilePasswordRecovery']);

Route::post('auth/password-reset', [Controller::class, 'profilePasswordReset']);


/** Check Session */

Route::post('auth/checksession', [Controller::class, 'checkSession']);