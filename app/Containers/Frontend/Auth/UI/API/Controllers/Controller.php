<?php

namespace App\Containers\Frontend\Auth\UI\API\Controllers;

use App\Containers\Frontend\Auth\UI\API\Requests\CreateAuthRequest;
use App\Containers\Frontend\Auth\UI\API\Requests\DeleteAuthRequest;
use App\Containers\Frontend\Auth\UI\API\Requests\GetAllAuthsRequest;
use App\Containers\Frontend\Auth\UI\API\Requests\FindAuthByIdRequest;
use App\Containers\Frontend\Auth\UI\API\Requests\UpdateAuthRequest;
use App\Containers\Frontend\Auth\UI\API\Transformers\AuthTransformer;
use App\Containers\Frontend\Auth\Actions\CreateAuthAction;
use App\Containers\Frontend\Auth\Actions\FindAuthByIdAction;
use App\Containers\Frontend\Auth\Actions\GetAllAuthsAction;
use App\Containers\Frontend\Auth\Actions\UpdateAuthAction;
use App\Containers\Frontend\Auth\Actions\DeleteAuthAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Containers\Backoffice\Customer\Models\Customer;
use App\Ship\Parents\Controllers\ApiHelper;
use App\Ship\Parents\Controllers\Frontend\ApiCart;
use App\Containers\Backoffice\Fillform\Models\Fillform;
use App\Ship\Parents\Controllers\Frontend\ApiCustomer;

class Controller extends ApiController
{
    public function registerAuth(CreateAuthRequest $request)
    {

        $response           = [];
        $err                = [];
        $model              = [];
        $valid              = true;
        $code               = 200;
        $user_id            = 0;
        $request->id        = '0';
        $token_active       = '';
        $token              = '';
        $send_mail_active   = false;
        $formIdRegister     = 0;
        $status             = 'notactive';
        $emailtemplate      = 'creation';
        $attribute          = Customer::getAttr();
        $rules              = $attribute['rules'];
        $required           = $attribute['required'];
        $level_registration = env('CUSTOMER_REGISTRATION_LEVEL');
        $statusRes          = [];
        $locale             = $request->current_locale;
        $customeremail      = $level_registration == 0 || $level_registration == 2 ? true : false;
        
        if($rules){
            $res = ApiHelper::CheckRules($request,$rules);
            if($res['valid'] == false){
                $err[] = $res['err'];
                $valid = false;
                $code    = 400;
            }
        }
        if($valid){
            $token_active     = md5(time().rand(1000,9999));
            $token            = md5(time().rand(1000,9999).'olmo');
            if($level_registration == 0){
                $status                              = 'active';
                $request->token_read_general	     = $token;
            }

            //Creo il customer (utente)
            $buildRequest          = ApiHelper::rebuildRequestByTable($attribute['table'], $request);
    	    $writeUser             = $this->addUser($buildRequest,$attribute);

            if($writeUser){
                $filterNull       = ApiHelper::filterNotNull($writeUser);
                //Triggero il change status
                $statusRes        = ApiCustomer::ChangeStatus($writeUser,$status,$emailtemplate,$locale,$customeremail);
            } else { 
                $code = 409;
                $err = ['User already exist'];
            }

        }

        $response['res']    = $valid;
        $response['user']   = [
            'user_data' => $writeUser,
            'token' => $token,
            'status' => $statusRes
        ];
        $response['err'] = $err;
        $response['fillform'] = $statusRes;

        return response($response, $code);

    }



    public function loginAuth(CreateAuthRequest $request)
    {
        
        $username    = $request->email;
        $password    = $request->password;
        $user        = @Customer::where('email_email_general',$username)->where('password_pwd_general',$password)->first();
        $userlog     = [];   
        $statusCode  = 200;

        if($user && $user->customerstatus_select_general !== 'active'){

            $userlog['user'] = null;
            $statusCode = 401;
            $userlog['error'] = [
                "err_code" => 2,
                "err_text" => "User not active"
            ];

        } else if(!$user) {

            $userlog['user'] = null;
            $statusCode = 404;
            $userlog['error'] = [
                "err_code" => 1,
                "err_text" => "User not exist"
            ];             

        } else {

            $token = md5(time().'OLMO');
            DB::table('olmo_customer')->where('id',$user->id)->update(['token_read_general' => $token ]);
            $user['token']    = $token;
            $user['cart']     = []; 
            $user['discount'] = ApiCart::getDiscountGlobal($user);
            $user             = ApiHelper::FrontResponseStrict($user);
            $userlog['user']  = $user;
            $userlog['error'] = [];

        }

        return response($userlog, $statusCode);

    }

    public function logoutAuth(CreateAuthRequest $request)
    {
        return ApiHelper::logoutCustomer();
    }

    public function profileCustomer(CreateAuthRequest $request)
    {

        $attribute    = Customer::getAttr();
        $rules        = $attribute['rules'];
        $required     = $attribute['required'];
        $user         = ApiHelper::getCustomer();      
        
        $buildRequest = strpos($request->input('email'), '_') ? $request : ApiHelper::rebuildRequestByTable('olmo_customer', $request);
        $buildRequest->id = $user->id;

        $response = [];
        $code = 200;
        $response['user'] = [];
        $response['buildRequest'] = $buildRequest->all();
        $response['request'] = $request->all();

        if($user){

            $updatedUser = $this->update($buildRequest, $attribute);
            $response['user'] = $updatedUser;

        } else {

            $response['err'] = ['User not found'];
            $code = 400;

        }

        return response($response, $code);

    }

    public function profileChangePwd(CreateAuthRequest $request)
    {

        $user        = ApiHelper::getCustomer();
        $code        = 200;
        $password    = $request->input('password');
        $newpassword = $request->input('newpassword');

        $attribute   = Customer::getAttr();

        $data = Db::table($attribute['table'])->where('id',$user->id)->where('password_pwd_general',$password)->first();
        if($data){
            $request->replace([
                'password_pwd_general' => $newpassword,
                'confirmpassword_pwd_general' => $newpassword,
                'id' => $user->id
            ]);
    
            $updatedUser = $this->update($request, $attribute);
            $response['user'] = $updatedUser;
        } else {
            $code = 404;
            $response['user'] = ["actual password doesn't match"];
        }

        return response($response, $code);

    }


    public function profileActivate(CreateAuthRequest $request)
    {

        $user        = ApiHelper::getCustomer();
        $sc          = 200;
        $res         = [];
        if(!$user)
            $sc = 400;
        else{
            $res = ApiCustomer::ChangeStatus($user->id,'active','customeractive'); 
        }

        return response($res,$sc);

    }

    public function checkSession(CreateAuthRequest $request)
    {
        $user        = ApiHelper::getCustomer();
        $sc          = 200;
        if(!$user){
            $txt = ['Session user not found'];
            $sc = 404;
        }
        else{
            $txt = ['Session user active'];
            $sc = 200;
            if($user->customerstatus_select_general != 'active'){
                $txt = ['Session user disabled'];
                $sc = 400;
            }
        }

        return response($txt,$sc);
    }



    public function profilePasswordReset(CreateAuthRequest $request)
    {
        $sc          = 200;
        $res         = [];
        $token       = $request->token;
        $password    = $request->password;
        $customer    = @Db::table('olmo_customer')->where('tokenactive_read_general',$token)->first();
        $customer_id = @$customer->id;
        if($customer_id == ''){

            $sc = 400;
            $res['status'] = 'ko';

        }else{

            @Db::table('olmo_customer')->where('id',$customer_id)->update([
                'tokenactive_read_general' => '',
                'password_pwd_general'     => $password
            ]);

            //invio mail
            $params  = [];
            $locale  = @$customer->customerlang_txt_general;
            if($locale == null)
                $locale = 'en';

            $params['locale'] = @$locale;
            $params['a']      = @$customer->email_email_general;
            $formid           = @DB::table('olmo_fillform')->where('formtype_select_general','customerpasswordchanged')->first()->code_txt_general;
            $res['mail']      = @FillForm::sendMailFillform($formid,$params);

            $res['status'] = 'ok';
        }

        return response($res,$sc);

    }


    public function profilePasswordRecovery(CreateAuthRequest $request)
    {

        $sc          = 200;
        $res         = [];
        $email       = $request->email;
        $reset_url   = $request->reset_url;
        $customer    = @Db::table('olmo_customer')->where('email_email_general',$email)->first();
        $customer_id = @$customer->id;
        if($customer_id == ''){

            $sc = 400;
            $res['status'] = 'ko';

        }else{

            $params = [];

            $token_active  = md5(time().'olmoactive'.rand(100,999));

            $locale = $customer->customerlang_txt_general;
            if($locale == null)
                $locale = 'en';

            $params['reset_url']      = $reset_url.$token_active;
            $params['locale'] = $locale;
            $params['a']              = $email;
            $formid      = @DB::table('olmo_fillform')->where('formtype_select_general','customerpasswordrecovery')->first()->code_txt_general;
            $res['mail'] = FillForm::sendMailFillform($formid,$params);
           
            @Db::table('olmo_customer')->where('id',$customer_id)->update([
                'tokenactive_read_general' => $token_active
            ]);
            $res['status'] = 'ok';

        }

        return response($res,$sc);

    }

}
