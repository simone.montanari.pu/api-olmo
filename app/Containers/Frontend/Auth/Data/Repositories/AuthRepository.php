<?php

namespace App\Containers\Frontend\Auth\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class AuthRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
