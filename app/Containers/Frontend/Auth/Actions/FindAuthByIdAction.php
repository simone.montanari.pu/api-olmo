<?php

namespace App\Containers\Frontend\Auth\Actions;

use App\Containers\Frontend\Auth\Models\Auth;
use App\Containers\Frontend\Auth\Tasks\FindAuthByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindAuthByIdAction extends Action
{
    public function run(Request $request): Auth
    {
        return app(FindAuthByIdTask::class)->run($request->id);
    }
}
