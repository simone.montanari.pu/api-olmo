<?php

namespace App\Containers\Frontend\Auth\Actions;

use App\Containers\Frontend\Auth\Tasks\DeleteAuthTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteAuthAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteAuthTask::class)->run($request->id);
    }
}
