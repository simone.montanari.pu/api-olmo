<?php

namespace App\Containers\Frontend\Auth\Actions;

use App\Containers\Frontend\Auth\Tasks\GetAllAuthsTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllAuthsAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllAuthsTask::class)->addRequestCriteria()->run();
    }
}
