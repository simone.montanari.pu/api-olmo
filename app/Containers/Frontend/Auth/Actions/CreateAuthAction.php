<?php

namespace App\Containers\Frontend\Auth\Actions;

use App\Containers\Frontend\Auth\Models\Auth;
use App\Containers\Frontend\Auth\Tasks\CreateAuthTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateAuthAction extends Action
{
    public function run(Request $request): Auth
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateAuthTask::class)->run($data);
    }
}
