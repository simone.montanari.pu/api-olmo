<?php

namespace App\Containers\Frontend\Base\Tasks;

use App\Containers\Frontend\Base\Data\Repositories\BaseRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllBasesTask extends Task
{
    protected BaseRepository $repository;

    public function __construct(BaseRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
