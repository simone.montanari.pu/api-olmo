<?php

/**
 * @apiGroup           Base
 * @apiName            getAllBases
 *
 * @api                {GET} /v1/bases Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Frontend\Base\UI\API\Controllers\Controller;
use App\Containers\Frontend\Base\UI\API\Controllers\ControllerForms;
use App\Containers\Frontend\Base\UI\API\Controllers\ControllerCart;
use App\Containers\Frontend\Base\UI\API\Controllers\ControllerOrder;
use App\Containers\Frontend\Base\UI\API\Controllers\ControllerWishlist;
use App\Containers\Frontend\Base\UI\API\Controllers\ControllerStorelocator;
use App\Containers\Frontend\Base\UI\API\Controllers\ControllerMedia;
use Illuminate\Support\Facades\Route;

/** Aggregate API frontend */

Route::get('{lang}/allcategories', [Controller::class, 'getAllcategories']);
Route::get('{lang}/allmodel/{model}', [Controller::class, 'getAllModel']);
Route::get('structure', [Controller::class, 'getStructure']);
Route::get('sitemap/{lang}', [Controller::class, 'getSitemap']);
Route::get('robots.txt', [Controller::class, 'getRobots']);



/** Request assets **/
Route::post('request/assets', [ControllerMedia::class, 'requestAssets']);







/** Address */
Route::post('/address', [ControllerForms::class, 'saveAdress']);
Route::post('/address/setdefault', [ControllerForms::class, 'setDefaultAddress']);
Route::patch('/address', [ControllerForms::class, 'editAdress']);
Route::delete('/address', [ControllerForms::class, 'deleteAdress']);
Route::get('/address', [ControllerForms::class, 'getListAddress']);

/** Shopping Cart */
Route::post('/{lang}/shoppingcart', [ControllerCart::class, 'addCart']);
Route::delete('/{lang}/shoppingcart', [ControllerCart::class, 'deleteCart']);
Route::post('/{lang}/shoppingcart/quantity', [ControllerCart::class, 'addQty']);
Route::get('/{lang}/shoppingcart', [ControllerCart::class, 'getCart']);
Route::get('/shoppingcart', [ControllerCart::class, 'getCart']);
Route::post('/{lang}/shoppingcart/discount', [ControllerCart::class, 'setDiscount']);
Route::delete('/{lang}/shoppingcart/discount', [ControllerCart::class, 'delDiscount']);

Route::post('/{lang}/shoppingcart/addresses', [ControllerCart::class, 'setAddress']);
Route::post('/{lang}/shoppingcart/paymentmethod', [ControllerCart::class, 'setPayment']);
Route::post('/{lang}/shoppingcart/shippingmethod', [ControllerCart::class, 'setShipping']);

/** Order */
Route::post('/{lang}/order', [ControllerOrder::class, 'placeOrder']);
Route::get('/{lang}/order', [ControllerOrder::class, 'getListOrder']);
Route::get('/{lang}/order/{id}', [ControllerOrder::class, 'getSingleOrder']);
Route::patch('/{lang}/order/{id}', [ControllerOrder::class, 'chnageStatusOrder']);

/** Wishlist **/
Route::get('/{lang}/wishlist', [ControllerWishlist::class, 'getWishlist']);
Route::get('/wishlist', [ControllerWishlist::class, 'getWishlist']);
Route::post('/wishlist', [ControllerWishlist::class, 'addWishlist']);
Route::delete('/wishlist', [ControllerWishlist::class, 'deleteWishlist']);

/** Froms */
Route::get('auth/forms/{lang}/{type}', [ControllerForms::class, 'getForms']);
Route::get('forms/{lang}/address/{type}', [ControllerForms::class, 'getForms']);

/** Storelocator */
Route::get('{lang}/storelocator/list', [ControllerStorelocator::class, 'getStorlocator']);

/** Fitelrs */
Route::post('{lang}/filters/{model}', [Controller::class, 'filterPost']);

/** Base */
Route::get('{lang}/{slug1?}/{slug2?}/{slug3?}', [Controller::class, 'getPage']);