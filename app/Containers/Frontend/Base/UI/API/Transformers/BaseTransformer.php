<?php

namespace App\Containers\Frontend\Base\UI\API\Transformers;

use App\Containers\Frontend\Base\Models\Base;
use App\Ship\Parents\Transformers\Transformer;

class BaseTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Base $base): array
    {
        $response = [
            'object' => $base->getResourceKey(),
            'id' => $base->getHashedKey(),
            'created_at' => $base->created_at,
            'updated_at' => $base->updated_at,
            'readable_created_at' => $base->created_at->diffForHumans(),
            'readable_updated_at' => $base->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $base->id,
            // 'deleted_at' => $base->deleted_at,
        ], $response);

        return $response;
    }
}
