<?php

namespace App\Containers\Frontend\Base\UI\API\Controllers;

use App\Containers\Frontend\Base\UI\API\Requests\CreateBaseRequest;
use Illuminate\Support\Facades\DB;
use App\Ship\Parents\Controllers\ApiController;
use App\Ship\Parents\Controllers\ApiHelper;

class ControllerStorelocator extends ApiController
{

  public function getStorlocator(CreateBaseRequest $request){

    $request = Db::table('olmo_storelocator')->get()->toArray();
    $data = ApiHelper::cleanKeyArray($request);

    return response($data, 200);

  }
}