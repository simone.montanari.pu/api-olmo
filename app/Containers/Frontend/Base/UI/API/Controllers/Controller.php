<?php

namespace App\Containers\Frontend\Base\UI\API\Controllers;

use App\Containers\Frontend\Base\UI\API\Requests\CreateBaseRequest;
use App\Containers\Frontend\Base\UI\API\Requests\DeleteBaseRequest;
use App\Containers\Frontend\Base\UI\API\Requests\GetAllBasesRequest;
use App\Containers\Frontend\Base\UI\API\Requests\FindBaseByIdRequest;
use App\Containers\Frontend\Base\UI\API\Requests\UpdateBaseRequest;
use App\Containers\Frontend\Base\UI\API\Transformers\BaseTransformer;
use App\Containers\Frontend\Base\Actions\CreateBaseAction;
use App\Containers\Frontend\Base\Actions\FindBaseByIdAction;
use App\Containers\Frontend\Base\Actions\GetAllBasesAction;
use App\Containers\Frontend\Base\Actions\UpdateBaseAction;
use App\Containers\Frontend\Base\Actions\DeleteBaseAction;
use App\Ship\Parents\Controllers\ApiController;
use App\Ship\Parents\Controllers\ApiFilters;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Ship\Parents\Controllers\ApiHelper;
use App\Containers\Backoffice\Page\Models\Page;
use App\Containers\Backoffice\Template\Models\Template;
use App\Containers\Backoffice\Category\Models\Category;
use App\Containers\Backoffice\Blog\Models\Blog;
use App\Ship\Parents\Controllers\Frontend\ApiRoute;
use Illuminate\Support\Facades\Schema;


class Controller extends ApiController
{

  public function getRobots(CreateBaseRequest $request){

    $env = env('APP_ENV');

    if($env === 'production'){
      $query = Db::table('olmo_analytics')->where('id','1')->first();
      return response($query->robotsprod_txt_analytics)->withHeaders(['Content-Type' => 'text/plain']);
    } else {
      $query = Db::table('olmo_analytics')->where('id','1')->first();
      return response($query->robots_txt_analytics)->withHeaders(['Content-Type' => 'text/plain']);
    }
        
  }  

  public function getSitemap(CreateBaseRequest $request){

    $results     = [];
    $base_url    = env('APP_URL');
    $sitemap_xml = [];
    $templates   = Db::table('olmo_template')->where('enabled_is_general','true')->where('activate_is_sitemap','true')->where('locale_hidden_general',$request->lang)->get();
    $i           = 0;
    
    $model = [];

    foreach ($templates as $template) {
      array_push($model, $template->model_select_general);
    }

    $models = array_unique($model);

    foreach($models as $model){

      $pages = Db::table('olmo_'.$model)
              ->where('enabled_is_general','true')
              ->where('disable_is_sitemap','false')
              ->where('locale_hidden_general',$request->lang)->get();

      foreach($pages as $page){

        $templatepage = Db::table('olmo_template')->where('id', $page->template_id_general)->first();

        if($templatepage->activate_is_sitemap === 'true' && $page->disable_is_sitemap !== 'true'){            
          $slug_template        = $templatepage->slug_txt_general;
          $slug_page            = $page->slug_txt_general;
          $slug                 = str_replace(":slug",$slug_page,$slug_template);
          $folder               = $slug_page;

          /** Lastmode Conversion */
          $dateConversion = $page->lastmod_read_general ? $page->lastmod_read_general : $page->create_read_general;
          $date = new \DateTime($dateConversion);
          $lastmod = $date->format('Y-m-d');          

          $results[$i]['baseurl']    = $base_url;
          $results[$i]['lang']       = $page->locale_hidden_general;
          $results[$i]['lastmod']    = $lastmod;
          $results[$i]['slug']       = $slug;
          $results[$i]['folder']     = $folder;
          $results[$i]['type']       = $model;
          $results[$i]['changefreq'] = $page->overwrite_is_sitemap == 'false' ? $templatepage->frequency_select_sitemap : $page->frequency_select_sitemap;
          $results[$i]['priority']   = $page->overwrite_is_sitemap == 'false' ? $templatepage->priority_select_sitemap : $page->priority_select_sitemap;
          $i++;
        }

      }

    }      

    return $results;

  }

    public function getStructure(CreateBaseRequest $request)
    {
        $valid_lang   = ApiHelper::validLang()['valid_lang'];
        $default_lang = ApiHelper::validLang()['default_lang'];

        $res = array();
        
        //assets
        $res['assets']['media']         = env('API_URL')."/storage";
        $res['i18n']['locales']         = $valid_lang;
        $res['i18n']['default_locale']  = @$default_lang;
        $res['analytics']               = ApiHelper::getAnalytics();

        $i = 0;

        $items = Template::where('enabled_is_general','true')->groupBy('name_txt_general')->get();
        foreach($items as $item){
          $res['routes'][$i]['id'] = $item->name_txt_general;
          foreach ($valid_lang as $key => $value) {
            $res['routes'][$i]['slug'][$key] = ApiHelper::getSlug($key,$item->name_txt_general);
          }
          $i++;
        }
        return $res;
    }


    public function getPage(CreateBaseRequest $request)
    {
      $page = [];
      $entity = ApiRoute::determinateIDpageByRoute();
      if($entity['dataModel'] == ''){
        return response('Page not found', 404);
        exit();
      }
      $lang                = $request->lang;
      $data                = json_decode($entity['dataModel']);
      $page                = ApiHelper::FrontResponseStrict($data);
      $page['prevpost']    = ApiHelper::getNextPrevPost($entity['model'], $data->id, 'prev');
      $page['nextpost']    = ApiHelper::getNextPrevPost($entity['model'], $data->id, 'next');      
      $page['children']    = self::getLoopChildren($entity['model'], $data->id, $request->lang);
      $page['seo']         = ApiRoute::Seo($entity['dataModel']);
      $page['route']       = ApiRoute::Route($lang,$entity['dataModel'],$entity['model'],$request);
      return $page;
    }

    public function getLoopChildren($table, $id, $lang)
    {
      /**
       * Check if the column exist
       */
      $isColExist = Schema::connection("mysql")->hasColumn('olmo_'.$table,$table.'_id_general');
      if($isColExist){
        /** Extract the children */
        $response = json_decode(json_encode(ApiHelper::getChildren($table, $id)), true);
        /**
         * Loop through the children and assign other children till now just one step
         * need to understand how to make infinite  
         * */
        foreach($response as $key=>$value){
          $response[$key]['children'] = json_decode(json_encode(ApiHelper::getChildren($table, $response[$key]['id'])), true);
        } 
        return $response;
      }
      return null;
      
    }

    public function getAllcategories(CreateBaseRequest $request)
    {
      $categories =  ApiHelper::FrontResponseChild(Category::where('locale_hidden_general',$request->lang)->where('category_id_general','')->get());
      $response = [];
      foreach($categories as $key=>$children){
        $response[$key] = $children;
        $child = ApiHelper::getChildren('category', $categories[$key]['id']);
        foreach($child as $subKey=>$subChildren){
          $subChild = ApiHelper::getChildren('category', $child[$subKey]['id']);
          $child[$subKey]['children'] = $subChild;
        }
        $response[$key]['children'] = $child;
      }   
      return $response;
    }

    public function getAllModel(CreateBaseRequest $request)
    {
      $debug          = [];
      $page           = @$request->page;
      $salt           = @$request->offset;
      $q              = @$request->q;
      $sensibility    = @$request->sensibility;
      $model          = @$request->model;
      $select         = @$request->select;

      $page     = $page;
      $skip     = ($page * $salt) -$salt;
      $nextSkip = (($page + 1) * $salt) -$salt;

      $query   =  @('App\Containers\Backoffice\\'.ucfirst($model).'\Models\\'.ucfirst($model))::query();

      $query   = $query->where('locale_hidden_general',$request->lang)->where('enabled_is_general','true');
      if($model == 'category'){
        $query = $query->where('category_id_general','');
      }

      if($q != ''){
        if($sensibility == '')
        {
          return 'Sensibility : '.$sensibility.' missing param for search';
          exit();
        }
        $fields    = explode(',',$sensibility);
        $words = explode(' ',$q);
        $loop  = 0;
        foreach($fields as $field){
          foreach($words as $word){
            $debug[] = $word;
            if($loop == 0)
              $query   = $query->where($field, 'like', '%'.$word.'%');
            else
              $query   = $query->orWhere($field, 'like', '%'.$word.'%');
            
            $loop++;
          }
        }
      }

      $queryNext   = clone $query;
      $query       = $query->skip($skip)->take($salt);
      $queryNext   = $queryNext->skip($nextSkip)->take($salt);

      $query      = $query->orderBy('position_ord_general','asc');
  
      if($select != ''){
        $select = explode(',',$select);
        array_unshift($select,'id');
        $query  =   $query->select($select);
      }

      $blogs   = $query->get();

      $nextBlog   = $queryNext->get();

      $blogs  = ApiHelper::FrontResponseChild($blogs);
      $res      = [];
      $response = [];
      foreach($blogs as $key => $children){
        $response[$key] = $children;
        $child = ApiHelper::getChildren('category', $blogs[$key]['id']);
        foreach($child as $subKey=>$subChildren){
          $subChild = ApiHelper::getChildren('category', $child[$subKey]['id']);
          $child[$subKey]['children'] = $subChild;
        }
        $response[$key]['children'] = $child;
      }  
      
      $res['response'] = $response;

      $res['total']     = count($response);
      $res['nextPage']  = count($nextBlog);


      return $res;
    }


    public function filterPost(CreateBaseRequest $request)
    {
      return ApiFilters::filter($request);
    }


}
