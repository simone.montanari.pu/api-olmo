<?php

namespace App\Containers\Frontend\Base\UI\API\Controllers;

use App\Containers\Frontend\Base\UI\API\Requests\CreateBaseRequest;
use App\Containers\Frontend\Base\UI\API\Requests\DeleteBaseRequest;
use App\Containers\Frontend\Base\UI\API\Requests\GetAllBasesRequest;
use App\Containers\Frontend\Base\UI\API\Requests\FindBaseByIdRequest;
use App\Containers\Frontend\Base\UI\API\Requests\UpdateBaseRequest;
use App\Containers\Frontend\Base\UI\API\Transformers\BaseTransformer;
use App\Containers\Frontend\Base\Actions\CreateBaseAction;
use App\Containers\Frontend\Base\Actions\FindBaseByIdAction;
use App\Containers\Frontend\Base\Actions\GetAllBasesAction;
use App\Containers\Frontend\Base\Actions\UpdateBaseAction;
use App\Containers\Frontend\Base\Actions\DeleteBaseAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Ship\Parents\Controllers\ApiHelper;
use App\Containers\Backoffice\Page\Models\Page;
use App\Containers\Backoffice\Template\Models\Template;
use App\Containers\Backoffice\Category\Models\Category;
use App\Ship\Parents\Controllers\Frontend\ApiRoute;
use Illuminate\Support\Facades\Schema;
use App\Ship\Parents\Controllers\Frontend\ApiCart;
use App\Ship\Parents\Controllers\Frontend\ApiWishlist;

class ControllerWishlist extends ApiController
{

  public function getWishlist(CreateBaseRequest $R)
  {
    $customer = ApiHelper::getCustomer();
    $customer_id = @$customer->id;
    $lang = $R->lang ? $R->lang : 'en';
    return ApiWishlist::getWishlistResponse($customer_id, $lang);
  }  

  public function addWishlist(CreateBaseRequest $R)
  {
    $res      = []; 
    $sc       = 0;
    $table    = 'olmo_'.$R->type;
    $item_id  = $R->id;
    $sc       = 200;

    $model    =  @Db::table($table)->where('id',$item_id)->get();

    if(count($model) == 0){
      $sc = 400;
    }else{
      
      $customer = ApiHelper::getCustomer();
      $customer_id = @$customer->id;

      if($customer_id){
          //Controllo se c'è gia in carello
        $wish = Db::table('olmo_wishlist') ->where('customer_id',$customer_id)->where('model',$R->type) ->where('model_id',$item_id)->first();

        if(!($wish)) {
          Db::table('olmo_wishlist')->insert([
            'model_id' => $item_id,
            'model' => $R->type,
            'customer_id' => @$customer_id
          ]);
        }
        $lang = $R->lang ? $R->lang : 'en';
        $res = ApiWishlist::getWishlistResponse($customer_id, $lang);
      }else{
        $sc = 400;
      }
    }
    return Response($res,$sc);
  }  


  public function deleteWishlist(CreateBaseRequest $request)
  {
    $customer    = ApiHelper::getCustomer();
    $customerId  = $customer->id;
    $id          = $request->id;
    $type        = $request->type;
    $r = Db::table('olmo_wishlist')->where('model',$type)->where('model_id',$id)->where('customer_id',$customerId)->delete();
    $status = $r ? 200 : 400;

    return Response($r,$status);
  }  

}
