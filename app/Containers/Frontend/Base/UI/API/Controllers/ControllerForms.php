<?php

namespace App\Containers\Frontend\Base\UI\API\Controllers;

use App\Containers\Frontend\Base\UI\API\Requests\CreateBaseRequest;
use App\Containers\Frontend\Base\UI\API\Requests\DeleteBaseRequest;
use App\Containers\Frontend\Base\UI\API\Requests\GetAllBasesRequest;
use App\Containers\Frontend\Base\UI\API\Requests\FindBaseByIdRequest;
use App\Containers\Frontend\Base\UI\API\Requests\UpdateBaseRequest;
use App\Containers\Frontend\Base\UI\API\Transformers\BaseTransformer;
use App\Containers\Frontend\Base\Actions\CreateBaseAction;
use App\Containers\Frontend\Base\Actions\FindBaseByIdAction;
use App\Containers\Frontend\Base\Actions\GetAllBasesAction;
use App\Containers\Frontend\Base\Actions\UpdateBaseAction;
use App\Containers\Frontend\Base\Actions\DeleteBaseAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Ship\Parents\Controllers\ApiHelper;
use App\Containers\Backoffice\Page\Models\Page;
use App\Containers\Backoffice\Template\Models\Template;
use App\Containers\Backoffice\Category\Models\Category;
use App\Ship\Parents\Controllers\Frontend\ApiRoute;
use Illuminate\Support\Facades\Schema;
use App\Ship\Parents\Controllers\ApiStructure;

class ControllerForms extends ApiController
{


    public function getListAddress(CreateBaseRequest $R){
      $customer = ApiHelper::getCustomer();
      $res         = [];
      $customer_id = @$customer->id;
      if($customer_id != ''){
        $address = DB::table('olmo_address')->where('customer_hidden_general',$customer_id)->where('enabled_is_general','true')->get()->toArray();
        $address = json_decode(json_encode($address), true);

        $res = array_map(function (array $arr) {
          $arr['_type']    = [$arr['typeaddress_select_general']];
          $arr['_default'] = $arr['default_is_general'] == "true" ? [$arr['typeaddress_select_general']] : [];
          unset($arr['typeaddress_select_general']);
          unset($arr['default_is_general']);
          return $arr;
        }, $address);

        return ApiHelper::cleanKeyArray($res);

      }else{
        return  response($res,400);
      }
    }


    public function saveAdress(CreateBaseRequest $request){
      
      $customer                           = ApiHelper::getCustomer();
      $customer_id                        = $customer->id;
      $attribute                          = ['table' => 'olmo_address'];
      $type                               = $request->_type[0];

      $request['enabled'] = 'true';
      $request['typeaddress'] = $type;
      $request['customer'] = $customer_id;
      // $request['id'] = 'empty';

      $buildRequest = ApiHelper::rebuildRequestByTable($attribute['table'], $request);

      $rows = Db::table($attribute['table'])->where('customer_hidden_general',$customer_id)
      ->where('typeaddress_select_general',$type)
      ->get()
      ->count();

      if($rows == 0){
        $buildRequest['default_is_general']   = 'true';
      }
    
      $response =  $this->add($buildRequest,$attribute);

      //Aggiorno la relazione nella tabella customer
      $address_id = Db::table($attribute['table'])->where('customer_hidden_general',$customer_id)->pluck('id')->toArray();
      $address_id = implode(',',$address_id);
      Db::table('olmo_customer')->where('id',$customer_id)->update([ 'address_list_general' => $address_id ]);
      
      return $response;

    }
  
    public function editAdress(CreateBaseRequest $request){   
      $customer                     = ApiHelper::getCustomer();
      $customer_id                  = $customer->id;
      $request['customer']          = $customer_id;
      $attribute                    = ['table' => 'olmo_address'];
      unset($request['_type']);
      unset($request['_default']);
      $buildRequest = ApiHelper::rebuildRequestByTable($attribute['table'], $request);
      return $this->update($buildRequest,$attribute);
    }


    public function deleteAdress(CreateBaseRequest $request){   
      $request['enabled_is_general']           = 'false';
      $attribute                    = ['table' => 'olmo_address'];
      return $this->update($request,$attribute);
    }


    public function setDefaultAddress(CreateBaseRequest $R){   
      $customer                     = ApiHelper::getCustomer();
      $attribute                    = ['table' => 'olmo_address'];

      $default  = $R->_default[0];
      Db::table('olmo_address')->where('typeaddress_select_general',$default)->update([
         'default_is_general' => 'false'
      ]);


  

      Db::table('olmo_address')->where('id',$R->id)->update([
        'default_is_general' => 'true'
      ]);

    }


    public function getForms(CreateBaseRequest $R){

      $type    = $R->type;
      $groups  = [];
      $exclude = [];
      $forms   = [];
      $forms['billing']  = 'address';
      $forms['shipping'] = 'address';
      $forms['register'] = 'customer';
      $forms['profile']  = 'customer';
      $model      = $forms[$type];
      $table      = 'olmo_'.$model;
      $columns    = Db::select('SHOW COLUMNS FROM '.$table);
      $attribute  = ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model))::getAttr();
      $groups     = $attribute['forms'][$type]['groups'];
      $exclude    = $attribute['forms'][$type]['exclude'];
      $submit     = $attribute['forms'][$type]['submit'];
      $required   = $attribute['required'];

      $Fields    = [];

      if($type == 'billing' || $type == 'shipping'){
        $Fields['fields'][] =  [
          'name'      =>  'id',
          'element'   =>  'Input',
          'typology'  =>  'hidden',
          'label'     =>  '',
          'required'  =>  '',
          'option'    =>  [],
        ];  
      }       

      /** Ciclo i campi */
      foreach ($columns as $c) {
        $P      = ApiHelper::getPartsField($c->Field);
        $valid  = ApiHelper::getValidFieldForms($c->Field,$groups,$exclude);
        
        $req    = false;

        if(in_array($c->Field,$required)){
          $req = true;
        }       
       
        if($valid){
          $Fields['fields'][] =  [
            'name'      =>  ApiHelper::cleanKeyString($c->Field),
            'element'   =>  $P['element'],
            'typology'  =>  $P['typology'],
            'label'     =>  $P['label'],
            'required'  =>  $req,
            'option'    =>  $P['option'],
          ];
       }
      }

      if($submit){
        $Fields['fields'][] =  [
          'name'      =>  'submit',
          'element'   =>  'Input',
          'typology'  =>  'submit',
          'label'     =>  'submit',
          'required'  =>  'false',
          'option'    =>  [],
        ];     
      }         

      return $Fields;

    }


}
