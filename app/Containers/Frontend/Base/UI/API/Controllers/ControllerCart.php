<?php

namespace App\Containers\Frontend\Base\UI\API\Controllers;

use App\Containers\Frontend\Base\UI\API\Requests\CreateBaseRequest;
use App\Containers\Frontend\Base\UI\API\Requests\DeleteBaseRequest;
use App\Containers\Frontend\Base\UI\API\Requests\GetAllBasesRequest;
use App\Containers\Frontend\Base\UI\API\Requests\FindBaseByIdRequest;
use App\Containers\Frontend\Base\UI\API\Requests\UpdateBaseRequest;
use App\Containers\Frontend\Base\UI\API\Transformers\BaseTransformer;
use App\Containers\Frontend\Base\Actions\CreateBaseAction;
use App\Containers\Frontend\Base\Actions\FindBaseByIdAction;
use App\Containers\Frontend\Base\Actions\GetAllBasesAction;
use App\Containers\Frontend\Base\Actions\UpdateBaseAction;
use App\Containers\Frontend\Base\Actions\DeleteBaseAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Ship\Parents\Controllers\ApiHelper;
use App\Containers\Backoffice\Page\Models\Page;
use App\Containers\Backoffice\Template\Models\Template;
use App\Containers\Backoffice\Category\Models\Category;
use App\Ship\Parents\Controllers\Frontend\ApiRoute;
use Illuminate\Support\Facades\Schema;
use App\Ship\Parents\Controllers\Frontend\ApiCart;
use App\Ship\Parents\Controllers\ApiCustomer;

class ControllerCart extends ApiController
{

  public function addCart(CreateBaseRequest $R){

    $res      = []; 
    $sc       = 0;
    $table    = 'olmo_'.$R->type;
    $item_id  = $R->id;
    $lang     = $R->lang;
    $sc       = 200;
    if($R->quantity == null || $R->quantity == 'null'){
      $R->quantity =  1;
    }

    $model    =  @Db::table($table)->where('id',$item_id)->get();

    if(count($model) == 0){

      $sc = 400;

      $resposne = [];

    } else {
      
      $customer = ApiHelper::getCustomer();

      if(@$customer->id == ''){
        $customer_id = $customer;
      } else {
        $customer_id = $customer->id;
      }

        //Controllo se c'è gia in carello
      $cart = Db::table('olmo_cartitem')
      ->where('customer_id_general',$customer_id)
      ->where('model_select_general',$R->type)
      ->where('model_id_general',$item_id)
      ->first();

      if($cart){
          $qty = $R->quantity + $cart->quantity_num_general;
          Db::table('olmo_cartitem')
          ->where('customer_id_general',$customer_id)
          ->where('model_select_general',$R->type)
          ->where('model_id_general',$item_id)
          ->update([
            'quantity_num_general'  => $qty,
          ]);

      }else{
        Db::table('olmo_cartitem')->insert([
          'model_id_general'      => $item_id,
          'model_select_general'  => $R->type,
          'quantity_num_general'  => $R->quantity,
          'customer_id_general'   => @$customer_id
        ]);
      }

      $resposne = ApiCart::getCartResponse($customer_id,$lang);

    }

    return Response($resposne,$sc);
      
  }

    public function getCart(CreateBaseRequest $R){
      $lang     = $R->lang;
      $customer = ApiHelper::getCustomer();
      if(@$customer->id == ''){
        $customer_id = $customer;
      } else {
        $customer_id = $customer->id;
      }

      $response = ApiCart::getCartResponse($customer_id,$lang);
      $satus = $response ? 200 : 400; 

      return Response($response, $satus);

    }

    public function addQty(CreateBaseRequest $request){

      $customer = ApiHelper::getCustomer();
      if(@$customer->id == ''){
        $customer_id = $customer;
      }else{
        $customer_id = $customer->id;
      }

      $cartitemid = $request->cartitemid;
      $qty        = $request->quantity;

      $cart = Db::table('olmo_cartitem')->where('id',$cartitemid)->first(); // questo a cosa serve?

      Db::table('olmo_cartitem')->where('id',$cartitemid)->update(['quantity_num_general' => $qty]);

      $res = ApiCart::getCartResponse($customer_id);

      return $res;
    }

    public function deleteCart(CreateBaseRequest $request){

      $lang     = $request->lang;
      $customer = ApiHelper::getCustomer();
      if(@$customer->id == ''){
        $customer_id = $customer;
      }else{
        $customer_id = $customer->id;
      }

      $id = $request->cartitemid;
      $response  = Db::table('olmo_cartitem')->where('id',$id)->where('customer_id_general',$customer_id)->delete();
      if($response){
        $response = ApiCart::getCartResponse($customer_id,$lang);
        return Response($response, 200);
      }else{
        return Response([],400);
      }
    }


    public function setAddress(CreateBaseRequest $R)
    {
      $lang         = @$R->lang;
      $customer    = ApiHelper::getCustomer();
      $customer_id = @$customer->id;
      ApiCustomer::setAddress($customer_id,$R);
      return ApiCart::getCartResponse($customer_id,$lang);
    }

    public function setShipping(CreateBaseRequest $R)
    {
      $lang         = @$R->lang;
      $customer    = ApiHelper::getCustomer();
      $customer_id = @$customer->id;
      ApiCustomer::setShipping($customer_id,$R);
      return ApiCart::getCartResponse($customer_id,$lang);
    }

    public function setPayment(CreateBaseRequest $R)
    {
      $lang         = @$R->lang;
      $customer    = ApiHelper::getCustomer();
      $customer_id = @$customer->id;
      ApiCustomer::setPayment($customer_id,$R);
      return ApiCart::getCartResponse($customer_id,$lang);
    }

}
