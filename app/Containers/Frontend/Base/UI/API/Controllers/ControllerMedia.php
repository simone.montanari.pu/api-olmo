<?php

namespace App\Containers\Frontend\Base\UI\API\Controllers;

use App\Containers\Frontend\Base\UI\API\Requests\CreateBaseRequest;
use Illuminate\Support\Facades\DB;
use App\Ship\Parents\Controllers\ApiController;
use App\Ship\Parents\Controllers\ApiHelper;
use App\Containers\Backoffice\Fillform\Models\Fillform;

class ControllerMedia extends ApiController
{

  public function requestAssets(CreateBaseRequest $request){

  	$truename = @$request->truename;
  	$media    = @Db::table('olmo_storage')->where('truename',$truename)->where('model','filemanager')->first();
	if(!$media) {
  	  return response([],403);
  	  exit();
    }
  	$tokens = Db::table('olmo_tokens')
                          ->where('updated_at',null)
                          ->where('modelid',$media->truename)
                          ->first();

    if(!$tokens) {
  	  return response(['token not found'],403);
  	  exit();
    }

  	$url_media = env('API_URL')."/storage/media/".$media->truename."?olmotoken=".$tokens->token;

  	//Invio la mail
  	$params = [
  		'email'    => $request->email,
  		'urlasset' => $url_media
  	];

  	$formid       = 115; ///@DB::table('olmo_fillform')->where('formtype_select_general','requestassets')->first()->code_txt_general;
    $res = @FillForm::sendMailFillform($formid,$params);

  
  	return $res;
   
  }
}