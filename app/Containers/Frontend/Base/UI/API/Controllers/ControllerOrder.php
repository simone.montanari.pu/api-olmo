<?php

namespace App\Containers\Frontend\Base\UI\API\Controllers;

use App\Containers\Frontend\Base\UI\API\Requests\CreateBaseRequest;
use App\Containers\Frontend\Base\UI\API\Requests\DeleteBaseRequest;
use App\Containers\Frontend\Base\UI\API\Requests\GetAllBasesRequest;
use App\Containers\Frontend\Base\UI\API\Requests\FindBaseByIdRequest;
use App\Containers\Frontend\Base\UI\API\Requests\UpdateBaseRequest;
use App\Containers\Frontend\Base\UI\API\Transformers\BaseTransformer;
use App\Containers\Frontend\Base\Actions\CreateBaseAction;
use App\Containers\Frontend\Base\Actions\FindBaseByIdAction;
use App\Containers\Frontend\Base\Actions\GetAllBasesAction;
use App\Containers\Frontend\Base\Actions\UpdateBaseAction;
use App\Containers\Frontend\Base\Actions\DeleteBaseAction;
use App\Ship\Parents\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Ship\Parents\Controllers\ApiHelper;
use App\Containers\Backoffice\Page\Models\Page;
use App\Containers\Backoffice\Template\Models\Template;
use App\Containers\Backoffice\Category\Models\Category;
use App\Ship\Parents\Controllers\Frontend\ApiRoute;
use Illuminate\Support\Facades\Schema;
use App\Ship\Parents\Controllers\Frontend\ApiCart;
use App\Ship\Parents\Controllers\Frontend\ApiWishlist;
use App\Ship\Parents\Controllers\ApiStructure;
use App\Containers\Backoffice\Fillform\Models\Fillform;

class ControllerOrder extends ApiController
{
  public function placeOrder(CreateBaseRequest $R)
  {
    $O = [];

    $customer    = ApiHelper::getCustomer();
    $customer_id = @$customer->id;


    $cart = ApiCart::getCartResponse($customer_id);


    if(count(@$cart['items']) == 0)
    {
      return Response("Cart is empty not possible create order",400); 
      exit();
    }



   

    $O['code_read_general']           = time().rand(100,999);
    $O['customerid_read_general']     = $customer_id;
    $O['createdonutc_read_general']   = date("Y-m-d H:i:s", time() - date("Z"));
    $O['lastmod_read_general']        = date("Y-m-d H:i:s");
    $O['orderstatus_select_general']  = 'pending';
    $O['paidonutc_read_general']      = '';
    $O['paymentmethod_id_general']    = $customer->paymentmethod_hidden_general;
    $O['shippingmethod_id_general']   = $customer->shippingmethod_hidden_general;
    $O['subtotal_read_general']       = $cart['subtotal'];
    $O['total_read_general']          = $cart['total'];
    $O['discountcode_read_general']   = '';
    $O['discountvalue_read_general']  = '';
    $O['totalvat_read_general']       = $cart['total_vat'];
    $O['vat_read_general']            = $customer->vat_txt_register;
    $O['country_read_general']        = $customer->country_txt_register;
    $O['company_read_general']        = $customer->company_txt_register;
    $O['orderitems_json_items']       = json_encode($cart['items']);


    /** Billing order  */
    @$B = Db::table('olmo_address')->where('id',$customer->billingaddress_hidden_general)->first();
    $O['billingname_read_general']       = @$B->name_txt_general;
    $O['billingsurname_read_general']    = @$B->surname_txt_general;
    $O['billingphone_read_general']      = @$B->phone_txt_general;
    $O['billingcompany_read_general']    = @$B->company_txt_general;
    $O['billingaddress1_read_general']   = @$B->address1_txt_general;
    $O['billingaddress2_read_general']   = @$B->address2_txt_general;
    $O['billingcity_read_general']       = @$B->city_txt_general;
    $O['billingregion_read_general']     = @$B->region_txt_general;
    $O['billingcountry_read_general']    = @$B->country_txt_general;
    $O['billingZIP_read_general']        = @$B->zip_txt_general;
    

    /**Shipping method */
    @$S = Db::table('olmo_address')->where('id',$customer->shippingaddress_hidden_general)->first();
    $O['shippingname_read_general']       = @$S->name_txt_general;
    $O['shippingsurname_read_general']    = @$S->surname_txt_general;
    $O['shippingphone_read_general']      = @$S->phone_txt_general;
    $O['shippingcompany_read_general']    = @$S->company_txt_general;
    $O['shippingaddress1_read_general']   = @$S->address1_txt_general;
    $O['shippingaddress2_read_general']   = @$S->address2_txt_general;
    $O['shippingcity_read_general']       = @$S->city_txt_general;
    $O['shippingregion_read_general']     = @$S->region_txt_general;
    $O['shippingcountry_read_general']    = @$S->country_txt_general;
    $O['shippingZIP_read_general']        = @$S->zip_txt_general;


    /**Meta info */
    $O['meta_hidden_general']              = @$customer->meta_hidden_general;



    $id = Db::table('olmo_order')->insertGetId($O);


    //Recupero l ordine appena creato
    $customer    = ApiHelper::getCustomer();
    $orders      = Db::table('olmo_order')->where('id',$id)->get();
    $order       = ApiStructure::responseFrontend($orders,['orderitems'],true);
    $O           = ApiStructure::getStructureOrder($order);

    $params = [];

  
    $params['id']   = $O['id'];
    $params['code'] = $O['code'];
    $params['subtotal'] = $O['subtotal'];
    $params['total'] = $O['total'];
    $params['totalvat'] = $O['totalvat'];
    $params['vat'] = $O['vat'];
    $params['country'] = $O['country'];
    $params['company'] = $O['company'];

    $keys = ['shippingaddress','billingaddress','shippingmethod','paymentmethod'];
    foreach($keys as $kk){
      foreach ($O[$kk] as $key => $value) {
        $key =  $kk.'_'.$key;
        $params[$key] = $value;
      }
    }
    
  
    $params['locale'] = $R->lang;
    $params['email']  = $customer->email_email_general;
    // $params['a']      = $params['email'];

   

    $formid        = @DB::table('olmo_fillform')->where('formtype_select_general','orderconfirmed')->first()->code_txt_general;
    $sendMail      = @FillForm::sendMailFillform($formid,$params);
   

    $response =[
      "orderid"       => $id,
      "status"        => "pending",
      "paymentmethod" => "banktransfer",
      "data"          =>'',
      'formid'        => $formid,
      "mail"          => $sendMail
    ];
    

    //Normlizzazione dello stato
    //1 svuoto il carello
    Db::table('olmo_cartitem')->where('customer_id_general',$customer_id)->delete();


 
    return $response;

  }



   public function getListOrder(CreateBaseRequest $R){
    $O = [];
    $customer    = ApiHelper::getCustomer();
    $customer_id = @$customer->id;
    $orders = Db::table('olmo_order')->where('customerid_read_general',$customer_id)->get();
    $orders = ApiStructure::responseFrontend($orders,['orderitems']);
    foreach ($orders as $order) {
      $O[] = ApiStructure::getStructureOrder($order);
    }
    return $O;
   }


  public function getSingleOrder(CreateBaseRequest $R){
   
    $customer    = ApiHelper::getCustomer();
    $customer_id = @$customer->id;
    $order_id    = $R->id;
    $orders = Db::table('olmo_order')->where('customerid_read_general',$customer_id)->where('id',$order_id)->get();
    $order = ApiStructure::responseFrontend($orders,['orderitems'],true);


    $O = ApiStructure::getStructureOrder($order);

    return $O;

   }

   public function chnageStatusOrder(CreateBaseRequest $R){

    $valid_status  = array('pending','paid','shipped','completed','canceled', 'refunded');
    $O = [];
    $customer    = ApiHelper::getCustomer();
    $customer_id = @$customer->id;
    $order_id    = $R->id;
    $status      = $R->status;

    if( ! (in_array($status,$valid_status)) ){

        return Response(["Status order not valid"],404);
        exit();
    }


    Db::table('olmo_order')->where('customerid_read_general',$customer_id)->where('id',$order_id)->update([
      'orderstatus_select_general' => $status  
    ]);

    $formid = '';

    @FillForm::sendMailFillform($formid,$params);


    $orders = Db::table('olmo_order')->where('customerid_read_general',$customer_id)->where('id',$order_id)->get();
    return ApiStructure::responseFrontend($orders);
   }
 
}
