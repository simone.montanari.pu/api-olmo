<?php

namespace App\Containers\Frontend\Base\Actions;

use App\Containers\Frontend\Base\Models\Base;
use App\Containers\Frontend\Base\Tasks\CreateBaseTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateBaseAction extends Action
{
    public function run(Request $request): Base
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateBaseTask::class)->run($data);
    }
}
