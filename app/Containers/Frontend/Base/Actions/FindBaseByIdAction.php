<?php

namespace App\Containers\Frontend\Base\Actions;

use App\Containers\Frontend\Base\Models\Base;
use App\Containers\Frontend\Base\Tasks\FindBaseByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindBaseByIdAction extends Action
{
    public function run(Request $request): Base
    {
        return app(FindBaseByIdTask::class)->run($request->id);
    }
}
