<?php

namespace App\Containers\Frontend\Base\Actions;

use App\Containers\Frontend\Base\Tasks\DeleteBaseTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteBaseAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteBaseTask::class)->run($request->id);
    }
}
