<?php

namespace App\Containers\Frontend\Base\Actions;

use App\Containers\Frontend\Base\Tasks\GetAllBasesTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllBasesAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllBasesTask::class)->addRequestCriteria()->run();
    }
}
