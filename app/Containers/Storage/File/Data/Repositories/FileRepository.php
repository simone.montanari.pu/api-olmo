<?php

namespace App\Containers\Storage\File\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class FileRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
