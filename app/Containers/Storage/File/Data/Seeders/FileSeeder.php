<?php

namespace App\Containers\Storage\File\Data\Seeders;

use App\Ship\Parents\Seeders\Seeder;
use Illuminate\Support\Facades\DB;

class FileSeeder extends Seeder
{
    public function run()
    {
        DB::table('olmo_storage')->truncate();
       
		DB::table('olmo_storage')->insert([
			'filename' 	=> '0000-1111-6666-777.jpeg',
			'truename' 	=> 'luna.jpg',
			'model'     => 'product',
			'public'    => 'true',
			'type'      => 'jpg'
		]);

    }
}
