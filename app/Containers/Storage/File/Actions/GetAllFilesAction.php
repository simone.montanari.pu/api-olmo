<?php

namespace App\Containers\Storage\File\Actions;

use App\Containers\Storage\File\Tasks\GetAllFilesTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllFilesAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllFilesTask::class)->addRequestCriteria()->run();
    }
}
