<?php

namespace App\Containers\Storage\File\Actions;

use App\Containers\Storage\File\Models\File;
use App\Containers\Storage\File\Tasks\FindFileByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindFileByIdAction extends Action
{
    public function run(Request $request): File
    {
        return app(FindFileByIdTask::class)->run($request->id);
    }
}
