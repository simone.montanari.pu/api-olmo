<?php

namespace App\Containers\Storage\File\Actions;

use App\Containers\Storage\File\Models\File;
use App\Containers\Storage\File\Tasks\UpdateFileTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateFileAction extends Action
{
    public function run(Request $request): File
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateFileTask::class)->run($request->id, $data);
    }
}
