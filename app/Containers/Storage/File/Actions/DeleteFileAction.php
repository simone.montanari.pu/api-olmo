<?php

namespace App\Containers\Storage\File\Actions;

use App\Containers\Storage\File\Tasks\DeleteFileTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteFileAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteFileTask::class)->run($request->id);
    }
}
