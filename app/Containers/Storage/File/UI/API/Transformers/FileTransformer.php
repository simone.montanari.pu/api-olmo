<?php

namespace App\Containers\Storage\File\UI\API\Transformers;

use App\Containers\Storage\File\Models\File;
use App\Ship\Parents\Transformers\Transformer;

class FileTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(File $file): array
    {
        $response = [
            'object' => $file->getResourceKey(),
            'id' => $file->getHashedKey(),
            'created_at' => $file->created_at,
            'updated_at' => $file->updated_at,
            'readable_created_at' => $file->created_at->diffForHumans(),
            'readable_updated_at' => $file->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $file->id,
            // 'deleted_at' => $file->deleted_at,
        ], $response);

        return $response;
    }
}
