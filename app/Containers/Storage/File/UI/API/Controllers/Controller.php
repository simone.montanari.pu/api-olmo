<?php

namespace App\Containers\Storage\File\UI\API\Controllers;

use App\Containers\Storage\File\UI\API\Requests\CreateFileRequest;
use App\Containers\Storage\File\UI\API\Requests\DeleteFileRequest;
use App\Containers\Storage\File\UI\API\Requests\GetAllFilesRequest;
use App\Containers\Storage\File\UI\API\Requests\FindFileByIdRequest;
use App\Containers\Storage\File\UI\API\Requests\UpdateFileRequest;
use App\Containers\Storage\File\UI\API\Transformers\FileTransformer;
use App\Containers\Storage\File\Actions\CreateFileAction;
use App\Containers\Storage\File\Actions\FindFileByIdAction;
use App\Containers\Storage\File\Actions\GetAllFilesAction;
use App\Containers\Storage\File\Actions\UpdateFileAction;
use App\Containers\Storage\File\Actions\DeleteFileAction;
use App\Ship\Parents\Controllers\ApiController;
use App\Ship\Parents\Controllers\ApiStorage;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Picqer\Barcode\BarcodeGeneratorHTML;
use Picqer\Barcode\BarcodeGeneratorSVG;
use Picqer\Barcode\BarcodeGeneratorPNG;


class Controller extends ApiController
{    

    public function getFileManager(GetAllFilesRequest $request){

        return ApiStorage::getFileManager($request);

    }    

    public function getFile(GetAllFilesRequest $request){

        return ApiStorage::getFile($request);

    }

    public function getBarcode(GetAllFilesRequest $request){

        return ApiStorage::getBarcode($request);

    }
    
}
