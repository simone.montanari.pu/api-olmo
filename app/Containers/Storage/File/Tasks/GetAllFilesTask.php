<?php

namespace App\Containers\Storage\File\Tasks;

use App\Containers\Storage\File\Data\Repositories\FileRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllFilesTask extends Task
{
    protected FileRepository $repository;

    public function __construct(FileRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
