<?php

namespace App\Containers\Storage\Filemanager\Data\Repositories;

use App\Ship\Parents\Repositories\Repository;

class FilemanagerRepository extends Repository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id' => '=',
        // ...
    ];
}
