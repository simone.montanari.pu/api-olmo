<?php

namespace App\Containers\Storage\Filemanager\Actions;

use App\Containers\Storage\Filemanager\Models\Filemanager;
use App\Containers\Storage\Filemanager\Tasks\FindFilemanagerByIdTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class FindFilemanagerByIdAction extends Action
{
    public function run(Request $request): Filemanager
    {
        return app(FindFilemanagerByIdTask::class)->run($request->id);
    }
}
