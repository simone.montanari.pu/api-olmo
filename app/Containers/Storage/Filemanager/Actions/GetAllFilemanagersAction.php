<?php

namespace App\Containers\Storage\Filemanager\Actions;

use App\Containers\Storage\Filemanager\Tasks\GetAllFilemanagersTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class GetAllFilemanagersAction extends Action
{
    public function run(Request $request)
    {
        return app(GetAllFilemanagersTask::class)->addRequestCriteria()->run();
    }
}
