<?php

namespace App\Containers\Storage\Filemanager\Actions;

use App\Containers\Storage\Filemanager\Models\Filemanager;
use App\Containers\Storage\Filemanager\Tasks\UpdateFilemanagerTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class UpdateFilemanagerAction extends Action
{
    public function run(Request $request): Filemanager
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(UpdateFilemanagerTask::class)->run($request->id, $data);
    }
}
