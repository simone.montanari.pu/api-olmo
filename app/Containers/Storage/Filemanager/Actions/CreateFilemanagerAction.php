<?php

namespace App\Containers\Storage\Filemanager\Actions;

use App\Containers\Storage\Filemanager\Models\Filemanager;
use App\Containers\Storage\Filemanager\Tasks\CreateFilemanagerTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class CreateFilemanagerAction extends Action
{
    public function run(Request $request): Filemanager
    {
        $data = $request->sanitizeInput([
            // add your request data here
        ]);

        return app(CreateFilemanagerTask::class)->run($data);
    }
}
