<?php

namespace App\Containers\Storage\Filemanager\Actions;

use App\Containers\Storage\Filemanager\Tasks\DeleteFilemanagerTask;
use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;

class DeleteFilemanagerAction extends Action
{
    public function run(Request $request)
    {
        return app(DeleteFilemanagerTask::class)->run($request->id);
    }
}
