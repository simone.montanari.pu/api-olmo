<?php

namespace App\Containers\Storage\Filemanager\Tasks;

use App\Containers\Storage\Filemanager\Data\Repositories\FilemanagerRepository;
use App\Ship\Exceptions\DeleteResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class DeleteFilemanagerTask extends Task
{
    protected FilemanagerRepository $repository;

    public function __construct(FilemanagerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id): ?int
    {
        try {
            return $this->repository->delete($id);
        }
        catch (Exception $exception) {
            throw new DeleteResourceFailedException();
        }
    }
}
