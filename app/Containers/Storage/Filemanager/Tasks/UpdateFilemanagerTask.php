<?php

namespace App\Containers\Storage\Filemanager\Tasks;

use App\Containers\Storage\Filemanager\Data\Repositories\FilemanagerRepository;
use App\Ship\Exceptions\UpdateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class UpdateFilemanagerTask extends Task
{
    protected FilemanagerRepository $repository;

    public function __construct(FilemanagerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id, array $data)
    {
        try {
            return $this->repository->update($data, $id);
        }
        catch (Exception $exception) {
            throw new UpdateResourceFailedException();
        }
    }
}
