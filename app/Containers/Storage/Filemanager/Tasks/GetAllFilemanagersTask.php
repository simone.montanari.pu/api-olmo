<?php

namespace App\Containers\Storage\Filemanager\Tasks;

use App\Containers\Storage\Filemanager\Data\Repositories\FilemanagerRepository;
use App\Ship\Parents\Tasks\Task;

class GetAllFilemanagersTask extends Task
{
    protected FilemanagerRepository $repository;

    public function __construct(FilemanagerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run()
    {
        return $this->repository->paginate();
    }
}
