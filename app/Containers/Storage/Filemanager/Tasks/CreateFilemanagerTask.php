<?php

namespace App\Containers\Storage\Filemanager\Tasks;

use App\Containers\Storage\Filemanager\Data\Repositories\FilemanagerRepository;
use App\Ship\Exceptions\CreateResourceFailedException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class CreateFilemanagerTask extends Task
{
    protected FilemanagerRepository $repository;

    public function __construct(FilemanagerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run(array $data)
    {
        try {
            return $this->repository->create($data);
        }
        catch (Exception $exception) {
            throw new CreateResourceFailedException();
        }
    }
}
