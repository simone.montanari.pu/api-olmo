<?php

namespace App\Containers\Storage\Filemanager\Tasks;

use App\Containers\Storage\Filemanager\Data\Repositories\FilemanagerRepository;
use App\Ship\Exceptions\NotFoundException;
use App\Ship\Parents\Tasks\Task;
use Exception;

class FindFilemanagerByIdTask extends Task
{
    protected FilemanagerRepository $repository;

    public function __construct(FilemanagerRepository $repository)
    {
        $this->repository = $repository;
    }

    public function run($id)
    {
        try {
            return $this->repository->find($id);
        }
        catch (Exception $exception) {
            throw new NotFoundException();
        }
    }
}
