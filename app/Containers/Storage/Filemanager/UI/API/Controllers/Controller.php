<?php

namespace App\Containers\Storage\Filemanager\UI\API\Controllers;

use App\Containers\Storage\Filemanager\UI\API\Requests\CreateFilemanagerRequest;
use App\Containers\Storage\Filemanager\UI\API\Requests\DeleteFilemanagerRequest;
use App\Containers\Storage\Filemanager\UI\API\Requests\GetAllFilemanagersRequest;
use App\Containers\Storage\Filemanager\UI\API\Requests\FindFilemanagerByIdRequest;
use App\Containers\Storage\Filemanager\UI\API\Requests\UpdateFilemanagerRequest;
use App\Containers\Storage\Filemanager\UI\API\Transformers\FilemanagerTransformer;
use App\Containers\Storage\Filemanager\Actions\CreateFilemanagerAction;
use App\Containers\Storage\Filemanager\Actions\FindFilemanagerByIdAction;
use App\Containers\Storage\Filemanager\Actions\GetAllFilemanagersAction;
use App\Containers\Storage\Filemanager\Actions\UpdateFilemanagerAction;
use App\Containers\Storage\Filemanager\Actions\DeleteFilemanagerAction;
use App\Ship\Parents\Controllers\ApiController;
use App\Ship\Parents\Controllers\ApiUpload;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class Controller extends ApiController
{
    public static function getMeta($type,$filename,$path,$info)
    {
        $file            = Db::table('olmo_storage')->where('filename','like','%'.$filename.'%')->first();
        $res = [];
      
        if($type == 'folder'){
          $res['caption']  = '';
          $res['alt']      = '';  
          $res['name']     = $filename;
          $res['idfile']   = self::generatePath($path.'/'.$info['basename']);;
        } else {
          $truename        = pathinfo($file->truename);
          $res['name']     = $truename['filename'];
          $res['idfile']   = $file->id;
          $res['path']     = $file->truename;
          $res['caption']  = ( @$file->caption != '' )  ?   @$file->caption : '';
          $res['alt']      = ( @$file->alt     != '' )  ?   @$file->alt : '';
          $res['public']   = ( @$file->public  != '' )  ?   @$file->public : '';
        }
       
        return $res;
    }

    public static function generatePath($path)
    {
      $string = substr($path, 0, strpos($path, 'media/'));
      $replace = str_replace($string, '', $path);
      $finalpath = str_replace('media/', '', $replace);
      return $finalpath;
    }
  
    public static  function getFileInfo($filename,$path,$i)
    {
        $res               = [];
     
        $info              = pathinfo($filename);
        if(array_key_exists('extension',$info))
        {
          $res['id']           = uniqid();
          $meta                = self::getMeta('file',$filename,$path,$info);
          $res['name']         = $meta['name'];
          $res['physicname']   = $filename;
          $res['type']         = 'file';
          $res['extension']    = $info['extension'];
          $res['description']  = $meta;
        }else{
          $res['id']           = uniqid();
          $meta                = self::getMeta('folder',$filename,$path,$info);
          $res['name']         = $filename;
          $res['physicname']   = '';
          $res['type']         = 'folder';
          $res['extension']    = '';
          $other               = $path.'/'.$info['basename'];
          $res['description']  = $meta;
          $i+=1;
          $res['other']        = self::scanAllDir($other,$i);
        }

        return ['file' => $res, 'index' => $i];
    }

    public static  function scanAllDir($path,$i)
    {
      $result = [];
    
      $files  = preg_grep('/^([^.])/', scandir($path));
    
      foreach($files as $filename){
        $res = self::getFileInfo($filename,$path,$i);
        $result[]  = $res['file'];
        $i+=1;
      }

      return $result;
    }
    
    public function getAllFile(GetAllFilemanagersRequest $request)
    {
      $res = [];
      $path = storage_path('app/public/media');
      $res =  self::scanAllDir($path,0);

      return $res;
    }

    public static function normalizestring($text, string $divider = '-')
    {
      // $text = preg_replace('~[^\pL\d]+~u', $divider, $text);
      $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
      $text = preg_replace('~[^-\w]+~', '', $text);
      $text = trim($text, $divider);
      $text = preg_replace('~-+~', $divider, $text);
      $text = strtolower($text);

      if (empty($text)) {
        return 'n-a';
      }
      return $text;
    }
 
    public function fileEdit(GetAllFilemanagersRequest $request)
    {
      $sc                = 200;
      $idfile           = $request->idfile;
      $filename         = $request->filename;
      $file             = Db::table('olmo_storage')->where('id',$idfile)->first();
      $infofile         = pathinfo($file->truename);
      $current_filename = $infofile['filename'];
      $truename         = $file->truename;
      $checkTrueName    = 0;
      $resUpdate       = false;
      $data   = [
        'alt'     => $request->alt,
        'caption' => $request->caption,
        'public'  => $request->public
      ];
    
      if($filename != ''){
      $filename  = self::normalizestring($filename);
      $truename  = str_replace($current_filename,$filename,$truename);
      $data['truename'] = $truename;
      $checkTrueName =  DB::table('olmo_storage')->where('truename',$truename)->where('id', '!=', $idfile)->get()->count();
      }

      if($checkTrueName > 0){
      $sc = 403;          
      }else{
      $resUpdate = Db::table('olmo_storage')->where('id',$idfile)->update($data);
      }
  
  
      $msg = [
        'update'   => $resUpdate,
        'infofile' => $infofile,
        'truename' => $truename
      ];

      return Response($msg,$sc);
    }

    public static function str_replace_first($search, $replace, $subject)
    {
      $search = '/'.preg_quote($search, '/').'/';
      return preg_replace($search, $replace, $subject, 1);
    }

    public function folderEdit(GetAllFilemanagersRequest $request)
    {
       $sc               = 200;
       $folderPath       = $request->folderPath;
       $folderName       = $request->folderName;
       $paths            = explode('/',$folderPath);
       $lastDir          = $paths[count($paths) -1];  
       $newpath = '';
       for($i=0; $i < count($paths) - 1; $i++){
        $newpath .= $paths[$i].'/';
       }
       $newpath .= $folderName;
       $folderPathFull = storage_path('app/public/media/'. $folderPath);
       $newpathFull = storage_path('app/public/media/'. $newpath);
       if (file_exists($folderPathFull)) {
          rename($folderPathFull, $newpathFull);
          $files = Db::table('olmo_storage')->get();
          foreach($files as $file){
             $truename    = $file->truename;
             $idfile      = $file->id;
             $newtruename = self::str_replace_first($folderPath,$newpath,$truename);
             Db::table('olmo_storage')->where('id',$idfile)->update(['truename' => $newtruename]);
          }
          return response("Nome cartella cambiato",200);
       }else{
          return response("Il percorso specificato non esiste",403);
       }
    }

    public function moveFiles(GetAllFilemanagersRequest $request)
    {
      $sc                = 200;
      $movefiles         = $request->movefiles;
      $destinationfolder = $request->destinationfolder;
      $fileMoved = [];
      
      foreach($movefiles as $file){
        $query                 = Db::table('olmo_storage')->where('id',$file)->first();
        $truename                 = $query->truename;
        $filename                 = $query->filename;
        $path                     = pathinfo($truename);
        $newpath                  = $destinationfolder.'/'.$path['basename'];
        $physicnewpath            = $destinationfolder.'/'.$filename;
        $current_physic_path      = $path['dirname'].'/'.$filename;
        // $folderPathFull           = storage_path('app/public/media/'. $filename);
        $physicnewpathFull        = storage_path('app/public/media/'. $physicnewpath);
        $current_physic_path_full = storage_path('app/public/media/'. $current_physic_path);
      
        if (file_exists($current_physic_path_full)) {
          rename($current_physic_path_full,$physicnewpathFull);
          $newtruename = self::str_replace_first($truename,$newpath,$truename);
          Db::table('olmo_storage')->where('id',$file)->update(['truename' => $newtruename]);            
          $message = "File moved. Id: ";
          $code = 200;
          array_push($fileMoved, $file);
        }else{
          $message = "The file is not in the file system";
          $code = 404;
        }        
      }

      $message = $message.implode(',', $fileMoved);

      return response($message, 200);
    }

    public function deleteFoldersAndFile($dir)
    {      
      $it = new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS);
      $files = new \RecursiveIteratorIterator($it,
                   \RecursiveIteratorIterator::CHILD_FIRST);
      foreach($files as $file) {
          if ($file->isDir()){
            rmdir($file->getRealPath());
          } else {
            $filename = explode('/', $file);
            Db::table('olmo_storage')->where('filename',$filename[count($filename) - 1])->delete();
            unlink($file->getRealPath());
          }
      }
      rmdir($dir);      
    }

    public function deleteFile(GetAllFilemanagersRequest $request)
    {      
      $ids = $request->ids;

      foreach($ids as $id){
        $query = Db::table('olmo_storage')->where('id',$id)->first();

        if($query){
          $pathname = $query->truename;
          $physicName = $query->filename;
          $filename = explode("/", $pathname);
          $path = str_replace($filename[count($filename) - 1], $physicName, $pathname);
  
          $physicFilePath = storage_path('app/public/media/'. $path);
  
          if(file_exists($physicFilePath)) {
            if(realpath($physicFilePath)){
              if(is_writable($physicFilePath)){
                unlink($physicFilePath);
                Db::table('olmo_storage')->where('id',$id)->delete();
              } else {
                return response('error here' . $id, 400);    
              }
            } else {
              return response('error here' . $id, 400);
            }
            $code = 200;
            $message = "File deleted";
          } else {            
            return response('error here' . $id, 400);
          }
        } else {
          self::deleteFoldersAndFile(storage_path('app/public/media/'. $id));
          $code = 200;
          $message = "File deleted";          
        }

      }

      return response($message, $code);
      
    }

    public function getToken(GetAllFilemanagersRequest $request)
    {
        $data = [
          'token'     => md5(time().'tokenolmo'),
          'model'     => $request->model,
          'modelid'   => $request->modelid,
          'created_at' => date('Y-m-d H:i:s')
        ];

        return Db::table('olmo_tokens')->insert($data);
    }

    public function uploadChunk(GetAllFilemanagersRequest $request)
    {
      return ApiUpload::uploadChunk();

    }
    public function uploadComplete(GetAllFilemanagersRequest $request)
    {
      return ApiUpload::completeChunkManager();
     
    }

    public function folderCreate(GetAllFilemanagersRequest $request)
    {
      return ApiUpload::folderCreate($request);
     
    }

    public function fileMove(GetAllFilemanagersRequest $request)
    {
      return ApiUpload::fileMove();
     
    }


 }
