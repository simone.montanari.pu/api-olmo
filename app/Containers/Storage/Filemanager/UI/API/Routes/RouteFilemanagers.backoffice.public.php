<?php

/**
 * @apiGroup           Filemanager
 * @apiName            getAllFilemanagers
 *
 * @api                {GET} /backoffice/** Endpoint title here..
 * @apiDescription     Endpoint description here..
 *
 * @apiVersion         1.0.0
 * @apiPermission      none
 *
 * @apiParam           {String}  parameters here..
 *
 * @apiSuccessExample  {json}  Success-Response:
 * HTTP/1.1 200 OK
{
  // Insert the response of the request here...
}
 */

use App\Containers\Storage\Filemanager\UI\API\Controllers\Controller;
use Illuminate\Support\Facades\Route;

Route::get('filemanager/get/all', [Controller::class, 'getAllFile'])->middleware(['auth:api']);
Route::post('filemanager/uploadchunks', [Controller::class, 'uploadChunk'])->middleware(['auth:api']);
Route::post('filemanager/uploadcomplete', [Controller::class, 'uploadComplete'])->middleware(['auth:api']);

//Create folder
Route::put('filemanager/folder/create', [Controller::class, 'folderCreate'])->middleware(['auth:api']);

//Move file
Route::patch('filemanager/file/move', [Controller::class, 'fileMove'])->middleware(['auth:api']);

//Edit file
Route::patch('filemanager/file/edit/{idfile}', [Controller::class, 'fileEdit'])->middleware(['auth:api']);

//Move file
Route::patch('filemanager/files/move', [Controller::class, 'moveFiles'])->middleware(['auth:api']);

//Edit folder
Route::patch('filemanager/folder/edit', [Controller::class, 'folderEdit'])->middleware(['auth:api']);

//Delete
Route::patch('filemanager/delete', [Controller::class, 'deleteFile'])->middleware(['auth:api']);



//Post
Route::post('filemanager/file/token', [Controller::class, 'getToken']);