<?php

namespace App\Containers\Storage\Filemanager\UI\API\Transformers;

use App\Containers\Storage\Filemanager\Models\Filemanager;
use App\Ship\Parents\Transformers\Transformer;

class FilemanagerTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    public function transform(Filemanager $filemanager): array
    {
        $response = [
            'object' => $filemanager->getResourceKey(),
            'id' => $filemanager->getHashedKey(),
            'created_at' => $filemanager->created_at,
            'updated_at' => $filemanager->updated_at,
            'readable_created_at' => $filemanager->created_at->diffForHumans(),
            'readable_updated_at' => $filemanager->updated_at->diffForHumans(),

        ];

        $response = $this->ifAdmin([
            'real_id'    => $filemanager->id,
            // 'deleted_at' => $filemanager->deleted_at,
        ], $response);

        return $response;
    }
}
