<?php

namespace App\Ship\Commands;

use App\Ship\Parents\Commands\ConsoleCommand;
use Illuminate\Support\Facades\DB;

class FillformGetCommand extends ConsoleCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fillform:form {token}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Hello Fillform!';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        //https://api.fillform.io/20e35e4b224c05493cdf6e3f5460bda2/checktoken
        $token = $this->argument('token');
        $url   = "https://api.fillform.io/".$token."/checktoken";
        $response =  json_decode(file_get_contents($url));

        if($response->user == 'ko'){

            $this->error("Token FILLFORM non valido !");
            exit();
        }
        
        if ($this->confirm('Sei sicuro di volere importare i form per '.$response->user)) {
            foreach($response->forms as $form){
                $code = $form[0];
                $name = $form[1];
                DB::table('olmo_fillform')->insert([
                    'code_txt_general' => $code,
                    'name_txt_general' => $name,
                ]);

                $this->line('<fg=green> Importo form: <fg=red> '.$code.'</>  nome: '.$name.'</>');
            }
        }else{
            $this->line('<fg=green>Arrivederci !</>');
        }
        

        

    }
}
