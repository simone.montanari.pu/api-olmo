<?php

namespace App\Ship\Middlewares\Http;

use App\Ship\Exceptions\AuthenticationException;
use Exception;
use Illuminate\Auth\Middleware\Authenticate as LaravelAuthenticate;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
class Authenticate extends LaravelAuthenticate
{
    public function authenticate($request, array $guards): void
    {
        $header = getallheaders();
        $X_token =  @$header['x-token'];
        if($X_token == ''){
            $X_token =  @$header['X-Token'];
        }
        if($X_token == ''){
          
            abort(403);
            exit();
        } else {
            $user = DB::table('olmo_user')->where('token_hidden_general',$X_token)->get();
            if(count($user) == 0){
                abort(403);
                exit();
            }
        }

 
    }

    protected function redirectTo($request): ?string
    {
        return route(Config::get('appSection-authentication.login-page-url'));
    }
}
