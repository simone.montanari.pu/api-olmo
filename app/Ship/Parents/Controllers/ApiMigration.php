<?php

namespace App\Ship\Parents\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Database\Schema\Blueprint;

class ApiMigration
{


  public static function Page($table){
    $table->increments('id')->unsigned();
    self::Locale($table);
    $table->string('enabled_is_general', 5)->nullable();
    $table->text('name_txt_general')->nullable();
    $table->text('slug_txt_general')->nullable();
    $table->dateTime('create_read_general')->useCurrent()->nullable();
    $table->dateTime('lastmod_read_general')->useCurrentOnUpdate()->nullable();
    $table->integer('template_id_general')->nullable();
    $table->smallInteger('position_ord_general')->nullable();
    $table->string('menu_is_general')->nullable();
  }
  public static function Locale($table){
    $table->text('lang_langs_general')->nullable();
    $table->text('locale_hidden_general')->nullable();
    $table->text('parentid_hidden_general')->nullable();    
  }  
   public static function Seo($table){
    $table->text('metatitle_txt_seo')->nullable();
    $table->text('metadesc_txtarea_seo')->nullable();
    $table->text('metakw_txt_seo')->nullable();
    $table->text('ogtitle_txt_seo')->nullable();
    $table->text('ogdesc_txtarea_seo')->nullable();
    $table->text('ogimg_img_seo')->nullable();
    $table->text('index_select_seo')->nullable();
    $table->text('follow_select_seo')->nullable();
   }

   public static function Sitemap($table){
    $table->string('disable_is_sitemap', 5)->nullable();
    $table->string('ovewrite_is_sitemap', 5)->nullable();
    $table->text('frequency_select_sitemap')->nullable();
    $table->text('priority_select_sitemap')->nullable();
   }

   // Ecommerce Migration to apply in different container
   public static function ECustomer($table){
    $table->text('address_list_general')->nullable();
    $table->text('paymentmethod_hidden_general')->nullable();
    $table->text('shippingmethod_hidden_general')->nullable();
    $table->text('discountcode_hidden_general')->nullable();
    $table->text('shippingaddress_hidden_general')->nullable();
    $table->text('billingaddress_hidden_general')->nullable();
    $table->text('meta_hidden_general');
   }
   
   public static function EProduct($table){
    $table->unsignedDecimal('price_num_general', $precision = 12, $scale = 2)->nullable();
    $table->integer('qty_spin_general')->nullable();
    $table->text('sku_txt_general')->nullable();
   }

}
