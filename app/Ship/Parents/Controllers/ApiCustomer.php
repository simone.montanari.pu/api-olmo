<?php

namespace App\Ship\Parents\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class ApiCustomer
{

    public static function setAddress($customer_id, $request){
        $respose = Db::table('olmo_customer')->where('id',$customer_id)
        ->update([
            'billingaddress_hidden_general'  => $request->billing,
            'shippingaddress_hidden_general' => $request->shipping
        ]);
        if($respose){
            return Response(['ok'],200);
        }else{
            return Response(['ko'],400);
        }
    }

    public static function setShipping($customer_id,$R){
        $res = Db::table('olmo_customer')->where('id',$customer_id)
        ->update([
            'shippingmethod_hidden_general'  => $R->code
        ]);
        if($res){
            return Response(['ok'],200);
        }else{
            return Response(['ko'],400);
        }
    }


    public static function setPayment($customer_id,$R){
        $res = Db::table('olmo_customer')->where('id',$customer_id)
        ->update([
            'paymentmethod_hidden_general'  => $R->code,
            'meta_hidden_general'           => json_encode($R->meta)
        ]);
        if($res){
            return Response(['ok'],200);
        }else{
            return Response(['ko'],400);
        }
    }


}
