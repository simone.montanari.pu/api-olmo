<?php

namespace App\Ship\Parents\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Schema;
use \stdClass;

class ApiHelper 
{

    public static function ArrayValidLanguage(){

        $langs = Db::table('olmo_language')->where('enabled_is_general','true')->get();
        $validlang = [];

        foreach($langs as $lang){
            array_push($validlang, $lang->code_txt_general);
        }

        return $validlang;

    }  
    
    public static  function clean($string) {
       $string = str_replace(' ', '-', $string);
       $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
       return preg_replace('/-+/', '-', $string); 
    }

    public static function normalizestring($text, string $divider = '-')
    {
      $text         = pathinfo($text);
      $filename     = $text['filename'];
      $extension    = $text['extension'];
      $filename     = self::clean($filename);
      $filename     = strtolower($filename);
      return $filename.'.'.$extension;
    }
 

    public static function getValidFieldForms($field,$type,$exclude){
        $valid = false;
        $group  = @explode('_',$field)[2];
        if($group != '')
        {

            if(in_array($group,$type)){
                $valid = true;
            }
            if(in_array($field,$type)){
                 $valid = true;
            }
        }

        if(in_array($field,$exclude)){
            $valid = false;
        }

        return $valid;

    }


    public static function getTypeFront($element,$type){
        $t   = 'text';
        $e   = 'Input';
        $opt =  [];


        if($type == 'pwd')
            $t = 'password';

        if($type == 'is')
            $t = 'checkbox';

        if($element == 'country' || $element == 'state'){
            $e = 'InputList';
            $t = 'select';
            $opt = json_decode(file_get_contents("https://api.fillform.io/getNationsOlmo/en",true));
        }


        return [
            'typology' => $t,
            'element'  => $e,
            'option'   => $opt
        ];
    }

    public static function getPartsField($field)
    {
        $res = [];

        $item = explode('_',$field);

        $f  = self::getTypeFront(@$item[0],@$item[1]);

        $res['name']       = @$item[0];
        $res['element']    = $f['element'];
        $res['typology']   = $f['typology'];
        $res['label']      = @$item[0];
        $res['required']   = true;
        $res['option']     = $f['option'];


        return $res;
    }

    public static function CheckRequired($request,$required)
    {
        $valid = true;
        $res = [];
        $err = [];
        $valid = true;
        $body  = file_get_contents('php://input');
        $body_fields = array_keys(json_decode($body,true));
        foreach($required as $field){
           if(!(in_array($field,$body_fields))){
               $valid = false;
               $err[] = "$field missing";
           }
        }
        $res['debug'] = $body_fields;
        $res['valid'] = $valid;
        $res['err']   = $err;
        return $res;
       
    }

    public static function getTemplateInfoById($id){
        return @Db::table('olmo_template')->where('id',$id)->first();
    }

    public static function getUniquesIdByObject($ids){
        $res = [];
        foreach($ids as $id){
            $res[] = $id['id'];
        }
        return $res;
    }

    public static function getModelRelationByArray($model_table,$ids){
        $res = [];
        foreach($ids as $id){
            $data = @Db::table($model_table)->where('id',$id)->where('enabled_is_general','true')->get();
            if($data)
                $res[] = @self::FrontResponseChild($data)[0];
        }
     
        return $res;

    }





    public static function CheckRules($request,$rules)
    {

        // This is the pattarn to use to create rules
        //
        // ----- Unique
        // [
        //     'type' => 'unique',
        //     'field' => 'slug_txt_general',
        //     'table' => 'olmo_service',
        //     'lang' => true, default: true
        //     'errortxt' => 'Slug duplicate'
        // ] 
        // ----- Equal
        // [
        //     'type' => 'unique',
        //     'field_one' => 'password_pwd_general',
        //     'field_two' => 'confirmpassword_pwd_general',
        //     'errortxt' => 'Password not match'
        // ] 
        // ----- Check slug length
        // ----- Check slug length
                    

        $valid = true;
        $lang = $request->lang;
        $res = [];
        $err = [];
        $isLang = true;        
    
        foreach($rules as $rule){
            if($rule['type'] == 'equal'){
                $a = $request->{$rule['field_one']};
                $b = $request->{$rule['field_two']};     
                if($a != $b){
                    $valid = false;
                    $err[] = $rule['errortxt'];
                }
            }
            if($rule['type'] == 'unique'){
                $a   = $request->{$rule['field']};
                //Controllo che il valore del campo a sia presente solo una volta

                if(isset($rule['lang'])){
                    $isLang = $rule['lang'];
                }


                if($isLang){
                    $check = Db::table($rule['table'])->where($rule['field'],$a)->where('locale_hidden_general', $lang)->where('id','!=',$request->id)->get()->count();
                } else {
                    $check = Db::table($rule['table'])->where($rule['field'],$a)->where('id','!=',$request->id)->get()->count();                    
                }                
                if($check > 0){
                    $valid = false;
                    $err[] =$rule['errortxt'];
                }
             
            }
        }
        $res['valid'] = $valid;
        $res['err']   = $err;

        return $res;
       
    }



    public static function getChildren($table, $id)
    {
        $response = DB::table('olmo_'.$table)->where($table.'_id_general',$id)->get();
        return self::FrontResponseChild($response);
    }

    public static function rebuildRequestByTable($table, $data)
    {
        $data = $data->all();
        $request = new \Illuminate\Http\Request();
        $schema = Schema::getColumnListing($table);
        $convert = [];        

        foreach($data as $key=>$value){
            foreach($schema as $val){
                $pos = strpos($val, $key);
                if($pos !== false){
                    $convert[$val] = $value;
                }            
            }
            if($key == 'current_locale'){
                $convert['customerlang_txt_general'] = $value;
            }            
        }

        $request->replace($convert);

        return $request;
    }

    public static function cleanKeyArray($datas) 
    {
        $datas = json_decode(json_encode($datas), true);
        $array = [];
        foreach($datas as $items){
            $res = [];
            if(is_array($items) || is_object($items)){
                foreach($items as $k=>$v){                 
    
                    if($k == '_default' || $k == '_type'){
                        $key = $k;
                    }else{
                        $key  = @explode('_',$k)[0];
                        $type = @explode('_',$k)[1];
                        $tab  = @explode('_',$k)[2];
                    }
            
                    $res[$key] = $v;
                }
            }
            array_push($array,$res);
        }
        return $array;        
    }   

    public static function cleanKeyString($datas) 
    {
        $key  = @explode('_',$datas)[0];
        return $key;
    } 

    public static function cleanKeySimple($datas) 
    {
        $datas = json_decode(json_encode($datas), true);
        $res = [];
        foreach($datas as $k=>$v){

            $key  = @explode('_',$k)[0];
            $type = @explode('_',$k)[1];
            $tab  = @explode('_',$k)[2];
            
            $res[$key] = $v;
        }
        return $res;        
    } 

    public static function FrontResponseChild($datas) : array 
    {
        $datas = json_decode(json_encode($datas), true);
        $array = [];
        foreach($datas as $items){
            $res = [];
            if(is_array($items) || is_object($items)){
                foreach($items as $k=>$v){                 
    
                    $key  = @explode('_',$k)[0];
                    $type = @explode('_',$k)[1];
                    $tab  = @explode('_',$k)[2];
            
                    if($tab !='seo'){
                        if($type == 'id'){
                            $table_relation = env('TABLE_PREFIX').'_'.$key;
                            $res[$key]     = self::FrontResponseStrict(json_decode(json_encode(self::currentRelation($table_relation,$v,'select',true)), true));
                        }
                        else if($type == 'multid'){
                            $table_relation = env('TABLE_PREFIX').'_'.$key;
                            $res[$key]     =  self::cleanKeyArray(json_decode(json_encode(self::currentRelation($table_relation,$v,'multiselect',true)), true));
                        }
                        else if($type == 'dnd'){
                            $table_relation = env('TABLE_PREFIX').'_'.$key;
                            $res[$key]     =  json_decode(json_encode(self::currentRelation($table_relation,$v,'multiselect',true)), true);  
                        }
                        else if($type == 'multimg'){
                            $res[$key]      = self::getMultiplePathStorageById($v);
                        }                                         
                        else if($type == 'img'){
                            $res[$key]     = self::getSinglePathStorageById($v);
                        }
                        else if($type == 'media'){
                            $res[$key]      = self::getSinglePathStorageById($v);
                        }                        
                        else if($type == 'slider'){
                            $slider = explode(',',$v);
                            foreach($slider as $s){
                                $res[$key][] = json_decode(json_encode(self::getSingleFullSlide($s)), true);
                            }
                        }
                        else
                            $res[$key] = $v;
                    }                 
                }
            }
            array_push($array,$res);
        }
        return $array;
    }     
    public static function FrontResponseStrict($datas) : array 
    {
        $datas = json_decode(json_encode($datas), true);
        $res = [];

        foreach($datas as $k=>$v){

            $key  = @explode('_',$k)[0];
            $type = @explode('_',$k)[1];
            $tab  = @explode('_',$k)[2];
    
            if($tab !='seo'){
                if($type == 'id'){
                    $table_relation = env('TABLE_PREFIX').'_'.$key;
                    $res[$key]      = self::FrontResponseStrict(json_decode(json_encode(self::currentRelation($table_relation,$v,'select',true)), true));
                }
                else if($type == 'multid'){
                    $table_relation = env('TABLE_PREFIX').'_'.$key;
                    $res[$key]      =  self::FrontResponseArray(json_decode(json_encode(self::currentRelation($table_relation,$v,'multiselect',true)), true));
                }
                else if($type == 'dnd'){
                    $table_relation  = env('TABLE_PREFIX').'_'.$key;
                    $model_relation  = env('TABLE_PREFIX').'_'.self::getTemplateInfoById($datas['template_id_general'])->model_select_general;
                    $ids             = self::getValuesOrder($k,$datas['id'],$model_relation,$v);  
                    $ids             = self::getUniquesIdByObject($ids);
                    $res[$key]       = self::getModelRelationByArray($table_relation,$ids);
                }
                else if($type == 'multimg'){
                    $res[$key]      = self::getMultiplePathStorageById($v);
                }                 
                else if($type == 'visual'){
                    $res[$key]      =  json_decode($v, true);
                }                
                else if($type == 'img'){
                    $res[$key]      = self::getSinglePathStorageById($v);
                }
                else if($type == 'media'){
                    $res[$key]      = self::getSinglePathStorageById($v);
                }
                else if($type == 'slider'){
                    $slider = $v !== "" ? explode(',',$v) : [];
                    $slides = [];
                    foreach($slider as $s){
                        $slide = self::cleanKeySimple(json_decode(json_encode(self::getSingleFullSlide($s)), true));
                        array_push($slides, $slide);
                    }
                    $res[$key] = $slides;
                }
                else
                    $res[$key] = $v;
            }
             
        }

        return $res;
    }

    /**
     * Questo metodo sarebbe da cancellare ma non riesco a capire l'intrecio che ho fatto nei due precedenti e mi è rimasto tra le gambe
     */
    public static function FrontResponseArray($datas) : array 
    {
        $datas = json_decode(json_encode($datas), true);
        $array = [];
        foreach($datas as $items){
            $res = [];
            if(is_array($items) || is_object($items)){
                foreach($items as $k=>$v){                 
    
                    $key  = @explode('_',$k)[0];
                    $type = @explode('_',$k)[1];
                    $tab  = @explode('_',$k)[2];
            
                    if($tab !='seo'){
                        if($type == 'id'){
                            $table_relation = env('TABLE_PREFIX').'_'.$key;
                            $res[$key]     = self::FrontResponseStrict(json_decode(json_encode(self::currentRelation($table_relation,$v,'select',true)), true));
                        }
                        else if($type == 'multid'){
                            $table_relation = env('TABLE_PREFIX').'_'.$key;
                            $res[$key]     =  self::cleanKeyArray(json_decode(json_encode(self::currentRelation($table_relation,$v,'multiselect',true)), true));
                        }
                        else if($type == 'dnd'){
                            $table_relation = env('TABLE_PREFIX').'_'.$key;
                            $res[$key]     =  json_decode(json_encode(self::currentRelation($table_relation,$v,'multiselect',true)), true);  
                        }
                        else if($type == 'multimg'){
                            $res[$key]      = self::getMultiplePathStorageById($v);
                        }                                         
                        else if($type == 'img'){
                            $res[$key]     = self::getSinglePathStorageById($v);
                        }
                        else if($type == 'media'){
                            $res[$key]      = self::getSinglePathStorageById($v);
                        }                        
                        else if($type == 'slider'){
                            $slider = explode(',',$v);
                            foreach($slider as $s){
                                $res[$key][] = json_decode(json_encode(self::getSingleFullSlide($s)), true);;
                            }
                        }
                        else
                            $res[$key] = $v;
                    }                 
                }
            }
            array_push($array,$res);
        }
        return $array;
    }    

    public static function filterNotNull($array) {
        $arrays = array_map(function($item) {
            return is_array($item) ? self::filterNotNull($item) : $item;
        }, $array);
        
        return array_filter($arrays, function($item) {
            return $item !== "" && $item !== null && (!is_array($item) || count($item) > 0);
        });
    }    

    public static function getCategoryTreeFromParent($parent_id)
    {
    
        $categories =  DB::table('olmo_category')
                        ->where('category_id_general',$parent_id)
                        ->get()->toArray();

        $categories = json_decode(json_encode($categories), true);
        
        foreach ($categories as $category){
            $categories['children'] = self::getCategoryTreeFromParent($category['id']);     
         }

         print_r($categories);
            
    }    

    public static function currentRelation($table,$values,$type = '',$front = false){


        $columns = Db::select('SHOW COLUMNS FROM '.$table);
        $columns = json_decode(json_encode($columns), true);
        $ord     = false;
        foreach($columns as $col){
            if($col['Field'] == 'position_ord_general'){
                $ord = false;
            }
        }

        $items  = [];
        $i      = 0;
        if($values == '')
        {
            if($type == 'multiselect'){
                $items = [];

            }else{
                $item['key']    = '';
                $item['value']  = '';
                $items          = $item;
            }
        }
        else
        {

            if($front == true){
                $ids = explode(',',$values);
                if($ord){
                    $datas  = Db::table($table)->whereIn('id',$ids)
                    ->where('enabled_is_general','true')
                    ->orderBy('position_ord_general')->get();
                }else{
                    $datas  = Db::table($table)->whereIn('id',$ids)
                    ->where('enabled_is_general','true')->get();
                }

                $items = $datas;

            } else {

                $ids = explode(',',$values);
                $datas  = Db::table($table)->whereIn('id',$ids)->get();
                foreach($datas as $dato){
                    $item = [];
                    $item['key']    =  @$dato->name_txt_general;
                    $item['value']  = (string)@$dato->id;
                    $items[$i]   = $item;
                    $i++;
                }
            }

            if($type == 'select'){
                $items = @$items[0];
            }
        }
      
        return $items;
        
    }


    public static function getDefaultValue($required,$k)
    {
        $res = "";
        if(isset($required[$k]))
        {
             $res =  $required[$k];
        }
        return $res; 
    }

   public static function isRequired($required,$k)
   {
       
     $res = "false";
     if(is_array($required)){
        if(in_array($k,$required)){
            $res = "true";
        }
     }
     return $res;

   }

    public static function getSlugFront($model,$lang,$id_single){

        $row = Db::table('olmo_template')->where('locale_hidden_general',$lang)->where('model_select_general',$model)->first();
       
        if($row){
            $slug =  $row->slug_txt_general;
            $model = Db::table('olmo_'.$model)->where('id',$id_single)->first();
            $model_slug = $model->slug_txt_general;
            $slug    = str_replace(":slug",$model_slug,$slug);
            return $slug;
        }
        else
            return '';
    }
    public static function getSlug($lang,$name){

        $row = Db::table('olmo_template')->where('locale_hidden_general',$lang)->where('name_txt_general',$name)->first();
       
        if($row)
             return $row->slug_txt_general;
        else
            return '';
    }

    public static function getXtoken(){
        $header = getallheaders();
        $X_token =  @$header['x-token'];
        if($X_token == ''){
            $X_token =  @$header['X-Token'];
        }
        return $X_token;
    }

    public static function getUser(){
        $header = getallheaders();
        $X_token =  @$header['x-token'];
        if($X_token == ''){
            $X_token =  @$header['X-Token'];
        }

        return DB::table('olmo_user')->where('token_hidden_general',$X_token)->first();
    }


    public static function getCustomer(){
        $header = getallheaders();
        $X_token =  @$header['x-token'];
        if($X_token == ''){
            $X_token =  @$header['X-Token'];
        }
        $res = DB::table('olmo_customer')->where('token_read_general',$X_token)->first();
        if($res){
            return $res;
        }else{
            $guest_token =  @$header['x-guest'];
            if($X_token == ''){
                $guest_token =  @$header['x-guest'];
            }
            return $guest_token;
        }
    }

    public static function activeCustomer(){
        $header = getallheaders();
        $X_token =  @$header['x-token'];
        if($X_token == ''){
            $X_token =  @$header['X-Token'];
        }

        return DB::table('olmo_customer')
        ->where('tokenactive_read_general',$X_token)
        ->update([
            'customerstatus_select_general'       => 'active',
            'tokenactive_read_general' => ''
        ]);
    }

   

    public static function logoutCustomer(){
        $header = getallheaders();
        $X_token =  @$header['x-token'];
        if($X_token == ''){
            $X_token =  @$header['X-Token'];
        }
        $date = date('Y-m-d H:i:s');

        $res = DB::table('olmo_customer')
                ->where('token_read_general',$X_token)
                ->update(['token_read_general' => '','endsession_read_general' => $date]);

        return $res;
      
    }


    public static function validLang(){
       $datas =  Db::table('olmo_language')->where('enabled_is_general','true')->select('code_txt_general')->get()->toArray();
       $res = [];
       foreach($datas as $d){
            $res['valid_lang'][$d->code_txt_general] = $d->code_txt_general;
       }
       $res['default_lang'] = @Db::table('olmo_language')->where('default_is_general','true')->select('code_txt_general')->first()->code_txt_general;
       return $res;
    }



    public static function getAnalytics(){

            return @Db::table('olmo_analytics')->where('id',1)->select(
                'gtmheader_txt_analytics as gtm_header',
                'gtmbody_txt_analytics as gtm_body',
                'gtmheaderprod_txt_analytics as gtm_header_prod',
                'gtmbodyprod_txt_analytics as gtm_body_prod'
                )->first();

    }


    public static function getSinglePathStorageById($id){

        $categories_files = [
            'jpg'  => 'images',
            'jpeg' => 'images',
            'png'  =>  'images',
            'pdf'  => 'documents',
            'gif'  => 'images',
            'webm' => 'images',
            'txt'  => 'documents',
            'mp4'  => 'media'
          ];

        $im = DB::table('olmo_storage')->where('id',$id)->first();
        if($im){
             return '/'.$categories_files[$im->type].'/'.$im->model.'/'.$im->truename;
        }else{
         return '';
        }
    }

    public static function getMultiplePathStorageById($id){

        $categories_files = [
            'jpg'  => 'images',
            'jpeg' => 'images',
            'png'  =>  'images',
            'pdf'  => 'documents',
            'gif'  => 'images',
            'webm' => 'images',
            'txt'  => 'documents',
            'mp4'  => 'media'
          ];

        $arrayId = explode(',', $id);
        $arrayImages = [];
        $dbImage = DB::table('olmo_storage')->whereIn('id',$arrayId)->get()->toArray();
        foreach($dbImage as $image){
            $structureImg = '/'.$categories_files[$image->type].'/'.$image->model.'/'.$image->truename;
            array_push($arrayImages, $structureImg);
        }
        if($arrayImages){
            return $arrayImages;
        }else{
         return '';
        }
    }    

    public static function getSinglePathFmanagerById($id){

        $categories_files = [
            'jpg'  => 'images',
            'jpeg' => 'images',
            'png'  =>  'images',
            'pdf'  => 'documents',
            'gif'  => 'images',
            'webm' => 'images',
            'txt'  => 'documents',
            'mp4'  => 'media'
          ];

        $im = DB::table('olmo_storage')->where('id',$id)->first();
        if($im){
             return '/media/'.$im->truename;
        }else{
         return '';
        }
    }    

    public static function getSingleSlide($s){
        $slide = DB::table('olmo_slideritem')->where('id',$s)->select('id','name_txt_content as name','primary_filemanager_content as primary')->first();
        $res = [];
        if($s){
            $slide->primary = self::getSinglePathFmanagerById($slide->primary);
            return $slide;
        }else{
         return '';
        }
    }
    public static function getSingleFullSlide($s){
        $slide = DB::table('olmo_slideritem')->where('id',$s)->first();
        $res = [];
        if($s){
            $slide->primary_filemanager_content = self::getSinglePathFmanagerById($slide->primary_filemanager_content);
            $slide->secondary_filemanager_content = self::getSinglePathFmanagerById($slide->secondary_filemanager_content);
            return $slide;
        }
        return null;
    }

    public static function getSingleProperty($p){
        $property = DB::table('olmo_propertyitem')->where('id',$p)->select('id','name_txt_general as name','image_img_general as image')->first();
        $res = [];
        if($p){
            $property->image = self::getSinglePathStorageById($property->image);
            return $property;
        }else{
         return '';
        }
    }


    public static function getDefaultId($table,$id_single){
        $e =  DB::table($table)->where('id',$id_single)->first();
        $default_id = @$e->parentid_hidden_general;
        if($default_id == '' || $default_id == null){
            $default_id = $id_single;
        }
        return $default_id;
    }

    public static function getValuesOrder($field,$id,$table,$ids){

        $table_rel = 'olmo_'.explode('_',$field)[0];
        $campo = explode('_',$table)[1].'_multid_general';
        $res = [];
        
        $master_id =  $id;

        $items = Db::table($table_rel)->where('enabled_is_general','true')->select('id','name_txt_general',$campo)->get();
        foreach($items as $i){
            if(strpos($i->{$campo},(string)$id)    !== false) { 
                 $res[$i->id]    = ['id' => (string)$i->id,'label' => $i->name_txt_general];
            }
        }
        
        //Se sono stati ordinati ( salvati nel database gli ordino i base l'array)
        $temp = [];
        if($ids != ''){
            $ids = explode(',',$ids);
           foreach($ids as $id){
               if(isset($res[$id])){
                 $temp[] = $res[$id];
                 unset($res[$id]);
               }
           }
        }
        $res = array_merge($temp,$res);
   

        //category_multid_general
        $result   = [];
        $field_rel =  explode('_',$table)[1].'_multid_general';
       
        foreach($res as $item){

            $id    = $item['id'];
            $cat =  Db::table($table_rel)->where('id',$id)->first()->{$field_rel};
            $cat = explode(',',$cat);
            if(in_array($master_id,$cat)){
                $result[]    = ['id' => (string)$id,'label' => $item['label']];
            }

        
        }

        return $result;
    }

    public static function getLangsBymodelId($table,$id_single){

       $langs         = self::validLang()['valid_lang'];
       $default_lang  = self::validLang()['default_lang'];
       $res   = [];
       $i     = 0;

       $default_id  = self::getDefaultId($table,$id_single);

       foreach($langs as $ln){ 

            $exist = false;
            $id    = 0;
            $check =  Db::table($table)
            ->where('parentid_hidden_general',$default_id)
            ->where('locale_hidden_general',$ln)
            ->first();

            $exist = true;
            $id    = @$check->id;
            
            if($default_lang == $ln){
                $exist = true;
                $id    = @$default_id;
            }
            if($id == ''){
                $exist = false;
                $id    = @$default_id; 
            }
            $res[$i]['code']  = $ln;
            $res[$i]['exist'] = $exist;
            $res[$i]['id']    = (string)$id;
            $i++;

       }
       return $res;
    }

    public static function getNextPrevPost($table, $position, $type){     

        $getAllquery = Db::table('olmo_'.$table)->get()->toArray();
        
        usort($getAllquery, function($a,$b){ return ($a->position_ord_general < $b->position_ord_general) ? -1 : 1; });

        $id = 0;

        foreach($getAllquery as $index=>$post){
            if($post->id == $position){
                if($type == 'next'){
                    $id = $index + 2;
                } else if($type == 'prev'){
                    $id = $index;
                }
            }
        }

        $query = Db::table('olmo_'.$table)->where('id', $id)->first();
        if(!$query){
            $id = $type == 'next' ? $getAllquery[0]->id : $getAllquery[count($getAllquery)-1]->id;
            $query = Db::table('olmo_'.$table)->where('id', $id)->first();
        }
        $normalize = ApiHelper::cleanKeySimple($query);

        return $id;

    }

}