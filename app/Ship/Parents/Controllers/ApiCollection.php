<?php

namespace App\Ship\Parents\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class ApiCollection
{

    public static  function get($type,$value = 0){

        $res = [];


        $collection['customerprop'] = [
            [ 'key' => '', 'value' => ''],
            [ 'key' => 'vat', 'value' => 'vat']
        ];

        $collection['customercondition'] = [
            [ 'key' => '', 'value' => ''],
            [ 'key' => '=', 'value' => '='],
            [ 'key' => '=', 'value' => '!='],
            [ 'key' => '>', 'value' => '>'],
            [ 'key' => '<', 'value' => '<'],
            [ 'key' => '>=', 'value' => '>='],
            [ 'key' => '<=', 'value' => '<=']
        ];

        $collection['ordercondition'] = [
            [ 'key' => '', 'value' => ''],
            [ 'key' => '=', 'value' => '='],
            [ 'key' => '=', 'value' => '!='],
            [ 'key' => '>', 'value' => '>'],
            [ 'key' => '<', 'value' => '<'],
            [ 'key' => '>=', 'value' => '>='],
            [ 'key' => '<=', 'value' => '<=']
        ];


        $collection['ordernumber'] = [
            [ 'key' => '', 'value' => '']
        ];


        $collection['model'] = [
            [ 'key' => '',          'value' => ''],
            [ 'key' => 'page',      'value' => 'page'],
            [ 'key' => 'product',   'value' => 'product'],
            [ 'key' => 'category',  'value' => 'category'],
            [ 'key' => 'blog',      'value' => 'blog'],
            [ 'key' => 'service',   'value' => 'service'],
            [ 'key' => 'casehistory',   'value' => 'casehistory'],
            [ 'key' => 'storelocator',  'value' => 'storelocator']
        ];


        $collection['index'] = [
            [ 'key' => '', 'value' => ''],
            [ 'key' => 'index', 'value' => 'index'],
            [ 'key' => 'noindex', 'value' => 'noindex'],
        ];

        $collection['follow'] = [
            [ 'key' => '', 'value' => ''],
            [ 'key' => 'follow', 'value' => 'follow'],
            [ 'key' => 'nofollow', 'value' => 'nofollow']
        ];

        $collection['frequency'] = [
            [ 'key' => '',        'value' => ''],
            [ 'key' => 'always',  'value' => 'always'],
            [ 'key' => 'hourly',  'value' => 'hourly'],
            [ 'key' => 'daily',   'value' => 'daily'],
            [ 'key' => 'weekly',  'value' => 'weekly'],
            [ 'key' => 'monthly', 'value' => 'monthly'],
            [ 'key' => 'never',   'value' => 'never']
        ];  

        $collection['priority'] = [
            [ 'key' => '', 'value' => ''],
            [ 'key' => '0', 'value' => '0'],
            [ 'key' => '0.1', 'value' => '0.1'],
            [ 'key' => '0.2', 'value' => '0.2'],
            [ 'key' => '0.3', 'value' => '0.3'],
            [ 'key' => '0.4', 'value' => '0.4'],
            [ 'key' => '0.5', 'value' => '0.5'],
            [ 'key' => '0.6', 'value' => '0.6'],
            [ 'key' => '0.7', 'value' => '0.7'],
            [ 'key' => '0.8', 'value' => '0.8'],
            [ 'key' => '0.9', 'value' => '0.9'],
            [ 'key' => '1', 'value' => '1'],
        ];

        $collection['formtype'] = [
            [ 'key' => '', 'value' => ''],
            [ 'key' => 'login', 'value' => 'login'],
            [ 'key' => 'register', 'value' => 'register'],
            [ 'key' => 'resetpassword', 'value' => 'resetpassword'],
            [ 'key' => 'neworder', 'value' => 'neworder'],
            [ 'key' => 'orderconfirmed', 'value' => 'orderconfirmed'],
            [ 'key' => 'ordershipped', 'value' => 'ordershipped'],
            [ 'key' => 'orderrefunded', 'value' => 'orderrefunded'],
            [ 'key' => 'customer creation', 'value' => 'customercreation'],
            [ 'key' => 'customer not active', 'value' => 'customernotactive'],
            [ 'key' => 'customer active', 'value' => 'customeractive'],
            [ 'key' => 'customer disabled', 'value' => 'customerdisabled'],
            [ 'key' => 'customer blocked', 'value' => 'customerblocked'],
            [ 'key' => 'customer password recovery', 'value' => 'customerpasswordrecovery'],
            [ 'key' => 'customer password changed', 'value' => 'customerpasswordchanged']

        ]; 

        $collection['typeaddress'] = [
            [ 'key' => '', 'value' => ''],
            [ 'key' => 'shipping', 'value' => 'shipping'],
            [ 'key' => 'billing', 'value' => 'billing']
        ]; 
        
        $collection['customerstatus'] = [
            [ 'key' => 'Not active', 'value' => 'notactive'],
            [ 'key' => 'Active', 'value' => 'active'],
            [ 'key' => 'Disabled', 'value' => 'disabled'],
            [ 'key' => 'Blocked', 'value' => 'blocked']
        ]; 
        
        $collection['orderstatus'] = [
            [ 'key' => 'Pending', 'value' => 'pending'],
            [ 'key' => 'Paid', 'value' => 'paid'],
            [ 'key' => 'Shipped', 'value' => 'shipped'],
            [ 'key' => 'Completed', 'value' => 'completed'],
            [ 'key' => 'Canceled', 'value' => 'canceled'],
            [ 'key' => 'Refunded', 'value' => 'refunded']
        ];         

        if(isset($collection[$type])){
            $res = $collection[$type];
        }



        return $res;
    }

    public static  function currentValue($collection,$value){
        $res = $collection;
        foreach($res as $r){
           if($r['value'] == $value){
                return  $r;
                break;
            }
        }
    }

    public static  function visualComponent(){
        $res = []; 
        $res['blocks'][0]['id'] = '0';
        $res['blocks'][0]['icon'] = 'fa-user';
        $res['blocks'][0]['name'] = 'hero';
     
        $res['blocks'][1]['id'] = '1';
        $res['blocks'][1]['icon'] = 'fa-setting';
        $res['blocks'][1]['name'] = 'heading';

        return $res;
    }












}
