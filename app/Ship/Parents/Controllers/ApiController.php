<?php

namespace App\Ship\Parents\Controllers;

use Apiato\Core\Abstracts\Controllers\ApiController as AbstractApiController;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Ship\Parents\Controllers\ApiHelper;
use App\Ship\Parents\Controllers\ApiCollection;
use App\Ship\Parents\Controllers\Frontend\ApiCustomer;
use App\Ship\Parents\Controllers\Frontend\ApiRoute;
use App\Ship\Parents\Controllers\ApiPermissions;
use Http;

use App\Containers\Backoffice\Page\UI\API\Requests\GetAllPagesRequest;

abstract class ApiController extends AbstractApiController
{


    public function setLabel($label){
        $parts = explode('_',$label);
        if(isset($parts[0])){
            $label = $parts[0];
        }
        return $label;
    }

    public function setTab($label){
        $tab = 'general';
        $parts = explode('_',$label);
        if(isset($parts[2])){
            $tab = $parts[2];
        }
        return $tab;
    }

	public  function getType($field,$table){


        $type = 'hidden';

        if(strpos($field,'_id_')){$type      = 'select';}
        if(strpos($field,'_multid_')){$type  = 'multiselect';}
        if(strpos($field,'_select_')     !== false) { $type = 'select';}
        if(strpos($field,'_email_')      !== false) { $type = 'email';}
        if(strpos($field,'_media_')      !== false) { $type = 'media';}
        if(strpos($field,'_multimg_')    !== false) { $type = 'multiimage';}
        if(strpos($field,'_img_')        !== false) { $type = 'singleimage';}
        if(strpos($field,'_is_')         !== false) { $type = 'checkbox';}
        if(strpos($field,'_editor_')     !== false) { $type = 'texteditor';}
        if(strpos($field,'_txt_')        !== false) { $type = 'text';}
        if(strpos($field,'_txtarea_')    !== false) { $type = 'textarea';}
        if(strpos($field,'_num_')        !== false) { $type = 'number';}
        if(strpos($field,'_date_')       !== false) { $type = 'date';}
        if(strpos($field,'_hidden_')     !== false) { $type = 'hidden';}
        if(strpos($field,'_list_')       !== false) { $type = 'list';}
        if(strpos($field,'_ord_')        !== false) { $type = 'number';}
        if(strpos($field,'_div_')        !== false) { $type = 'divider';}
        if(strpos($field,'_down_')       !== false) { $type = 'downloader';}
        if(strpos($field,'_spin_')       !== false) { $type = 'spinner';}
        if(strpos($field,'_read_')       !== false) { $type = 'readonly';}
        if(strpos($field,'_color_')      !== false) { $type = 'colorpicker';}
        if(strpos($field,'_dnd_')        !== false) { $type = 'order';}
        if(strpos($field,'_spin_')       !== false) { $type = 'spinner';}
        if(strpos($field,'_pwd_')        !== false) { $type = 'password';}
        if(strpos($field,'_slider_')     !== false) { $type = 'slider';}
        if(strpos($field,'_visual_')     !== false) { $type = 'visual';}
        if(strpos($field,'_label_')      !== false) { $type = 'label';}
        if(strpos($field,'_props_')      !== false) { $type = 'props';}
        if(strpos($field,'_filemanager_') !== false) { $type = 'filemanager';}
        if(strpos($field,'copy')         !== false) { $type = 'copy';}

        return (string)$type;


    }



    public function emptyItem($attribute,$lang = ''){
            $i      =  0;
            $items  =  [];
            $res    =  [];
            $columns = Db::select('SHOW COLUMNS FROM '.$attribute['table']);
            $columns = json_decode(json_encode($columns), true);
            $model   = explode('_',@$attribute['table'])[1];
            
        

           

            if(isset($_GET['parentid']))
                array_unshift($columns,[ 'Field' => 'copytranslate']);

            // controllo se devo recuperare  il post di defualt
            $copyid = 0;
            if(isset($_GET['copytranslate'])){
                $copyid       = $_GET['copytranslate'];
                $model        = explode('/',$_SERVER['REQUEST_URI'])[3];
                $temp         = [];
                $url          = env('API_URL')."/backoffice/en/".$model."/".$_GET['copytranslate'];
                $token        = ApiHelper::getXtoken();
                $response     = Http::withHeaders([ 'x-token' => $token])->get($url);       
                $itemsdefault = json_decode($response->body());
                foreach($itemsdefault->items as $t){
                    $temp[@$t->name] = @$t->value;
                }
            }
                
            foreach ($columns as $c) {
            $item = [];
            $k                   = $c['Field'];
            $item['name']        = (string)$k;           
            $item['value']       = ApiHelper::getDefaultValue(@$attribute['default'],$k);
            $label               = $this->setLabel((string)$k);
            $type                = $this->getType((string)$k,$attribute['table']);
            $item['tab']         = $this->setTab((string)$k);
            $item['required']    = ApiHelper::isRequired(@$attribute['required'],$k);
            $item['label']       = $label;

             if(isset($_GET['copytranslate'])){
                $item['value']  = $temp[(string)$k];
            }

            if( ($k == 'parentid_hidden_general' && isset($_GET['copytranslate'])) && ($type != 'readonly')   ){
                $item['value'] = $_GET['copytranslate'];
            }

            if($k == 'copytranslate'){
                $item['value'] = $_GET['parentid'];
                $item['label'] = "Copy content post";
            }

            if($k == 'id'){
                $type = 'hidden';
                $item['value']  = '0';
            }

            if(strpos($k,'_select')    !== false) 
            { 
                $collection     = explode('_',$k)[0];
                $item['values'] = ApiCollection::get($collection);
                $item['value']  = ApiCollection::currentValue($item['values'],'');
            }else{

                if($type == 'select' || $type == 'multiselect')
                {
                    // capisco che valori prendre dal nome del field
                    $field          = explode('_',$k);
                    $table_relation = env('TABLE_PREFIX').'_'.$field[0];
                    $item['values'] = $this->listRelation($table_relation,@$lang,$model);
                    $item['value']  = $this->currentRelation($table_relation,'',$type,false,$copyid,$attribute['table'],$k);
                }
            }

            if($type == 'multiimage'){
                $item['values'] = [];
                $item['value']  = [];
            }

            if($type == 'number'){
                $item['value'] = "0";
            }

            if($type == 'order'){
                $item['value'] = [];
            }

            if(isset($_GET[$label])){
                $item['value'] = $_GET[$label];
            }

            if($k == 'lang_langs_general'){
                $item['value']      = ApiHelper::getLangsBymodelId($attribute['table'],0);
                $item['label']      = 'locale';
                $type               = 'hidden';
            }

            $item['type']        = $type;

            if($k == 'locale_hidden_general'){
                $item['value']      = $lang;
            }

            if($type == 'list'){
                $item['value'] = "value";
                $item['properties'] = [];
            }            

            if($type == 'props')
            {
                $field          = explode('_',$k);
                $table_relation = env('TABLE_PREFIX').'_'.$field[0];
                $item['values'] = $this->listRelationProps();
                $item['value']  = [];
                $item['type']   = 'multiselect';
            }            
           
            if($type != 'list')
                $items[$i][] = $item;
            }

           

            
        
            $res['items']        =  $items[0];
            $res['pagename']     =  $model;
            $res['title']        =  '';
            $res['url']          =  '';
            $res['id']           =  (int)0;

            return $res;
    }

    public  function getItem($datas,$attribute){
        $items  = [];
        $i      = 0;
        foreach($datas as $dato){
          foreach ($dato as $k => $v) {
            $item = [];
            $item['id']     = (string)$k;
            $item['value']  = (string)$v;
            $item['label']  = (string)$k;
            $item['type']   = $this->getType($k,$attribute['table']);

            $item['type']   = @$type;
            $items[$i][]    = $item;
          }
          $i++;
        }
        return $items;
    }

    public  function listRelation($table,$lang,$model){

        $res = Db::select("SHOW COLUMNS FROM $table LIKE 'locale_hidden_general'");
        $res = count(json_decode(json_encode($res), true));
        if($res == 0)
        {
             $lang = '';
        }

        if($lang == '')
             $datas  = Db::table($table)->get();
        else
            $datas  = Db::table($table)->where('locale_hidden_general',$lang)->get();
    

        if($table == 'olmo_template'){
            $datas  = Db::table($table)->where('locale_hidden_general',$lang)->where('model_select_general',$model)->get();
        }
        $items          = [];
        $i              = 0;
        $item           = [];
        $item['key']    = '';
        $item['value']  = '';
        $items[$i]      = $item;
        $i++;
        foreach($datas as $dato){
            $item = [];
            $item['key']    = @$dato->name_txt_general ?? @$dato->username_txt_general ?? @$dato->email_email_general;
            $item['value']  = (string)@$dato->id;
            $items[$i]      = $item;
            $i++;
        }
        return $items;
    }



    public  function listRelationProps(){

        $datas          = DB::table('olmo_property')->where('enabled_is_general','true')->get();
        $items          = [];
        $i              = 0;
        $item           = [];
        $item['key']    = '';
        $item['value']  = '';
        $items[$i]      = $item;
        $i++;
        foreach($datas as $dato){

            $propertyname   = $dato->name_txt_general;
            $values         = explode(',', $dato->property_list_general);
            $dataValues     = DB::table('olmo_propertyitem')->whereIn('id',$values)->where('enabled_is_general','true')->get();

            foreach($dataValues as $v){
                $item           = [];
                $item['key']    = $propertyname.' - '.$v->name_txt_general;
                $item['value']  = (string)@$v->id;
                $items[$i]      = $item;
                $i++;
            }
        }
        return $items;
    }



    public  function currentRelation($table,$values,$type = '',$front = false,$copyid = 0,$model= '',$field= ''){



    
        $items  = [];
        $i      = 0;

        if($copyid != 0){
            $values = Db::table($model)->where('id',$copyid)->first()->{$field};
          
        }

        if($values == '')
        {
            if($type == 'multiselect'){
                $items = [];

            }else{
                $item['key']    = '';
                $item['value']  = '';
                $items          = $item;
            }
        }
        else
        {
            $ids = explode(',',$values);
            //Recupero gli id in lingua
            if($copyid != 0)
            {
                $temp = [];
                foreach ($ids as $id) { 
                    $row = Db::table($table)->where('parentid_hidden_general',$id)->first();
                    if($row)
                        $temp[] =  $row->id;
                }
                $ids = $temp;
             
            }

          


            $datas  = Db::table($table)->whereIn('id',$ids)->get();
            
            if($front == true){
                $items = $datas;
            }else{
                foreach($datas as $dato){
                    $item = [];
                    $item['key']    =  @$dato->name_txt_general ?? @$dato->username_txt_general ?? @$dato->email_email_general;
                    $item['value']  = (string)@$dato->id;
                    $items[$i]      = $item;
                    $i++;
                }
            }

            if($type == 'select'){
                $items = @$items[0];
            }
        }
      
        return $items;
        
    }

    public  function currentRelationProps($values = ''){

        $items  = [];
        $i      = 0;
        if($values == '')
        {
            $items          = [];
        }
        else
        {
            $values         = explode(',', $values);
            $datas          = DB::table('olmo_property')->where('enabled_is_general','true')->get();
            $items          = [];
            $i              = 0;
            foreach($datas as $dato){

                $propertyname   = $dato->name_txt_general;
                $valuesTemp     = explode(',', $dato->property_list_general);
                $dataValues     = DB::table('olmo_propertyitem')->whereIn('id',$valuesTemp)->where('enabled_is_general','true')->get();

                foreach($dataValues as $v){
                    $item           = [];
                    $item['key']    = $propertyname.' - '.$v->name_txt_general;
                    $item['value']  = (string)@$v->id;
                    if(in_array($v->id,$values)){
                        $items[$i]      = $item;
                        $i++;
                    }
                }
            }
        }
      
        return $items;
        
    }


	public function getList($datas,$attribute){

		$items  = [];
        $i      = 0;
        $res['route']  = $attribute['table'];
        foreach($datas as $dato){

          foreach ($dato as $k => $v) {
            $item = [];
      
            $id             = (string)$k;
            $parts          = explode('.',$id);
            $item['id']     = $id;
            $item['label']  = (string)$v;
            $label          = $this->setLabel((string)$k);
            $type           = $this->getType((string)$k,$attribute['table']);
            $item['label']  = $label;

            if($k == 'id'){
                $type = 'hidden';
            }

            if($type == 'checkbox'){
                $type = 'label';
            }

            if($type == 'singleimage'){
                $type = 'image';
            }

            $item['value'] = $v;
            if($type == 'select' || $type == 'multiselect')
            {
                // capisco che valori prendre dal nome del field
                $field          = explode('_',$k);
                $table_relation = env('TABLE_PREFIX').'_'.$field[0];
                $item['value']  = $this->currentRelation($table_relation,$v,$type)['key'];
            }

            if($type == 'select' || $type == 'multiselect'){
                $type = 'text';
            }

            if($k == 'lang_langs_general'){
                $item['value']      = ApiHelper::getLangsBymodelId($attribute['table'],$dato['id']);
                $item['label']      = 'locale';
                $type               = 'locale';
            }

            $item['type']    = $type;

            if($type == 'image'){
                $item['value'] = ApiHelper::getSinglePathStorageById($v);
            }          

            if(@$parts[0] == 'extrafield')
            {
                $table          = 'olmo_'.$parts[1];
                $field_rel      = $parts[2];
                $value          = Db::table($table)->where('id',$v)->first();
                $label          = explode('.',@$label);
                $item['value']  = @$value->{$field_rel};
                $item['label']  = ucfirst(@$label[1].' '.@$label[2]);
                $item['type']   = 'text';
            }

            $items[$i][] =   $item; 
       
          }
          $i++;
        }
        $res['items']        =  $items;
        $res['page']         =  1;
        $res['pagesize']     =  count($items);
        $res['totalpages']   = 1;
        $res['totalentries'] = 1;
        return $res;
	}

    public function getSingleList($datas,$attribute,$id_single,$lang = ''){

        $items  = [];
        $i      = 0;
        $model = explode('_',$attribute['table'])[1];
        
        $datas = ApiPermissions::userPermissionFields($datas);

        foreach($datas as $dato){

          foreach ($dato as $k => $v) {
            $item = [];

            if($k == 'paymentmethod_id_general')   
                $k = 'paymentmethod_txt_general';

            if($k == 'shippingmethod_id_general')
                $k ='shippingmethod_txt_general';

            $item['name']   = (string)$k;
            $item['value']  = (string)$v;
            $label = $this->setLabel((string)$k);
            $item['label']  = $label;
            
            $type           = $this->getType((string)$k,$attribute['table']);
            $item['tab']    = $this->setTab((string)$k);
            if($k == 'id'){
                $type = 'hidden';
            }
            $item['type']        = $type;

            if(isset($attribute['requiredbackoffice'])){
                $attribute['required'] = $attribute['requiredbackoffice'];
            }                        
            $item['required']    = ApiHelper::isRequired(@$attribute['required'],$k);

            if(strpos($k,'_select')    !== false) 
            { 
                $collection     = explode('_',$k)[0];
                $item['values'] = ApiCollection::get($collection);
                $item['value']  = ApiCollection::currentValue($item['values'],$v);

            }else{

                if($type == 'select' || $type == 'multiselect')
                {
                    // capisco che valori prendre dal nome del field
                    $field          = explode('_',$k);
                    $table_relation = env('TABLE_PREFIX').'_'.$field[0];
                    $item['values'] = $this->listRelation($table_relation,@$lang,$model);
                    $item['value']  = $this->currentRelation($table_relation,$v,$type);
                }
            }

            if($type == 'multiimage'){
                if($v == ''){
                    $item['value'] = [];
                    $item['path'] = [];
                }
                else{
                    $images = explode(',',$v);
                    $item['value'] = $images;
                    foreach($images as $im){
                        $item['path'][] = ApiHelper::getSinglePathStorageById($im);
                    }
                }
            }

            //Slider
            if($type == 'slider'){
                if($v == ''){
                    $item['value'] = [];
                    $item['slides'] = [];
                }
                else{
                    $slider = explode(',',$v);
                    $item['value'] = $slider;
                    foreach($slider as $s){
                        $item['slides'][] = ApiHelper::getSingleSlide($s);
                    }
                }
            }

            if($type == 'list'){
                 $item['value'] = 'value';
                 if($label == 'address'){
                    $item['value'] = 'address'; 
                 }
            }
            
            if($type == 'password'){
                $item['value'] = '';
            }

            if($type == 'filemanager'){
                $item['value'] =  @Db::table('olmo_storage')->where('id',$v)->first()->truename;                
            }
           

            if($type == 'singleimage' || $type == 'media'){
                $item['path'] = ApiHelper::getSinglePathStorageById($v);
            }

            if($k == 'lang_langs_general'){
                $langs           = ApiHelper::getLangsBymodelId($attribute['table'],$id_single);
                $item['value']   = $langs;
                $item['type']    = 'locale';
            }

            if($type == 'order'){
                $item['value']   = ApiHelper::getValuesOrder($k,$dato['id'],$attribute['table'],$v);
            }


            if($type == 'visual'){
                if($v == '')
                    $item['value']  = [];
                else
                    $item['value']  = json_decode($v);

            }

            if($label == 'orderitems'){

                $item['type']        = $label;
                $item['tab']         = "Orders items";
                if($v == '')
                     $item['value']  = [];
                else{
                    $orders                  = json_decode($v);
                    $temp = [];
                    foreach($orders as $o){
                        $a = [];
                        $a['sku']           = $o->sku;
                        $a['name']          = ApiRoute::transField($o->id,$lang,'product','title_txt_content');
                        $a['image']         = $o->image1;
                        $a['price']         = $o->totalprice;
                        $a['productid']     = $o->id;
                        $a['price per item (without discount)']   = $o->singileprice;
                        $a['quantity']      = $o->quantity;
                        $a['subtotal (without discount)']         = $o->totalprice;
                        $temp[] = $a; 
                    }


                    $item['value']      = $temp;

                }
                   
            }

            if($type == 'props')
            {
                $field          = explode('_',$k);
                $table_relation = env('TABLE_PREFIX').'_'.$field[0];
                $item['values'] = $this->listRelationProps();
                $item['value']  = $this->currentRelationProps($v);
                $item['type']   = 'multiselect';
            }              

            $items[$i][] = $item;
          }

          $i++;
        }
        
        
        $title               = @$datas[0]->name_txt_general;
        $model               =  @explode('_',$attribute['table'])[1];
        $res['items']        =  $items[0];
        $res['pagename']     =  explode('_',@$attribute['table'])[1];
        $res['title']        =  $title;
        $res['url']          =  ApiHelper::getSlugFront($model,$lang,$id_single);
        $res['id']           =  (int)$id_single;
        return $res;
    }


    public function isJson($string) {
       json_decode($string);
       return json_last_error() === JSON_ERROR_NONE;
    }


    public function createUpdate($request,$attribute,$frontResponse = false){

        $user  = ApiHelper::getUser();
        $id    = $request->id;
        $lang  = $request->lang;

        if($id == ''){
            $id = 1;
        }

        if($id == 'empty'){
            $id = 0;
        }

        $body  = file_get_contents('php://input');
        $check = $this->isJson($body);
        if(!$check){
            return response('Body not a JSON', 400);
            exit();
        }

        $body  = json_decode($body, true);
        unset($body['id']);
        unset($body['confirmpassword']);
        unset($body['_default']);
        unset($body['current_timezone']);
        unset($body['activate_url']);
       


        if(isset($body['current_locale']))
        {
            $body['customerlang_txt_general'] = $body['current_locale'];
            unset($body['current_locale']);
        }

        
        if(isset($body['_type']))
        {
            $body['typeaddress_select_general'] = $body['_type'][0];
            unset($body['_type']);
        }

        


        if(@$request->enabled_is_general != '')
             $body['enabled_is_general'] = $request->enabled_is_general;
      
        if(@$request->customer_hidden_general != '')
            $body['customer_hidden_general'] = $request->customer_hidden_general;

        if(@$request->default_is_general != '')
            $body['default_is_general'] = $request->default_is_general;

        if(@$request->tokenactive_read_general != '')
            $body['tokenactive_read_general'] = $request->tokenactive_read_general;

        if(@$request->token_read_general != '')
            $body['token_read_general'] = $request->token_read_general;

        if(@$request->customerstatus_select_general != '')
            $body['customerstatus_select_general'] = $request->customerstatus_select_general;

            
        if(isset($body['customerstatus_select_general']))
        {
            $locale    = $body['customerlang_txt_general'];
            $status    = $body['customerstatus_select_general']['value'];

            $prestatus = Db::table('olmo_customer')->where('id',$id)->first()->customerstatus_select_general;
            
            if($prestatus != $status)
                ApiCustomer::ChangeStatus($id,$status,$status,$locale,true);
                // ApiCustomer::ChangeStatus($user_id,$status,$emailtemplate,$locale);
        }
            

            


        //Normalizzo il body
        $params = [];
        foreach ($body as $key => $value) {
            if(is_array($value)){
            //conrolla se esiste la chiave key
                if(isset($value['value'])){
                    $params[$key] = @$value['value'];
                } else {
                    $rel = '';
                    foreach ($value as $v) {
                        $rel .= @$v['value'].',';
                    }
                    $params[$key] = rtrim($rel,',');
                }
            } else {
                if(strpos($key,'password') !== false) { 
                    if($value != '') {
                        $params[$key] = $value;
                    }                        
                } else if(strpos($key,'_is') !== false) {
                    // $params[$key] = $value;
                    if($value == 1 || $value == "true" || $value == "1"){
                        $params[$key] = "true";
                    } else {
                        $params[$key] = "false";
                    }
                } else if(strpos($key,'_filemanager') !== false) {
                    $params[$key] = $value ? Db::table('olmo_storage')->where('truename',$value)->first()->id : "";  
                } 
                
                else {
                  if($value == ''){
                    $value = "";
                  }
                  $params[$key] = $value;
                }
            }
        }


        $isLang = false;
        if(isset($attribute['lang'])){
            $isLang = $attribute['lang'];
            if($isLang == 'true'){
                $isLang = true;
            }
        }

        if($isLang){
            $params['locale_hidden_general'] = $lang;
        }
  
       //Normalizzazione
        if(isset($params['category_id_general'])){
            if($params['category_id_general'] == ''){
                $params['category_id_general'] = 0;
            }
        }

        if($id == 0){
         
            //controllo che il parentid sia diverso da 0 se la lingua NON è quella di default
            $default_lang = ApiHelper::validLang()['default_lang'];
            if($default_lang == ''){
                 return response('default lang', 400);
                 exit();
            }

            if(@$attribute['lang'] == 'true'){
                if($default_lang != $lang){
                    $check  = 0;
                    $check2 = 0;
                    $parentid = @$params['parentid_hidden_general'];
                    if((int)$parentid > 0){
                        $check =  Db::table($attribute['table'])
                        ->where('id',$parentid)
                        ->get()
                        ->count();
                    }
                
                    $check2 = Db::table($attribute['table'])
                                ->where('parentid_hidden_general',$parentid)
                                ->where('locale_hidden_general',$lang)
                                ->get()
                                ->count();


                    if($check == 0){
                        return response('parentid not valid check: '.$check, 400);
                        exit();
                    }
                    if($check2 > 0){
                        return response('parentid not duplicate max 1', 400);
                        exit();
                    }
                }

            }

            try {
               $item_id = Db::table($attribute['table'])->insertGetId($params);
               if($item_id){
                    $datas  = Db::table($attribute['table'])->where('id',$item_id)->get()->toArray();
                    $datas  = json_decode(json_encode($datas), true);

                    if($frontResponse == true){
                        return ApiHelper::FrontResponseChild($datas)[0];
                    }else{
                        return   $this->getSingleList($datas,$attribute,$item_id)['items'];
                    }
                   
               }
            }  catch (\Illuminate\Database\QueryException $e){
                
               if($e->getCode() === '23000') {
                  return response('Duplicate key', 400);
                }
            } catch (PDOException $e) {
               echo 0;
            }   
        } else if($id  > 0) {
            $res    = Db::table($attribute['table'])->where('id',$id)->update($params);
            $datas  = Db::table($attribute['table'])->where('id',$id)->get()->toArray();
            $datas  = json_decode(json_encode($datas), true);
            return  $this->getSingleList($datas,$attribute,$id)['items'];
        }
    }

    /*
     *   Prepare the object
     */
    public static function makeTheParams($request, $locale = null)
    {
        
        $body  = $request->all();
        $params = [];
        foreach ($body as $key => $value) {
            $params[$key] = $value;
        }
        
        return $params;
    }

    /*
     *   Questa l'ho messa perchè non riuscivo a fare funzionare quella sopra
     *   ma possiamo toglierla appena siamo sicuri che funzioni
     */
    public static function update($request, $attribute)
    {
        $id = $request->id;
        $params = self::makeTheParams($request);

        $response = Db::table($attribute['table'])->where('id',$id)->update($params);
        $data  = Db::table($attribute['table'])->where('id',$id)->first();

        return ApiHelper::cleanKeySimple($data);

    }  
    /*
     *   Questa l'ho messa perchè non riuscivo a fare funzionare quella sopra
     *   ma possiamo toglierla appena siamo sicuri che funzioni
     */
    public static function add($request, $attribute)
    {
        $id = $request->id;
        $params = self::makeTheParams($request);
        
        $response = Db::table($attribute['table'])->insertGetId($params);
        $data  = Db::table($attribute['table'])->where('id',$response)->first();

        return ApiHelper::cleanKeySimple($data);

    } 

    public static function addUser($request, $attribute)
    {
        $id = $request->id;
        $params = self::makeTheParams($request);
        
        $checkEmail  = Db::table($attribute['table'])->where('email_email_general',$params['email_email_general'])->first();

        // return $checkEmail;

        if(!$checkEmail){
            $response = Db::table($attribute['table'])->insertGetId($params);
            $data  = Db::table($attribute['table'])->where('id',$response)->first();
            $response = ApiHelper::cleanKeySimple($data);
            return $response;
        } else {
            return false;
        }

    }            
}