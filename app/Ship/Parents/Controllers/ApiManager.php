<?php

namespace App\Ship\Parents\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Schema;
use \stdClass;
use Illuminate\Support\Facades\Storage;
class ApiManager 
{



    public static function outputCSV($data) {
        $output = '';
        foreach ($data as $row){
           $output .= implode(";",$row).'\r\n';
        }
        return $output;
    }

   public static function export($model,$type){
    $res  = [];
    $data = ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model))::all();
    $data = json_decode(json_encode($data), true);


    $current_data = self::outputCSV($data);

    $data     = date('Y_m_d_h_m_s');
    
    $filename = ucfirst($model).'_export_'.$data.'.csv';

    $path  = 'public/documents/'.ucfirst($model).'/'.$filename;
    $r     = Storage::put($path, $current_data);


    $storage_id = DB::table('olmo_storage')->insertGetId([
        'filename'  => $filename,
        'truename'  => $filename,
        'model'     => ucfirst($model),
        'public'    => 'true',
        'type'      => $type
    ]);

    Db::table('olmo_manager')->insert([
        'store_id'          => $storage_id,
        'creation_date'     => date("Y-m-d H:i:s"),
        'type'              => 'export'
    ]);

    $res['filepath'] = '/path/'.ucfirst($model).'/'.$filename;

    return $res;

   }

}
