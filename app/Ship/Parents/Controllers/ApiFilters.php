<?php

namespace App\Ship\Parents\Controllers;

use Illuminate\Support\Facades\DB;
use App\Ship\Parents\Controllers\ApiHelper;
use Illuminate\Database\QueryException;

class ApiFilters
{

    public static function filter($request)
    {
        
        //SELECT * FROM olmo_product  WHERE CONCAT(',',category_multid_general,',')  REGEXP ',1'

        $model    = $request->model;
        $values    = $request->values;
        $category  = @$request->category;
        $filter  = '';
        foreach($values as $value){
           

            $regex   = "'";
            foreach($value as $v){
                $regex .= ','.$v.',|';
            }
            $regex = substr($regex, 0, -1)."'";

            $filter .= " CONCAT(',',property_props_general,',') REGEXP  $regex AND";
        }

        $filter = substr($filter, 0, -3);



        if($category != ''){
    
            $type = $category['type'];
            $values    = $category['values'];
            $filter    .=" AND CONCAT(',',".$type.",',') REGEXP ";

            $regex   = "'";
            foreach($values as $v){
                $regex .= ','.$v.',|';
            }
            $regex = substr($regex, 0, -1)."'";

            $filter .= $regex;
           

        }

  

        $sql = "SELECT * FROM olmo_".$model."  WHERE ".$filter;

       
        
        $response =  DB::select($sql);
        
        $response  =  ApiHelper::cleanKeyArray($response);

        //$response['sql'] = $sql;

        return $response;
        

    }

}
