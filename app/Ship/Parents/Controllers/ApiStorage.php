<?php

namespace App\Ship\Parents\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class ApiStorage
{

    public static function getFileManager($request)
    {
        $truename  = $request->any;
        $pathinfo  = pathinfo($truename);
        $dirname   = $pathinfo['dirname'];
        $folder    = 'filemanager';
        $image = Db::table('olmo_storage')->where('truename',$truename)->where('model',$folder)->first();
        
        $valid   = true;
        if($image) {

            if($image->public == "false"){
                if($_REQUEST['olmotoken']){

                  $token = $_REQUEST['olmotoken'];

                  $query = Db::table('olmo_tokens')
                          ->where('updated_at',null)
                          ->where('modelid',$image->truename)
                          ->where('token',$token)
                          ->first();

                  if(!$query){
                    $valid = false;
                  } else {
                    Db::table('olmo_tokens')
                    ->where('updated_at',null)
                    ->where('modelid',$image->truename)
                    ->where('token',$token)
                    ->update([
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                  }
                }
            }

            if(!$valid)
            {
               return response(403);
               exit();
            }

            if($dirname == '')
                $finalpath = $image->filename;
            else
                $finalpath = $dirname.'/'.$image->filename;

            $type    = pathinfo($truename, PATHINFO_EXTENSION);
            $data    = storage_path('app/public/media/'. $finalpath);
            $content = file_get_contents($data);
            $expires = 14 * 60*60*24;
            if($type == 'pdf') {
                return response()->download($data, $image->truename);
                // alternativa per visualizzazione inline:
                // header("Content-type:application/pdf");
                // header("Content-Disposition:inline;filename=".$image->truename);
                // readfile($data);
            } else {
                header("Content-Type: image/".$type);
            }
            header("Content-Length: " . strlen($content));
            header("Cache-Control: public", true);
            header("Pragma: public", true);
            header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT', true);
            echo $content;
            exit();
        }

        return redirect(404);

    }    

    public static function getFile($request){

        $truename  = $request->truename;
        $folder    = $request->folder;

        $image = Db::table('olmo_storage')->where('truename',$truename)->where('model',$folder)->first();

        $categories_files = [
            'jpg'  => 'images',
            'jpeg' => 'images',
            'png'  =>  'images',
            'pdf'  => 'documents',
            'gif'  => 'images',
            'webm' => 'images',
            'txt'  => 'documents',
            'mp4'  => 'media'
        ];   
        
        if($image->public  == 'false'){
            return response(403);
            exit();
        }
        
        if($image) {
            $type = pathinfo($truename, PATHINFO_EXTENSION);
            $data = storage_path('app/public/'.$categories_files[$type].'/'.$image->model.'/'. $image->filename);
            $content = file_get_contents($data);
            $expires = 14 * 60*60*24;
            if($type == 'pdf') {
                return response()->download($data, $image->truename);
                // alternativa per visualizzazione inline:
                // header("Content-type:application/pdf");
                // header("Content-Disposition:inline;filename=".$image->truename);
                // readfile($data);
            } else {
                header("Content-Type: image/".$type);
            }
            header("Content-Length: " . strlen($content));
            header("Cache-Control: public", true);
            header("Pragma: public", true);
            header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT', true);
            echo $content;
            exit();
        }

        return response(404);
    }

    public static function getBarcode($request){
            $code    = $request->code;
            $expires   = 14 * 60*60*24;
            $generator = new BarcodeGeneratorPNG();
            $data      =  $generator->getBarcode($code, $generator::TYPE_CODE_128);
            header("Content-Type: image/png");
            header("Content-Length: " . strlen($data));
            header("Cache-Control: public", true);
            header("Pragma: public", true);
            header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT', true);
            echo $data;
            exit();

    }    

}