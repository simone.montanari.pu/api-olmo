<?php
namespace App\Ship\Parents\Controllers\Frontend;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Ship\Parents\Controllers\ApiCollection;
use App\Ship\Parents\Controllers\ApiHelper;
use App\Ship\Parents\Controllers\ApiStructure;
use App\Containers\Frontend\Cart\Models\Cart;
use stdClass;
use App\Containers\Backoffice\Customer\Models\Customer;
use App\Ship\Parents\Controllers\Frontend\ApiRoute;

class ApiCart
{

    public static function getDiscountGlobal($user){
        $attribute                 = Customer::getAttr();
        $discountglobalfield       = $attribute['cart']['discountglobalfield'];   
        $value                     = @$user->{$discountglobalfield}; 
        $discount                  = Db::table('olmo_discount')->where('enabled_is_general', 'true')->where('customervalue_txt_condition',$value)->first();
        return @$discount->percentage_num_general;
    }

    public static function getCustomerPayment(){
       $customer = ApiHelper::getCustomer();
       $payment  =  DB::table('olmo_paymentmethod')->where('code_txt_general',$customer->paymentmethod_hidden_general)->first();
       return $payment ? ApiHelper::cleanKeySimple($payment) : null;
    }

    public static function getCustomerShipping(){
        $customer = ApiHelper::getCustomer();
        $shipping  =  DB::table('olmo_shippingmethod')->where('code_txt_general',$customer->shippingmethod_hidden_general)->first();
        return $shipping ? ApiHelper::cleanKeySimple($shipping) : null;
    }

    public static function getCustomerAddress(){
        $customer = ApiHelper::getCustomer();
        $address  =  DB::table('olmo_address')->where('code_txt_general',$customer->paymentmethod_hidden_general)->first();
        return $address ? ApiHelper::cleanKeySimple($payment) : null;
     }    

    public static function getCustomerDiscount(){
        $customer = ApiHelper::getCustomer();
        $res        = [];
        $res['percentage'] = self::getDiscountGlobal($customer);
        return $res;
    }

    public static function getCartProperty($customer_id,$lang){
        $carts    = [];
        $items    = Db::table('olmo_cartitem')->where('customer_id_general',$customer_id)->get();

        $i     = 0;
        $gQ    = 0;
        $total = 0;
        foreach ($items as $item) {

            $table          = 'olmo_'.$item->model_select_general;
            $modelStrict    = @Db::table($table)->where('id',$item->model_id_general)->first();
            // $model          = ApiHelper::cleanKeySimple($modelStrict);
            $price                                      = $modelStrict->price_num_general;
            $qty                                        = $item->quantity_num_general;
            $total += ($price * $qty);          
            $carts[$i]['cartitemid']                    = @$item->id;
            $e                                          = ApiRoute::transField($item->model_id_general,$lang,'product');
            $carts[$i]['id']                            = @$item->model_id_general;            
            $carts[$i]['quantity']                      = $qty;
            $carts[$i]['type']                          = $item->model_select_general;
            $carts[$i]['version']                       = null;
            $carts[$i]['num_version']                   = null;
            $carts[$i]['productname']                   = @$e->name_txt_general;
            $carts[$i]['available']                     = null;
            $carts[$i]['code']                          = @$modelStrict->sku_txt_general;
            $carts[$i]['singileprice']                  = $price;
            $carts[$i]['totalprice']                    = number_format($price * $qty, 2, '.', '');

            foreach($modelStrict as $key=>$value){
                $carts[$i][$key] = $e->{$key};
            }

            $gQ +=  $item->quantity_num_general;
            $i++;
        }
        $object = new stdClass();

        $discount    = self::getCustomerDiscount();
        $percentage  = @$discount['percentage'];
        $value       = @$discount->discount;
        $subtotal    = $total;

        if($percentage > 0){
            $total = $total - (($total/100) * $percentage);
        }

        if($value > 0){
            $total = $total - $value;
        }

        $object->items     = ApiHelper::cleanKeyArray($carts);
        $object->qty       = $gQ;
        $object->subtotal  = $subtotal;
        $object->total     = $total;
        $object->total_vat = $total;

        return $object;
    }


    public static function getCartResponse($customer_id,$lang){
        $res  = [];
        $cart = self::getCartProperty($customer_id,$lang);
        $customer = ApiHelper::getCustomer();
    
        $res['currency']['code']   = 'EUR';
        $res['currency']['symbol'] = '€';

        $res['subtotal'] = number_format($cart->subtotal, 2, '.', '');

        $res['addresses'] = [
            "shipping" => @$customer->shippingaddress_hidden_general,
            "billing"  => @$customer->billingaddress_hidden_general
        ];
    
        $res['selectedshippingmethod']  = self::getCustomerShipping();
        $res['selectedpaymentmethod']   = self::getCustomerPayment();        

        $res['metainfo'] = json_decode($customer->meta_hidden_general);

        $res['total']       = number_format($cart->total, 2, '.', '');
        $res['total_vat']   = number_format($cart->total_vat, 2, '.', '');
        $res['taxes']       = "";

        $res['discount']                 = @self::getCustomerDiscount()[0];
        $res['items_quantity']           = $cart->qty;
        $res['items']                    = $cart->items;

        $res['currencies']      = [];
        $res['shippingmethods'] = self::shippingMehtods();
        $res['paymentmethods']  = self::getPaymentMethos();

        return $res;
    }
  
    public static function getPaymentMethos(){
        $payments = Db::table('olmo_paymentmethod')->get();
        return ApiStructure::responseFrontend($payments);
    }

    public static function shippingMehtods(){
        $shipping = Db::table('olmo_shippingmethod')->get();
        return ApiStructure::responseFrontend($shipping);
    }

}