<?php
namespace App\Ship\Parents\Controllers\Frontend;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Ship\Parents\Controllers\ApiCollection;
use App\Ship\Parents\Controllers\ApiHelper;
use App\Containers\Frontend\Cart\Models\Cart;
use App\Containers\Backoffice\Fillform\Models\Fillform;
use Illuminate\Support\Facades\Http;


class ApiCustomer
{
    public static function ChangeStatus($user_id,$status,$template,$locale,$customeremail = false){
        $res = [];
        $res['status'] = $status;
        $res['emailSent'] = false;
        $level_registration = env('CUSTOMER_REGISTRATION_LEVEL');
        $customer = Db::table('olmo_customer')->where('id',$user_id)->update(['customerstatus_select_general' => $status]);
        if($customer){
            $data                         = DB::table('olmo_customer')->where('id',$user_id)->get()->toArray();
            $data                         = ApiHelper::FrontResponseChild($data);
            $formIdRegister               = @DB::table('olmo_fillform')->where('formtype_select_general','customer'.$template)->first()->code_txt_general;
            $res['formIdRegister'] = $formIdRegister;
            if($formIdRegister){
                $token = env('FILLFORM_TOKEN');
                $url = 'https://'.env('FILLFORM_API_URL').$token.'/sendMailV2/'.$formIdRegister;
                $params = json_decode(json_encode($data[0]), true);
                $params['locale'] = $locale;
                if($customeremail){
                    $params['a'] = $params['email'];
                }

                $response = Http::asForm()->post($url, $params);
               
                //Salvo la post
                $res['emailSent']  = $response;
                $res['fillformUrl'] = $url;
                $res['params'] = $params;
            }
            
        }
        return $res;

    }

    

}