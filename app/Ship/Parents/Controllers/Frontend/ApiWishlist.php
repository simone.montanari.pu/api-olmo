<?php
namespace App\Ship\Parents\Controllers\Frontend;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Ship\Parents\Controllers\Frontend\ApiRoute;

class ApiWishlist
{
    public static function getWishlistResponse($customer_id){
        $res      = [];
        $carts    = [];
        $items    = Db::table('olmo_wishlist')->where('customer_id',$customer_id)->get();
        $i        = 0;
        foreach ($items as $item) {
            $table                      = 'olmo_'.$item->model;
            $model                      =  @Db::table($table)->where('id',$item->model_id)->first();
            $carts[$i]['id']            = $model->id;
            $e = ApiRoute::transField($model->id,$lang,'product');
            $carts[$i]['productname']   = $e->name_txt_general;
            $carts[$i]['title']         = $e->title_txt_content;
            $carts[$i]['slug']          = $e->slug_txt_general;
            $carts[$i]['sku']           = @$model->sku_txt_general ?? '';
            $carts[$i]['price']         = @$model->price_num_general ?? '';
            $i++;
        }
        $res = $carts;
        return $res;
    }
}