<?php
namespace App\Ship\Parents\Controllers\Frontend;
use App\Containers\Backoffice\Template\Models\Template;
use App\Containers\Backoffice\Category\Models\Category;


use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Ship\Parents\Controllers\ApiCollection;
use App\Ship\Parents\Controllers\ApiHelper;

class ApiRoute
{

  
    public static function Seo($e){

        $seo = [];
        $seo['meta_title']       = $e['metatitle_txt_seo'];
        $seo['meta_description'] = $e['metadesc_txtarea_seo'];
        $seo['meta_keywords']    = $e['metakw_txt_seo'];
        $seo['follow']           = $e['follow_select_seo'];
        $seo['index']            = $e['index_select_seo'];
        $seo['og_title']         = $e['ogtitle_txt_seo'];
        $seo['og_description']   = $e['ogdesc_txtarea_seo'];
        $seo['og_image']         = ApiHelper::getSinglePathStorageById($e['ogimg_img_seo']);
        return $seo;

    }

    public static function transField($model_id,$lang,$model,$field){

        $default_lang  = ApiHelper::validLang()['default_lang'];
        $e             = ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model))::where('id',$model_id)->first();
        $parentid      = @$e->parentid_hidden_general;
        
        if($lang == $default_lang){
            if($parentid != '' || $parentid != ''){
                $e1 = ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model))::where('id',$parentid)->first();
            }
            else{
                $e1 = ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model))::where('id',$e->id)->first();
            }
        }
           
        else
        {

            if($parentid != '' || $parentid != '')
                $e1 = ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model))::where('id',$e->id)->first();
            else
                $e1 = ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model))::where('parentid_hidden_general',$e->id)
                ->where('locale_hidden_general',$lang)->first();

          
        }

        return @$e1->{$field};

    }

    public static function Route($lang,$e,$model,$request){

        $route = [];
        $langs  = ApiHelper::validLang()['valid_lang'];
        $template = Template::where('id',$e['template_id_general'])->first();
        $route['id']     = @$template->name_txt_general;
        $route['locale'] = $lang;

        foreach($langs as $ln){
            $pattern            = self::GetSlugTrans($template,$ln,$lang,'template');
            $slug               = self::GetSlugTrans($e,$ln,$lang,$model);
            $slug               = str_replace(":slug",$slug,$pattern);

            if(strpos($pattern,':subcategory')  !== false) {
                $category     = Category::where('slug_txt_general',$request->slug2)->first();
                $category     = self::GetSlugTrans($category,$ln,$lang,'category');
                $slug         = str_replace(":subcategory",$category,$slug);

                if(strpos($pattern,':category')  !== false) {
                    $category     = Category::where('slug_txt_general',$request->slug1)->first();
                    $category     = self::GetSlugTrans($category,$ln,$lang,'category');
                    $slug         = str_replace(":category",$category,$slug);
                }

            } else if(strpos($pattern,':category')  !== false) {
                $category     = Category::where('slug_txt_general',$request->slug2)->first();
                $category     = self::GetSlugTrans($category,$ln,$lang,'category');
                $slug         = str_replace(":category",$category,$slug);
            }

            $route['slug'][$ln] = $slug;
            
        }
    
        return $route;

    }

    public static function GetSlugTrans($e,$lang,$locale,$model){

        $default_lang  = ApiHelper::validLang()['default_lang'];
        $parentid      = $e['parentid_hidden_general'];
        $id = $parentid !== '' ? $parentid : $e['id'];

        if($lang == $default_lang) {
            $e1 = ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model))::where('id',$id)->first();
        } else {            
            $e1 = ('App\Containers\Backoffice\\'.ucfirst($model).'\\Models\\'.ucfirst($model))::where('parentid_hidden_general', $id)->where('locale_hidden_general', $lang)->first();
        }

        $slug =  isset($e1->slug_txt_general) ? $e1->slug_txt_general : $e1;

        return $slug;

    }

    public static function determinateIDpageByRoute(){
        $url    = explode('/',$_SERVER['REQUEST_URI']);
        $valid_lang = ApiHelper::ArrayValidLanguage();
        $lang       = '';
        $start_deep = 0;
        for($i=0;$i <= ( count($url) - 1) ;$i++)
        {
           if(in_array($url[$i],$valid_lang))
           { 
               $lang       = $url[$i]; 
               $start_deep = $i;
           }
        }
        $lang  = @$url[2];
        $slug0 = '';
        $slug1 = @$url[$start_deep+1];
        $slug2 = @$url[$start_deep+2];
        $slug3 = @$url[$start_deep+4];

        $deep    = 0;
        $resPage = [];

        $final_slug = '/';
        //calcolo profondità della url e prendo lo slug finale
        for($i=( $start_deep + 1) ;$i< (count($url)) ;$i++)
        {
           if($url[$i] != ''){
             $final_slug =  $url[$i];
             $deep++;
           }
        }

        $models = ApiCollection::get('model');
        $isFind = false;
        $Model   = '';
        $resModel = [];
        
        foreach($models as $model){
            if($model['value'] != ''){
                $Model = $model['value'];
                $resModel = @('App\Containers\Backoffice\\'.ucfirst($Model).'\Models\\'.ucfirst($Model))::where('slug_txt_general',$final_slug)->where('locale_hidden_general',$lang)->where('enabled_is_general','true')->first();  
                if($resModel){
                    $isFind = true;
                    break;
                }
           }
        }

 

        $res = array(
           'dataModel'  => $resModel ,
           'model'      => $Model,
           'deep_route' => $deep
         );



        return $res;


    }


}