<?php

namespace App\Ship\Parents\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class ApiStructure 
{

    public static function getStructureOrder($order)
    {
        $O = [];
        $O['items']               = $order['orderitems'];
        $O['id']                  = $order['id'];
        $O['code']                = $order['code'];
        $O['status']              = $order['orderstatus'];
        $O['date']                = $order['createdonutc'];
        $O['currency']['code']    = 'EUR';
        $O['currency']['symbol']  = '€';
        $O['subtotal']            = $order['subtotal'];
        $O['total']               = $order['total'];
        $O['totalvat']            = $order['totalvat'];
        $O['vat']                 = $order['vat'];
        $O['country']             = $order['country'];
        $O['company']             = $order['company'];
        $O['meta']                = $order['meta'];

        $O['shippingcost']        = '';
    
    
        $O['billingaddress']['id']          = '';
        $O['billingaddress']['_type']       = '';
        $O['billingaddress']['_default']    = '';
        $O['billingaddress']['label']       = '';
        $O['billingaddress']['name']        = $order['billingname'];
        $O['billingaddress']['surname']     = $order['billingsurname'];
        $O['billingaddress']['company']     = $order['billingcompany'];
        $O['billingaddress']['phone']       = $order['billingphone'];
        $O['billingaddress']['address1']    = $order['billingaddress1'];
        $O['billingaddress']['address2']    = $order['billingaddress2'];;
        $O['billingaddress']['city']        = $order['billingcity'];
        $O['billingaddress']['zip']         = $order['billingZIP'];
        $O['billingaddress']['country']     = $order['billingcountry'];
        $O['billingaddress']['region']      = $order['billingregion'];
        $O['billingaddress']['vatnumber']   = '';
        $O['billingaddress']['uniquecode']  = '';
        $O['billingaddress']['pec']         = '';
    
    
        $O['shippingaddress']['id']          = '';
        $O['shippingaddress']['_type']       = '';
        $O['shippingaddress']['_default']    = '';
        $O['shippingaddress']['label']       = '';
        $O['shippingaddress']['name']        = $order['shippingname'];
        $O['shippingaddress']['surname']     = $order['shippingsurname'];
        $O['shippingaddress']['company']     = $order['shippingcompany'];
        $O['shippingaddress']['phone']       = $order['shippingphone'];
        $O['shippingaddress']['address1']    = $order['shippingaddress1'];
        $O['shippingaddress']['address2']    = $order['shippingaddress2'];
        $O['shippingaddress']['city']        = $order['shippingcity'];
        $O['shippingaddress']['zip']         = $order['shippingZIP'];
        $O['shippingaddress']['country']     = $order['shippingcountry'];
        $O['shippingaddress']['region']      = $order['shippingregion'];
        $O['shippingaddress']['vatnumber']   = '';
        $O['shippingaddress']['uniquecode']  = '';
        $O['shippingaddress']['pec']         = '';
    
    
        $O['discount'] = null;
    
    
        $S  = DB::table('olmo_shippingmethod')->where('code_txt_general',$order['shippingmethod'])->first();

        $O['shippingmethod']['code']                       = $order['shippingmethod'];
        $O['shippingmethod']['name']                       = $S->name_txt_general;
        $O['shippingmethod']['description']                = $S->description_txt_general;
        $O['shippingmethod']['cost']                       = $S->cost_txt_general;
        $O['shippingmethod']['requiredShippingAddress']    = '';
        $O['shippingmethod']['codrequiredShippingAddress'] = '';
    

        $P  = DB::table('olmo_paymentmethod')->where('code_txt_general',$order['paymentmethod'])->first();
        $O['paymentmethod']['code']                       =  $order['paymentmethod'];
        $O['paymentmethod']['name']                       =  $P->name_txt_general;
        $O['paymentmethod']['description']                =  $P->description_txt_general;
    
        return $O;
    }


    public static function stripField($field){

      
        $C = @explode('_',$field);
        if(@$C[0] == ''){
            return $C;
        }else{
            return $C[0];
        }
    }

    public static function responseFrontend($items,$decode = [],$single = false){

        $items   = json_decode(json_encode($items), true);
        $res     = [];
        $ix      = 0;



        foreach($items as $item){
            array_walk_recursive( $item, function($value,$key) use(&$res,$ix,$decode) {
                $name = self::stripField($key);

                if($name == 'meta'){
                    $value = json_decode($value);
                }
                
                if(in_array($name,$decode)){
                    $value = json_decode($value);
                }
                    
                $res[$ix][$name] = $value;
            });
            $ix++;
        }
        if($single){
            return $res[0];
        }else{
            return $res;
        }
    
    }

}
